<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentLog extends Model
{
  protected $fillable = ['student_id', 'invoice_id', 'start_date', 'end_date'];
  
}
