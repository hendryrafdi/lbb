<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentSchool extends Model
{
    /**
     * Get the school that owns the student school.
     */
    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }
}
