<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * Get the item type that owns the item.
     */
    public function item_type()
    {
        return $this->belongsTo('App\Models\ItemType', 'item_type_id', 'id');
    }

    /**
     * Get the price list that owns the item.
     */
    public function price_list()
    {
        return $this->belongsTo('App\Models\PriceList', 'price_list_id', 'id');
    }
}
