<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    protected $table = 'price_lists';

    /**
     * Get the lines that pricelist owned.
     */
    public function lines()
    {
        return $this->hasMany('App\Models\PriceListLine');
    }
}
