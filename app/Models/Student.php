<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * Get the school that owns the student.
     */
    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_code', 'code');
    }

    /**
     * Get the documents that student owned.
     */
    public function documents()
    {
        return $this->hasMany('App\Models\StudentDocument');
    }

    /**
     * Get the schools that student owned.
     */
    public function schools()
    {
        return $this->hasMany('App\Models\StudentSchool');
    }
}
