<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionLine extends Model
{
    /**
     * Get the item that owns the transaction line.
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Item', 'item_code', 'code');
    }
}
