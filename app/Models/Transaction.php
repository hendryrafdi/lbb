<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * Get the customer that owns the transaction.
     */
    public function  customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * Get the student that owns the transaction.
     */
    public function  student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    /**
     * Get the transaction lines that transaction owned.
     */
    public function transaction_lines()
    {
        return $this->hasMany('App\Models\TransactionLine', 'transaction_code', 'code');
    }
    
    /**
     * Get the transaction logs that transaction owned.
     */
    public function transaction_logs()
    {
        return $this->hasMany('App\Models\TransactionLog', 'transaction_code', 'code');
    }
}
