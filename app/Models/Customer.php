<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Controllers\API;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_name', 'postal_code', 'photo', 'city', 'postal_code', 'country', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
        parent::setAttribute($key, $value);
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $arr['template'] = 'emails.forget';
        $arr['from_email'] = 'lenterabagibangsa@funedge.co.id';
        $arr['email'] = request()->email;
        $arr['token'] = $token;
        $arr['subject'] = 'Reset Password';
        API::sendEmail($arr);
    }
}
