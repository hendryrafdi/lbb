<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceListLine extends Model
{
    protected $fillable = ['price', 'school_code'];

    /**
     * Get the school that owns the pricelist.
     */
    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_code', 'code');
    }
}
