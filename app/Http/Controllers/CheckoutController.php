<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Models\Student;
use App\Models\Item;
use App\Models\PriceList;
use App\Http\Controllers\API;
use App\Models\PriceListLine;
use App\Models\Customer;
use Auth;

class CheckoutController extends Controller
{
    public function index()
    { 
      $lbb['config'] = API::getDefaultConfig();
      $lbb['session'] = Session::get(null);
      $lbb['page_type'] = 'checkout';
      $id = Auth::guard('customers')->id();
      $lbb['customer'] = Customer::where('id', $id)->firstOrFail();
            
      return view('frontend.checkout')->with($lbb);
    }
    
}
