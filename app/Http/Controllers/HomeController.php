<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Controllers\API;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      $lbb['config'] = API::getDefaultConfig();
      $lbb['session'] = Session::get(null);
      $cart = Session::get('cart');

      if($cart > 0){
        $lbb['arr'] = [
            'total' => count($cart),
            'data' => $cart,
        ];
      } else {
        $lbb['arr'] = [
            'total' => 0,
            'data' => $cart,
        ];
      }
      $lbb['page_type'] = 'home';
      return view('frontend.home')->with($lbb);
    }
    
    public function test_email(){
      $arr['template'] = 'emails.doku_recuring_update';
      $arr['from_email'] = 'lbb.info@lenterabagibangsa.org';
      $arr['email'] = 'hendryrafdi@gmail.com';
      $arr['name'] = 'Hendry';
      $arr['subject'] = 'Credit card have been expired';
      $arr['invoice_number'] = 'INV-TEST';
      $arr['link'] = 'http://lenterabagibangsa.funedge.co.id/';
      
	  file_put_contents(storage_path('logs') . '/test.log', print_r($arr) . "\n", FILE_APPEND);
      API::sendEmail($arr);
    }
    
    public function setLanguage($lang){
      if($lang !== ''){
        Session::put('language',strtolower($lang));
      }
      
      return Redirect::back();
    }
}
