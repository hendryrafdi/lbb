<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Yajra\Datatables\Datatables;
use DB;
use Validator;
use Auth;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.setting.index');
    }

    /**
     * Get data schools.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $setting = DB::table('settings')
                ->select([
                    DB::raw('@rownum := @rownum + 1 AS rownum'),
                    'settings.*'
                ]);

        $datatables = Datatables::of($setting)
            ->addColumn('action', function ($data) {
                $act = '<a href="'. route("admin.setting.edit", ['setting' => $data->id]) .'"><i class="icon icon-pencil"></i></a>';

                return $act;
            })
            ->removeColumn('id')
            ->rawColumns(['action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', function($query, $keyword) {
                    $sql = '@rownum + 1 like ?';
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }

        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.setting.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        try {
            $data = new Setting;
            $data->field = $request->field;
            $data->type = $request->type;
            $data->value = $request->value;
            $data->save();

            return redirect()->route('admin.setting.index')->with('status', 'Data saved successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Setting::where('id', $id)->firstOrFail();

        return view('backend.setting.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        try {
            $data = Setting::where('id', $id)->firstOrFail();
            $data->field = $request->field;
            $data->type = $request->type;
            $data->value = $request->value;
            $data->save();

            return redirect()->route('admin.setting.index')->with('status', 'Data updated successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['Failed to update data. Data is being used in other modules.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Setting::where('id', $id)->firstOrFail()->delete();

            return response()->json([]);
        } catch (\Exception $e) {
            return response()->json([], $e->getStatusCode());
        }
    }
}
