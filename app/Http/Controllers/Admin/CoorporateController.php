<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coorporate;
use App\Models\CoorporateLines;
use App\Models\CoorporatePayment;
use App\Models\Customer;
use App\Models\Student;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use Validator;
use Excel;
use Auth;
use File;

class CoorporateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['coorporates'] = Coorporate::join('customers', 'coorporate.customer_id','=','customers.id')->select('customers.*', 'coorporate.id as coorporate_id', 'coorporate.customer_id')->get();
        $data['customers'] = Customer::where('flag_coorporate', 1)->orderBy('name')->get();
        return view('backend.coorporate.index')->with($data);
    }

    /**
     * Get data students.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $coorporate = DB::table('coorporate')->leftJoin('customers', 'coorporate.customer_id', '=', 'customers.id')
                ->select([
                  DB::raw('@rownum := @rownum + 1 AS rownum'),
                  'coorporate.id',
                  'coorporate.customer_id',
                  'customers.name'
                ]);
        $datatables = Datatables::of($coorporate)
          ->addColumn('action', function ($coorporate) {
              $act = '<a href="'. route("admin.customer.show", ['customer' => $coorporate->customer_id]) .'"><i class="icon-magnifier"></i></a>';
              $act .= ' <a href="'. route("admin.coorporate.edit", ['coorporate' => $coorporate->id]) .'"><i class="icon-pencil"></i></a>';
            //   $act .= ' <a><i data-id="'. $coorporate->id .'" class="delete icon-trash"></i></a>';

              return $act;
          })
          ->removeColumn('id')
          ->rawColumns(['action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', function($query, $keyword) {
                    $sql = '@rownum + 1 like ?';
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }

        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
    	// $validator = Validator::make($request->all(), [
     //      'customer_id' => 'required|unique:coorporate',
     //    ]);

     //    if ($validator->fails()) {
     //        return redirect()->back()->withInput()->withErrors($validator);
     //    }

    	DB::beginTransaction();
    	try {
    		$sql = new Coorporate;
    		$sql->customer_id = $request->customer;
    		$sql->save();
    		DB::commit();
    		return redirect()->route('admin.coorporate.index')->with('status', 'Data saved successfully!');
    	} catch (\Exception $e) {
    		DB::rollBack();
    		return redirect()->back()->withInput()->withErrors([$e->getMessage()]);
    	}
    }

    public function edit($id){
      $data['coorporate'] = Coorporate::join('customers', 'coorporate.customer_id','=','customers.id')->select('customers.*', 'coorporate.id as coorporate_id', 'coorporate.customer_id')->where('coorporate.id', $id)->firstOrFail();
      $data['students'] = Student::Select('students.*')->Join('coorporate_lines', 'students.nisn', '=', 'coorporate_lines.nisn')->where('coorporate_lines.coorporate_id', $id)->paginate(10);
      // dd($data['students']);
      return view('backend.coorporate.edit')->with($data);
    }

    /**
     * Upload data students.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
      $validator = Validator::make($request->all(), [
          // 'id' => 'required',
          'file' => 'required',
      ]);
      // process the form
      if ($validator->fails()) {
          return redirect()->back()->withErrors($validator);
      }
      
      if($request->file('file'))
      {
            DB::beginTransaction();
            try {
                $path = $request->file('file')->getRealPath();
                $data = Excel::load($path, function($reader) {})->get();
                $headerRow = $data->first()->keys()->toArray();

                $formatUpload = [
                    'nisn' => 0,
                    'start_year' => 1,
                    'end_year' => 2
                ];

                foreach ($data as $k => $v) {
                    // if (empty($v[$headerRow[$formatUpload['first_name']]])) {
                    //     continue;
                    // }

                    // // Check school code exist
                    // $checkSchool = School::where('code', $v[$headerRow[$formatUpload['school_code']]])->first();
                    // if (empty($checkSchool)) {
                    //     throw new \Exception('School code cannot be empty or school code not found ' . $v[$headerRow[$formatUpload['school_code']]]);
                    // }

                    // Check NISN
                    $coorporate_lines = CoorporateLines::where('nisn', $v['nisn'])->first();
                    if (empty($coorporate_lines)) {
                        $coorporate_lines = new CoorporateLines;
                    }
                        $coorporate_lines->coorporate_id = $request->id;
                    foreach ($formatUpload as $kdx => $idx) {
                        if (isset($v[$headerRow[$idx]])) {
                            // if ($kdx === 'gender') {
                            //     $v[$headerRow[$idx]] = ($v[$headerRow[$idx]] === 'Male') ? 'M' : 'F';
                            // }

                            $coorporate_lines->{$kdx} = $v[$headerRow[$idx]];
                        }
                    }

                    $coorporate_lines->updated_at = date('Y-m-d H:i:s');
                    $coorporate_lines->save();

                    //make student sponsored_ship 1
                    $student = Student::where('nisn', $v['nisn'])->firstOrFail();
                    $student->is_sponsored = 1;
                    $student->save();
                }

                DB::commit();
                return response()->json([]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json($e->getMessage(), 500);
            }   
          
      }
    }

    /**
     * Get data students.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataStudent(Request $request, $id)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $students = DB::table('students')->leftJoin('coorporate_lines', 'students.nisn', '=', 'coorporate_lines.nisn')->leftJoin('coorporate', 'coorporate_lines.coorporate_id', '=', 'coorporate.id')
                ->select([
                  DB::raw('@rownum := @rownum + 1 AS rownum'),
                  'students.id',
                  'students.nisn',
                  'students.first_name',
                  'students.last_name',
                  'students.photo',
                  'students.birth_date'
                ])->where('coorporate_lines.coorporate_id', $id);

        $datatables = Datatables::of($students)
          ->addColumn('action', function ($students) {
              $act = '<a href="'. route("admin.student.show", ['student' => $students->id]) .'"><i class="icon-magnifier"></i></a>';

              return $act;
          })
          ->removeColumn('id')
          ->editColumn('photo', function ($data) {
            $image = '<center><img width="150" src="'. asset('/assets/images/avatar.jpeg') .'" /></center>';
            if (!empty($data->photo)) {
                $image = '<center><a target="_blank" href="'. asset('/uploads/photo/' . $data->photo). '">';
                if (file_exists(asset('/uploads/photo/200_200_' . $data->photo))){
                  $image .= '<img width="150" src="'. asset('/uploads/photo/200_200_' . $data->photo) .'" />';
                }else{
                  $image .= '<img width="150" src="'. asset('/uploads/photo/' . $data->photo) .'" />';
                }

                
                $image .= '</a></center>';
            }

            return $image;
          })
          ->rawColumns(['photo', 'action']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', function($query, $keyword) {
                    $sql = '@rownum + 1 like ?';
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            });
        }

        return $datatables->make(true);
    }

    function payment($id){
      $data['coorporate'] = Coorporate::leftJoin('customers', 'customers.id', '=', 'coorporate.customer_id')->select('coorporate.*', 'customers.name')->where('coorporate.id', $id)->firstOrFail();

      $data['coorporate_data'] = Coorporate::Select('coorporate_lines.*', 'students.first_name', 'students.last_name')->leftJoin('coorporate_lines', 'coorporate.id', '=', 'coorporate_lines.coorporate_id')->leftJoin('students', 'coorporate_lines.nisn', '=', 'students.nisn')->where('coorporate_lines.coorporate_id', $id)->paginate(10);

      $data['coorporate_payments'] = CoorporatePayment::where('coorporate_id', $id)->get();
      return view('backend.coorporate.payment')->with($data);
    }

    /**
     * Upload data students.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadPayment(Request $request)
    {
      $validator = Validator::make($request->all(), [
          // 'id' => 'required',
          'file' => 'required',
      ]);
      // process the form
      if ($validator->fails()) {
          return redirect()->back()->withErrors($validator);
      }
      
      if($request->file('file'))
      {
            DB::beginTransaction();
            try {
                $path = $request->file('file')->getRealPath();
                $data = Excel::load($path, function($reader) {})->get();
                $headerRow = $data->first()->keys()->toArray();

                $formatUpload = [
                    'nisn' => 0,
                    'month' => 1,
                    'year' => 2,
                    'status' => 3
                ];

                foreach ($data as $k => $v) {
                    // if (empty($v[$headerRow[$formatUpload['first_name']]])) {
                    //     continue;
                    // }

                    // // Check school code exist
                    // $checkSchool = School::where('code', $v[$headerRow[$formatUpload['school_code']]])->first();
                    // if (empty($checkSchool)) {
                    //     throw new \Exception('School code cannot be empty or school code not found ' . $v[$headerRow[$formatUpload['school_code']]]);
                    // }

                    // Check NISN
                    $coorporate_payment = CoorporatePayment::where('nisn', $v['nisn'])->where('month', $v['month'])->where('year', $v['year'])->first();
                    if (empty($coorporate_payment)) {
                        $coorporate_payment = new CoorporatePayment;
                    }
                        $coorporate_payment->coorporate_id = $request->id;
                    foreach ($formatUpload as $kdx => $idx) {
                        if (isset($v[$headerRow[$idx]])) {
                            // if ($kdx === 'gender') {
                            //     $v[$headerRow[$idx]] = ($v[$headerRow[$idx]] === 'Male') ? 'M' : 'F';
                            // }

                            $coorporate_payment->{$kdx} = $v[$headerRow[$idx]];
                        }
                    }

                    $coorporate_payment->updated_at = date('Y-m-d H:i:s');
                    $coorporate_payment->save();

                }

                DB::commit();
                return response()->json([]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json($e->getMessage(), 500);
            }   
          
      }
    }

    function loadPayment(Request $request){
        $tabel = "";
        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $years = array('2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025');
        $payments = CoorporatePayment::where('coorporate_id', $request->coorporate_id)->where('nisn', $request->nisn)->get();
        foreach ($payments as $payment) {
          // $tabel .= "<input type='hidden' id='coorporate_line_id-".$request->coorporate_line_id."'/>";
          // $tabel .= "<input type='hidden' id='coorporate_id-".$request->coorporate_id."'/>";
          // $tabel .= "<input type='hidden' id='nisn-".$request->nisn."'/>";
          $tabel .= "<input type='hidden' id='payment_id-".$payment->id."'/>";
          ($payment->status == 'Paid')? $tabel.="<tr class='success'>" : $tabel.="<tr class='danger'>";
          $tabel .= "<td class='oldPayment-".$payment->id."'>".$payment->month."</td>";
          $tabel .= "<td class='editPayment-".$payment->id."' style='display:none;'><select class='form-control' name='editedMonth' id='editedMonth-".$payment->id."'>";
                    foreach ($months as $i => $month) {
                        $tabel .= "<option value='".$month."' ".(($payment->month == $month)? 'selected' : '' )." >".$month."</option>";
                      }
          $tabel .= "</select></td>";
          $tabel .= "<td class='oldPayment-".$payment->id."'>".$payment->year."</td>";
          $tabel .= "<td class='editPayment-".$payment->id."' style='display:none;'><select class='form-control' name='editedYear' id='editedYear-".$payment->id."'>";
                    foreach ($years as $i => $year) {
                        $tabel .= "<option value='".$year."' ".(($payment->year == $year)? 'selected' : '' )." >".$year."</option>";
                      }
          $tabel .= "</select></td>";
          $tabel .= "<td class='oldPayment-".$payment->id."'>".$payment->status."</td>";
          $tabel .= "<td class='editPayment-".$payment->id."' style='display:none;'><select class='form-control' name='editedStatus' id='editedStatus-".$payment->id."'>";
                      $tabel .= "<option value='Paid' ".(($payment->status == 'Paid')? 'selected' : '').">Paid</option>";
                      $tabel .= "<option value='Not Paid' ".(($payment->status == 'Not Paid')? 'selected' : '').">Not Paid</option>";
          $tabel .= "</select></td>";
          $tabel .= '<td class="oldPayment-'.$payment->id.'" align="center">';
          $tabel .= '<a href="javascript:;" onClick="editPayment('.$payment->id.')"><i class="icon-pencil"></i></a>&nbsp;';
          $tabel .= '<a href="javascript:;" onClick="deletePayment('.$payment->id.','.$request->coorporate_line_id.')"><i class="icon-trash"></i></a>';
          $tabel .= '</td>';
          $tabel .= '<td class="editPayment-'.$payment->id.'" style="display:none;" align="center">';
          $tabel .= '<a href="javascript:;" onClick="cancelEdit('.$payment->id.')" data-toggle="tooltip" title="Cancel" id="btnCancel"><i class="fa fa-times"></i></a>&nbsp;';
          $tabel .= '<a href="javascript:;" onClick="saveDetail('.$payment->id.','.$request->coorporate_line_id.')"><i class="fa fa-check"></i></a>';
          $tabel .= '</td>';
          $tabel .= "</tr>";
        }
        return $tabel;
    }

    function updateDetailPayment(Request $request){
      $sql = CoorporatePayment::find($request->id);
      $sql->month = $request->month;
      $sql->year = $request->year;
      $sql->status = $request->status;
      $sql->updated_at = date('Y-m-d H:i:s');
      $sql->save();
      echo json_encode($sql);
    }

    function deleteDetailPayment(Request $request){
      $return_data = [];
      try {
        $sql = CoorporatePayment::find($request->payment_id);
        $sql->delete();
        $return_data['message_type'] = "success";
        $return_data['message'] = "Data deleted successfully";
      } catch (\Exception $e) {
        $return_data['message_type'] = "error";
        $return_data['message'] = $e->getMessage();
      }
        echo json_encode($return_data);
    }

    /**
     * Get data students.
     *
     * @return \Illuminate\Http\Response
     */
    // public function dataPayment(Request $request)
    // {
    //     DB::statement(DB::raw('set @rownum=0'));
    //     $coorporate = DB::table('coorporate')->leftJoin('customers', 'coorporate.customer_id', '=', 'customers.id')
    //             ->select([
    //               DB::raw('@rownum := @rownum + 1 AS rownum'),
    //               'coorporate.id',
    //               'coorporate.customer_id',
    //               'customers.name'
    //             ]);
    //     // $coorporate = DB::Select(DB::raw('@rownum := @rownum + 1 AS rownum'), 'select coorporate.*, customers.name,
    //     //   (select count(coorporate_lines.id) from coorporate_lines where coorporate_lines.coorporate_id = coorporate.id) as total_student
    //     //   from coorporate
    //     //   left join customers on coorporate.customer_id = customers.id'); 
    //     $datatables = Datatables::of($coorporate)
    //       ->addColumn('action', function ($coorporate) {
    //           $act = '<a href="'. route("admin.student.show", ['coorporate' => $coorporate->id]) .'"><i class="icon-magnifier"></i></a>';
    //           $act .= ' <a href="'. route("admin.coorporate.edit", ['coorporate' => $coorporate->id]) .'"><i class="icon-pencil"></i></a>';
    //           $act .= ' <a><i data-id="'. $coorporate->id .'" class="delete icon-trash"></i></a>';

    //           return $act;
    //       })
    //       ->removeColumn('id')
    //       ->rawColumns(['action']);

    //     if ($keyword = $request->get('search')['value']) {
    //         $datatables->filterColumn('rownum', function($query, $keyword) {
    //                 $sql = '@rownum + 1 like ?';
    //                 $query->whereRaw($sql, ["%{$keyword}%"]);
    //         });
    //     }

    //     return $datatables->make(true);
    // }

    /**
     * Download data students.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadPayment(Request $request, $id)
    {
        $data = CoorporatePayment::select([
            'nisn',
            'month',
            'year',
            'status'
        ])->where('coorporate_id', $id)->get();

        $customer = Coorporate::leftJoin('customers', 'coorporate.customer_id', '=', 'customers.id')->select('customers.name')->where('coorporate.id', $id)->firstOrFail();

        Excel::create('Payment Coorporate '.$customer->name, function($excel) use($data) {
            // Set the title
            $excel->setTitle('Data Payment Coorporate');
            // Call them separately
            $excel->setDescription('A payment file');

            $excel->sheet('DATABASE', function ($sheet) use($data) {
                $sheet->cell('A1', function($cell) { $cell->setValue('NISN'); });
                $sheet->cell('B1', function($cell) { $cell->setValue('Month'); });
                $sheet->cell('C1', function($cell) { $cell->setValue('Year'); });
                $sheet->cell('D1', function($cell) { $cell->setValue('Status'); });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i = $key + 2;
                        $sheet->cell('A' . $i, $value['nisn']); 
                        $sheet->cell('B' . $i, $value['month']); 
                        $sheet->cell('C' . $i, $value['year']);
                        $sheet->cell('D' . $i, $value['status']); 
                    }
                }
            });
        })->download('xlsx');
    }

    /**
     * Download data students.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadStudent(Request $request, $id)
    {
        $data = CoorporateLines::select([
            'nisn',
            'start_year',
            'end_year'
        ])->where('coorporate_id', $id)->get();

        $customer = Coorporate::leftJoin('customers', 'coorporate.customer_id', '=', 'customers.id')->select('customers.name')->where('coorporate.id', $id)->firstOrFail();

        Excel::create('Student '.$customer->name, function($excel) use($data) {
            // Set the title
            $excel->setTitle('Student Data');
            // Call them separately
            $excel->setDescription('A student file');

            $excel->sheet('DATABASE', function ($sheet) use($data) {
                $sheet->cell('A1', function($cell) { $cell->setValue('NISN'); });
                $sheet->cell('B1', function($cell) { $cell->setValue('Start Year'); });
                $sheet->cell('C1', function($cell) { $cell->setValue('End Year'); });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i = $key + 2;
                        $sheet->cell('A' . $i, $value['nisn']); 
                        $sheet->cell('B' . $i, $value['start_year']); 
                        $sheet->cell('C' . $i, $value['end_year']);
                    }
                }
            });
        })->download('xlsx');
    }
}
