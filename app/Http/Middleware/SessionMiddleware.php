<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;

class SessionMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
      $session = $request->session()->get(null);
      if(!isset($session['id'])){
        $sessionId = md5(uniqid());
        $session = [
            'id' => $sessionId,
            'cart' => [],
            'removed_cart' => [],
            'updated_cart' => [],
        ];
      
        // Default locale
        if(!isset($session['language'])){
          $session['language'] = 'en';
        }

        if(isset($_GET['language']) && in_array($_GET['language'], [ 'id', 'en' ]))
          $session['language'] = $_GET['language'];// Default locale

        if(isset($_GET['language']) && in_array($_GET['language'], [ 'id', 'en' ]))
          $session['language'] = $_GET['language'];
      
        $request->session()->put($session);
      }
      
      
      app()->setLocale($session['language']);
      
      $response = $next($request);
      
      return $response;

    }
}
