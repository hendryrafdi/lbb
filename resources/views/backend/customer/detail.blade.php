@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">Customer Detail</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.customer.index') }}">Customer</a></li>
            <li class="active">{{ $data->name }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          <div class="form-group">
            <h3>Email address</h3>
            {{ $data->email }}
          </div>
          <div class="form-group">
            <h3>Name</h3>
            {{ $data->name }}
          </div>
          <div class="form-group">
            <h3>Last Name</h3>
            {{ $data->last_name or "-" }}
          </div>
          <div class="form-group">
            <h3>Address</h3>
            {{ $data->address or "-" }}
          </div>
          <div class="form-group">
            <h3>City</h3>
            {{ $data->city or "-" }}
          </div>
          <div class="form-group">
            <h3>Postal Code</h3>
            {{ $data->postal_code or "-" }}
          </div>
          <div class="form-group">
            <h3>Country</h3>
            {{ $data->country or "-" }}
          </div>
          <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.customer.index') }}" class="btn btn-default">&laquo; Back</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/.row -->
@endsection