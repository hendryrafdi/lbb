@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">Coorporate</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.coorporate.index') }}">Coorporate</a></li>
            <li><a href="{{ route('admin.coorporate.edit', ['id' => $coorporate->id]) }}">{{ $coorporate->name }}</a></li>
            <li class="active">Payment</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-white">
              <div class="panel-heading">
                  <!-- <a class="btn btn-success m-b-sm" href="#" data-toggle="modal" data-target="#modal-add">Add New Coorporate</a> -->
                  <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-box">Import Payment</a>
                  <a class="btn btn-primary" href="{{ route('admin.coorporate.downloadpayment', ['id' => $coorporate->id]) }}">Export Payment</a>
                  <div class="panel-control">
                    
                  </div>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
                  @endif
                  <input type="hidden" name="coorporate_id" id="coorporate_id" value="">
                  <table id="data-table" class="display table dataTable">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>NISN</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Period</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php $no = 1; @endphp
                      @foreach($coorporate_data as $coorporate_line)
                      <tr id="payment-master-{{ $coorporate_line->id }}">
                        <td>
                          <input type="hidden" name="coorporateline_id-{{ $coorporate_line->id }}" id="coorporateline_id-{{ $coorporate_line->id }}" value="{{ $coorporate_line->id }}">
                          <input type="hidden" name="coorporate_id-{{ $coorporate_line->id }}" id="coorporate_id-{{ $coorporate_line->coorporate_id }}" value="{{ $coorporate_line->coorporate_id }}">
                          <input type="hidden" name="nisn-{{ $coorporate_line->nisn }}" id="nisn-{{ $coorporate_line->nisn }}" value="{{ $coorporate_line->nisn }}">
                          {{ $no }}
                        </td>
                        <td>{{ $coorporate_line->nisn }}</td>
                        <td>{{ $coorporate_line->first_name }}</td>
                        <td>{{ $coorporate_line->last_name }}</td>
                        <td>{{ $coorporate_line->start_year." - ".$coorporate_line->end_year }}</td>
                        <td>
                          <a href="javascript:;" onclick="showPayment({{ $coorporate_line->id }}, {{ $coorporate_line->coorporate_id }}, {{ $coorporate_line->nisn }})" id="btn-showPayment-{{ $coorporate_line->id }}"><i class="icon-magnifier"></i></a>
                          <a href="javascript:;" onclick="hidePayment({{ $coorporate_line->id }})" id="btn-hidePayment-{{ $coorporate_line->id }}" style="display: none;"><i class="icon-magnifier"></i></a>
                        </td>
                      </tr>
                      <tr align="center" id="payment-head-{{ $coorporate_line->id }}" style="display: none;">
                        <td colspan="6">
                          <table class="table">
                            <thead>
                              <tr class="active">
                                <th><b>Month</b></th>
                                <th><b>Year</b></th>
                                <th><b>Status</b></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="boxDetail-{{ $coorporate_line->id }}">
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      
                      @php $no++; @endphp
                      @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>No</th>
                      <th>NISN</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Period</th>
                      <th>Action</th>
                    </tr>
                    </tfoot>
                  </table>
                  {{ $coorporate_data->links() }}
                </div>
              </div>
            </div>
          </div>
      </div>
  </div><!-- Row -->
</div><!-- Main Wrapper -->

<div class="tooltip top" role="tooltip">
  <div class="tooltip-arrow"></div>
  <div class="tooltip-inner">
    Cancel
  </div>
</div>
@endsection

@section('modals')
<div class="modal fade" id="modal-box">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import Data</h4>
      </div>
      <form method="post" action="" id="importdropzone" class="dropzone" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="fallback">
            <input name="file" type="file" multiple />
        </div>
          <input type="hidden" name="id" value="{{ $coorporate->id }}">
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>	
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>	
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.css') }}">
<link href="{{ asset('assets/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
<!-- Sweet Alert -->
<script src="{{ asset('assets/plugins/datatables/js/jquery.datatables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script>
  function showPayment(id, coorporate_id, nisn){
    // console.log(coorporate_id);
    $("#payment-master-"+id).addClass('active');
    $("#payment-head-"+id).show();
    // $("#payment-body-"+id).show();
    $("#btn-hidePayment-"+id).show();
    $("#btn-showPayment-"+id).hide();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "post",
      // url: "{{'admin.coorporate.loadpayment'}}",
      url: "/admin/coorporate/loadpayment",
      data: {
        coorporate_line_id: id,
        coorporate_id: coorporate_id,
        nisn: nisn,
      },
      success:function(result){
        $("#boxDetail-"+id).html(result);
      }
    });
  }

  function hidePayment(id){
    $("#payment-master-"+id).removeClass('active');
    $("#payment-head-"+id).hide();
    // $("#payment-body-"+id).hide();
    $("#btn-hidePayment-"+id).hide();
    $("#btn-showPayment-"+id).show();
  }

  function editPayment(id){
    $(".editPayment-"+id).show();
    $(".oldPayment-"+id).hide();
  }

  function cancelEdit(id){
    $(".editPayment-"+id).hide();
    $(".oldPayment-"+id).show();
  }

  function saveDetail(id, coorporate_line_id){
    let month = $("#editedMonth-"+id).val();
    let year = $("#editedYear-"+id).val();
    let status = $("#editedStatus-"+id).val();
    var coorporate_id = $("#coorporate_id-"+coorporate_line_id).val();
    var nisn = $("#nisn-"+coorporate_line_id).val();
    var coorporateline_id = $("#coorporateline_id-"+coorporate_line_id).val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "post",
      url: "/admin/coorporate/updatedetailpayment",
      data: {
        id: id,
        month: month,
        year: year,
        status: status,
        coorporate_id: coorporate_id,
        nisn: nisn
      },
      success:function(result){
        // reloadPayment(nisn, coorporateline_id, coorporate_id);
        location.reload();
      }
    });
  }


  function deletePayment(payment_id, coorporate_line_id){
    var nisn = $("#nisn-"+coorporate_line_id).val();
    var coorporateline_id = $("#coorporateline_id-"+coorporate_line_id).val();
    var coorporate_id = $("#coorporate_id-"+coorporate_line_id).val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "post",
      url: "/admin/coorporate/deletepayment",
      data: {
        coorporateline_id: coorporateline_id,
        payment_id: payment_id,
        nisn: nisn,
      },
      success:function(result){
        if(result['message_type'] == "success")
          $("#alert").html(result['message']);
          location.reload();
          // reloadPayment(nisn, coorporateline_id, coorporate_id);
      }
    });

    function reloadPayment(nisn, coorporateline_id, coorporate_id){
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        // url: "{{'admin.coorporate.loadpayment'}}",
        url: "/admin/coorporate/loadpayment",
        data: {
          coorporate_id: coorporate_id,
          nisn: nisn,
        },
        success:function(result){
          $("#boxDetail-"+coorporateline_id).html(result);
        }
      });
    }
  }

  // $('#btnCancel').tooltip(options);

// function init() {
//       let coorporate_id = $('#coorporate_id').val();
//       let table = $('#data-table').DataTable({
//         processing: true,
//         serverSide: true,
//         responsive: true,
//         destroy: true,
//         ajax: '{!! route('admin.coorporate.datapayment') !!}',
//         columns: [
//           { data: 'rownum', name: 'rownum'},
//           { data: 'nisn', name: 'nisn' },
//           { data: 'photo', name: 'photo' },
//           { data: 'first_name', name: 'first_name' },
//           { data: 'last_name', name: 'last_name' },
//           { data: 'birth_date', name: 'birth_date' },
//           { data: "action", orderable: false, searchable: false, class: "text-left" }
//         ],
//         fnDrawCallback: () => {
//           $(".delete").on('click', onDelete);
//         },
//         initComplete: () => {
//           $(".delete").on('click', onDelete);
//         }
//       });
//   }

//   init();

//   //---------- SWEET ALERT ------------//
//   function onDelete(data) {
//     data.preventDefault();
//     return swal({
//       title: "Are you sure?",
//       text: "Your will not be able to recover this data!",
//       type: "warning",
//       showCancelButton: true,
//       confirmButtonClass: "btn-danger",
//       confirmButtonText: "Yes, delete it!",
//       closeOnConfirm: false
//     }, () => {
//       let url = '{{ route("admin.student.destroy", [ "student" => ":id"]) }}';
//       url = url.replace(':id', $(this).data('id'));
//       $.ajax({
//         url: url,
//         type: 'json',
//         data: '_token={{ csrf_token() }}',
//         method: 'DELETE',
//         error: () => {
//           swal("Error!", "Failed to delete data student. Please try again later.", "error");
//         },
//         success: () => {
//           swal("Deleted!", "Data student has been deleted.", "success");
//           init();
//         }
//       });
//     });
//   }

Dropzone.autoDiscover = false;
//----- DROPZONE -----//
let id = $('#coorporate_id').val();
var myDropzone = new Dropzone("form#importdropzone", {
  url: "{{ route('admin.coorporate.uploadpayment') }}",
  // data: "id="+id,
  // url: "/coorporate/upload/"+id,
  acceptedFiles: ".xls, .xlsx",
  error: function(file, response) {
    $('#modal-box').modal('hide');
    myDropzone.removeAllFiles();
    swal("Error!", response || "Failed to upload data coorporate. Please try again later.", "error");
  },
  success: function(file, response){
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        $('#modal-box').modal('hide');
        myDropzone.removeAllFiles();
        swal("Uploaded!", "Data student has been uploaded.", "success");
        init();
      }
    });
  }
});
</script>
@endsection
