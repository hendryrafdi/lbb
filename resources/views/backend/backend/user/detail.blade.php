@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">User Detail</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.user.index') }}">User</a></li>
            <li class="active">{{ $data->name }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          <div class="form-group">
            <h3>Email address</h3>
            {{ $data->email }}
          </div>
          <div class="form-group">
            <h3>Name</h3>
            {{ $data->name }}
          </div>
          <div class="form-group">
            <h3>Roles</h3>
            @foreach($data->roles as $key => $val)
              <br />
              <label for="{{ $val->name }}" style="font-weight: normal">{{ $val->display_name }}</label>
            @endforeach
          </div>
          <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.user.index') }}" class="btn btn-default">&laquo; Back</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/.row -->
@endsection