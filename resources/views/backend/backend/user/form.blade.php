@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.user.index') }}">User</a></li>
            <li class="active">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif

          <form role="form" method="POST" enctype="multipart/form-data" action="{{ isset($data) ? route('admin.user.update', ['user' => $data->id]) : route('admin.user.store') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="{{ $data->name or old('name') }}">
                </div>
                <div class="form-group">
                  <label for="email">Email address</label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ $data->email or old('email') }}">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="password_confirmation">Password Confirmation</label>
                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation">
                </div>
                <div class="form-group">
                  <label for="permissions">Roles</label>
                  @foreach($roles as $val)
                    <br />
                    <input id="{{ $val->name }}" class="minimal" type="checkbox" {!! (isset($data) && $data->hasRole($val->name)) ? 'checked="checked"' : '' !!} name="roles[]" value="{{ $val->id }}" /> <label for="{{ $val->name }}" style="font-weight: normal">{{ $val->display_name }}</label>
                  @endforeach
                </div>
              </div>
            </div>

            <!--/.col (left) -->
            <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.user.index') }}" class="btn btn-default">&laquo; Back</a>
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
            <!--/.row -->
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
