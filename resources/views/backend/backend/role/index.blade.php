@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">Roles</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li class="active">Role</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-white">
              <div class="panel-heading">
                  <a class="btn btn-success m-b-sm" href="{{ route('admin.role.create') }}">Add new row</a>
              </div>
              <div class="panel-body">
                  <div class="table-responsive">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                      <table id="data-table" class="display table dataTable" style="width: 100%; cellspacing: 0;">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </tfoot>
                      </table>  
                  </div>
              </div>
          </div>
      </div>
  </div><!-- Row -->
</div><!-- Main Wrapper -->
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>	
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>	
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.css') }}">
@endsection

@section('scripts')
<!-- Sweet Alert -->
<script src="{{ asset('assets/plugins/datatables/js/jquery.datatables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    function init() {
        let table = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            destroy: true,
            ajax: '{!! route('admin.role.data') !!}',
            columns: [
                { data: 'rownum', name: 'rownum'},
                { data: 'name', name: 'name' },
                { data: 'display_name', name: 'display_name' },
                { data: 'description', name: 'description' },
                { data: "action", orderable: false, searchable: false, class: "text-left" }
            ],
            fnDrawCallback: () => {
                $(".delete").on('click', onDelete);
            },
            initComplete: () => {
                $(".delete").on('click', onDelete);
            }
        });
    }

    init();

    //---------- SWEET ALERT ------------//
    function onDelete(data) {
        data.preventDefault();
        return swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, () => {
            let url = '{{ route("admin.role.destroy", [ "role" => ":id"]) }}';
            url = url.replace(':id', $(this).data('id'));
            $.ajax({
                url: url,
                type: 'json',
                data: '_token={{ csrf_token() }}',
                method: 'DELETE',
                error: () => {
                swal("Error!", "Failed to delete data role. Please try again later.", "error");
                },
                success: () => {
                swal("Deleted!", "Data role has been deleted.", "success");
                init();
                }
            });
    });
    }
});
</script>
@endsection
