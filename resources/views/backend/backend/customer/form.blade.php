@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.customer.index') }}">Customer</a></li>
            <li class="active">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif

          <form role="form" method="POST" enctype="multipart/form-data" action="{{ isset($data) ? route('admin.customer.update', ['customer' => $data->id]) : route('admin.customer.store') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="{{ $data->name or old('name') }}">
                </div>
                <div class="form-group">
                  <label for="email">Email address</label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ $data->email or old('email') }}">
                </div>
                <div class="form-group">
                  <label for="last_name">Last Name</label>
                  <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="{{ $data->last_name or old('last_name') }}">
                </div>
                <div class="form-group">
                  <label for="city">City/Province</label>
                  <input type="text" name="city" class="form-control" id="city" placeholder="Enter City" value="{{ $data->city or old('city') }}">
                </div>
                <div class="form-group">
                  <label for="postal_code">Postal Code</label>
                  <input type="text" name="postal_code" class="form-control" id="postal_code" placeholder="Enter Postal Code" value="{{ $data->postal_code or old('postal_code') }}">
                </div>
                <div class="form-group">
                  <label for="country">Country</label>
                  <input type="text" name="country" class="form-control" id="country" placeholder="Enter Country" value="{{ $data->country or old('country') }}">
                </div>
                <div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" name="address" class="form-control" id="address" placeholder="Enter Address" value="{{ $data->address or old('address') }}">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="password_confirmation">Password Confirmation</label>
                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation">
                </div>
              </div>
            </div>

            <!--/.col (left) -->
            <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.customer.index') }}" class="btn btn-default">&laquo; Back</a>
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
            <!--/.row -->
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
