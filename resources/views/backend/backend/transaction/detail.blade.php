@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">User Detail</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.student.index') }}">Student</a></li>
            <li class="active">{{ $data->nisn }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          <div class="form-group">
            <h3>Transaction ID</h3>
            {{ $data->code }}
          </div>
          <div class="form-group">
            <h3>Customer Name</h3>
            {{ $data->customer->name }}
          </div>
          <div class="form-group">
            <h3>Total Transaction</h3>
            Rp. {{ number_format($data->total,'0','.',',') }}
          </div>
          <div class="form-group">
            <h3>Transaction Date</h3>
            {{ $data->transaction_date }}
          </div>
          <div class="form-group">
            <h3>Payment Method</h3>
            {{ $data->payment_method }}
          </div>
          <div class="form-group">
            <h3>Instalment</h3>
            {{ ($data->instalment === 1) ? 'Yes' : 'No' }}
          </div>
          <div class="form-group">
            <h3>Transaction Details</h3>
            <table class="table table-bordered">
              <tr>
                <th>No.</th>
                <th>Item</th>
                <th>Description</th>
                <th>Price</th>
              </tr>
              @foreach($data->transaction_lines as $k => $v)
                <tr>
                  <td>{{ $k + 1 }}</td>
                  <td>{{ $v->item_code }}</td>
                  <td>{{ $v->description }}</td>
                  <td>Rp. {{ number_format($v->price,'0','.',',') }}</td>
                </tr>
              @endforeach
              @if(!empty($data->transaction_lines))
                <tr>
                  <th colspan="3" classs="text-right">Total</th>
                  <th>Rp. {{ number_format($data->total,'0','.',',') }}</th>
                </tr>
              @endif
            </table>
          </div>
          <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.transaction.index') }}" class="btn btn-default">&laquo; Back</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/.row -->
@endsection