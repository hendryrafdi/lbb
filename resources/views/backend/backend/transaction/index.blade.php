@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">Transactions</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li class="active">Transaction</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-white">
              <div class="panel-body">
                  <div class="table-responsive">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                      <table id="data-table" class="display table dataTable" style="width: 100%; cellspacing: 0;">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Customer Name</th>
                            <th>Total</th>
                            <th>Transaction Date</th>
                            <th>Payment Method</th>
                            <th>Instalment</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </table>  
                  </div>
              </div>
          </div>
      </div>
  </div><!-- Row -->
</div><!-- Main Wrapper -->
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>	
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>	
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/datatables/js/jquery.datatables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    function init() {
        let table = $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          responsive: true,
          destroy: true,
          ajax: '{!! route('admin.transaction.data') !!}',
          columns: [
            { data: 'rownum', name: 'rownum'},
            { data: 'code', name: 'code' },
            { data: 'customer_id', name: 'customer_id' },
            { data: 'total', name: 'total' },
            { data: 'transaction_date', name: 'transaction_date' },
            { data: 'payment_description', name: 'payment_description' },
            { data: 'instalment_description', name: 'instalment_description' },
            { data: "action", orderable: false, searchable: false, class: "text-left" }
          ]
        });
    }

    init();
});
</script>
@endsection
