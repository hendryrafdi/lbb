<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint lang="en">
    <head>
        
        <!-- Title -->
        <title>Meteor | Dashboard</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="stacks" />
        
        <!-- Styles -->
        <link href="{{ asset('assets/plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/uniform/css/default.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>	
        <link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/slidepushmenus/css/component.css') }}" rel="stylesheet" type="text/css"/>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Theme Styles -->
        <link href="{{ asset('assets/css/meteor.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/layers/dark-layer.css') }}" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>
        <style>
          body {
            background: none !important;
          }
          .panel {
            border: none !important;
          }
          @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
          }

          @media print {
            .btn-print {
                display: none;
            }
          }
        </style>
    </head>
    <body>
      <div id="main-wrapper">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-white">
              <div class="panel-head text-center">
                <h1>INVOICE. {{ $data->code }}</h1>
              </div>
              <div class="panel-body">
                <div class="form-group">
                  <h3>Transaction ID</h3>
                  {{ $data->code }}
                </div>
                <div class="form-group">
                  <h3>Customer Name</h3>
                  {{ $data->customer->name }}
                </div>
                <div class="form-group">
                  <h3>Total Transaction</h3>
                  Rp. {{ number_format($data->total,'0','.',',') }}
                </div>
                <div class="form-group">
                  <h3>Transaction Date</h3>
                  {{ $data->transaction_date }}
                </div>
                <div class="form-group">
                  <h3>Payment Method</h3>
                  {{ $data->payment_method }}
                </div>
                <div class="form-group">
                  <h3>Instalment</h3>
                  {{ ($data->instalment === 1) ? 'Yes' : 'No' }}
                </div>
                <div class="form-group">
                  <h3>Transaction Details</h3>
                  <table class="table table-bordered">
                    <tr>
                      <th>No.</th>
                      <th>Item</th>
                      <th>Description</th>
                      <th>Price</th>
                    </tr>
                    @foreach($data->transaction_lines as $k => $v)
                      <tr>
                        <td>{{ $k + 1 }}</td>
                        <td>{{ $v->item_code }}</td>
                        <td>{{ $v->description }}</td>
                        <td>Rp. {{ number_format($v->price,'0','.',',') }}</td>
                      </tr>
                    @endforeach
                    @if(!empty($data->transaction_lines))
                      <tr>
                        <th colspan="3" classs="text-right">Total</th>
                        <th>Rp. {{ number_format($data->total,'0','.',',') }}</th>
                      </tr>
                    @endif
                  </table>
                </div>
                <div class="row btn-print">
                    <div class="col-md-12">
                      <a onclick="window.print()" class="btn btn-default">Print</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/.row -->
    </body>
</html>