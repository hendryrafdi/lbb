@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">Students</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li class="active">Student</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-white">
              <div class="panel-heading">
                  <a class="btn btn-success m-b-sm" href="{{ route('admin.student.create') }}">Add New Student</a>
                  <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-box">Import</a>
                  <a class="btn btn-primary" href="{{ route('admin.student.download') }}">Export</a>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
                  @endif
                  <table id="data-table" class="display table dataTable">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>NISN</th>
                      <th>Photo</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Birth Date</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                      <th>No</th>
                      <th>NISN</th>
                      <th>Photo</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Birth Date</th>
                      <th>Action</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div><!-- Row -->
</div><!-- Main Wrapper -->
@endsection

@section('modals')
<div class="modal fade" id="modal-box">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import Data</h4>
      </div>
      <form method="post" action="" id="importdropzone" class="dropzone" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="fallback">
            <input name="file" type="file" multiple />
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>	
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>	
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.css') }}">
<link href="{{ asset('assets/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
<!-- Sweet Alert -->
<script src="{{ asset('assets/plugins/datatables/js/jquery.datatables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script>
function init() {
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        destroy: true,
        ajax: '{!! route('admin.student.data') !!}',
        columns: [
          { data: 'rownum', name: 'rownum'},
          { data: 'nisn', name: 'nisn' },
          { data: 'photo', name: 'photo' },
          { data: 'first_name', name: 'first_name' },
          { data: 'last_name', name: 'last_name' },
          { data: 'birth_date', name: 'birth_date' },
          { data: "action", orderable: false, searchable: false, class: "text-left" }
        ],
        fnDrawCallback: () => {
          $(".delete").on('click', onDelete);
        },
        initComplete: () => {
          $(".delete").on('click', onDelete);
        }
      });
  }

  init();

  //---------- SWEET ALERT ------------//
  function onDelete(data) {
    data.preventDefault();
    return swal({
      title: "Are you sure?",
      text: "Your will not be able to recover this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    }, () => {
      let url = '{{ route("admin.student.destroy", [ "student" => ":id"]) }}';
      url = url.replace(':id', $(this).data('id'));
      $.ajax({
        url: url,
        type: 'json',
        data: '_token={{ csrf_token() }}',
        method: 'DELETE',
        error: () => {
          swal("Error!", "Failed to delete data student. Please try again later.", "error");
        },
        success: () => {
          swal("Deleted!", "Data student has been deleted.", "success");
          init();
        }
      });
    });
  }

Dropzone.autoDiscover = false;
//----- DROPZONE -----//
var myDropzone = new Dropzone("form#importdropzone", {
  url: "{{ route('admin.student.upload') }}",
  acceptedFiles: ".xls, .xlsx, .jpg, .png, .jpeg",
  error: function(file, response) {
    $('#modal-box').modal('hide');
    myDropzone.removeAllFiles();
    swal("Error!", response || "Failed to upload data student. Please try again later.", "error");
  },
  success: function(file, response){
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        $('#modal-box').modal('hide');
        myDropzone.removeAllFiles();
        swal("Uploaded!", "Data student has been uploaded.", "success");
        init();
      }
    });
  }
});
</script>
@endsection
