@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">User Detail</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.student.index') }}">Student</a></li>
            <li class="active">{{ $data->nisn }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          <div class="form-group">
            <h3>NISN</h3>
            {{ $data->nisn }}
          </div>
          <div class="form-group">
            <h3>First Name</h3>
            {{ $data->first_name }}
          </div>
          <div class="form-group">
            <h3>Last Name</h3>
            {{ $data->last_name }}
          </div>
          <div class="form-group">
            <h3>School</h3>
            {{ $data->school->name or '-' }}
          </div>
          <div class="form-group">
            <h3>Photo</h3>
            @if(isset($data) && !empty($data->photo))
              <div class="input-group">
                <a href="{{ asset('uploads/photo/'. $data->photo) }}">
                  <img class="thumbnail" src="{{ asset('uploads/photo/200_200_'. $data->photo) }}" />
                </a>
              </div>
            @endif
          </div>
          <div class="form-group">
            <h3>Gender</h3>
            {{ $data->gender === 'M' ? 'Male' : 'Female' }}
          </div>
          <div class="form-group">
            <h3>Birth Date</h3>
            {{ $data->birth_date }}
          </div>
          <div class="form-group">
            <h3>Location</h3>
            {{ $data->slh_location }}
          </div>
          <div class="form-group">
            <h3>Academy</h3>
            {{ $data->academy }}
          </div>
          <div class="form-group">
            <h3>Grade</h3>
            {{ $data->grade }}
          </div>
          <div class="form-group">
            <h3>Dream</h3>
            {{ $data->dream }}
          </div>
          <div class="form-group">
            <h3>Sponsorship Status</h3>
            {{ ($data->is_sponsored) ? 'Yes' : 'No' }}
          </div>
          <div class="form-group">
            <h3>Parent Background (EN)</h3>
            {!! $data->parent_background_en !!}
          </div>
          <div class="form-group">
            <h3>Parent Background (ID)</h3>
            {{ $data->parent_background_id or '-' }}
          </div>
          <div class="form-group">
            <hr />
            <h3>Documents</h3>
            <table class="table table-bordered">
              <tr>
                <th>Period</th>
                <th>Thank You Letter</th>
                <th>First Report Card</th>
                <th>Second Report Card</th>
              </tr>
              @foreach($data->documents as $k => $v)
                <tr>
                  <td>{{ $v->period }}</td>
                  <td>
                    @if(!empty($v->file_1))
                      <a href="{{ !empty($v->file_1) ? asset('uploads/documents/' . $v->file_1) : '-' }}">Download</a>
                    @else
                      -
                    @endif
                  </td>
                  <td>
                    @if(!empty($v->file_2))
                      <a href="{{ !empty($v->file_2) ? asset('uploads/documents/' . $v->file_2) : '-' }}">Download</a>
                    @else
                      -
                    @endif
                  </td>
                  <td>
                    @if(!empty($v->file_3))
                      <a href="{{ !empty($v->file_3) ? asset('uploads/documents/' . $v->file_3) : '-' }}">Download</a>
                    @else
                      -
                    @endif
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
          <div class="form-group">
            <hr />
            <h3>School Histories</h3>
            <table class="table table-bordered">
              <tr>
                <th>Period</th>
                <th>School</th>
              </tr>
              @foreach($data->schools as $k => $v)
                <tr>
                  <td>{{ $v->period }}</td>
                  <td>{{ $v->school->name }}</td>
                </tr>
              @endforeach
            </table>
          </div>
          <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.student.index') }}" class="btn btn-default">&laquo; Back</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/.row -->
@endsection