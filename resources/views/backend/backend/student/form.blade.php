@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Student' : 'Create New Student' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.student.index') }}">Student</a></li>
            <li class="active">{{ isset($data) ? 'Update Student' : 'Create New Student' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-heading clearfix">
            <h3 class="panel-title">Student Information</h3>
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#personal" role="tab" data-toggle="tab">Personal</a></li>
                    <li role="presentation"><a href="#sponsorship" role="tab" data-toggle="tab">Sponsorship Status</a></li>
                    <li role="presentation"><a href="#documents" role="tab" data-toggle="tab">Documents</a></li>
                </ul>
                <!-- Tab panes -->
                <form role="form" method="POST" enctype="multipart/form-data" action="{{ isset($data) ? route('admin.student.update', ['student' => $data->id]) : route('admin.student.store') }}">
                  @if(isset($data))
                    <input name="_method" type="hidden" value="PUT">
                  @endif
                  {{ csrf_field() }}
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active fade in" id="personal">
                      @include('backend.student.inc.personal')
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="sponsorship">
                      @include('backend.student.inc.sponsorship')
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="documents">
                      @include('backend.student.inc.documents')
                    </div>
                    <hr />
                    <div class="row">
                      <div class="col-md-12">
                        <a href="{{ route('admin.student.index') }}" class="btn btn-default">&laquo; Back</a>
                        <button type="submit" class="pull-right btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
            </div>
        </div>
      </div>
    </div> 
  </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/summernote-master/summernote.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/summernote-master/summernote.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/dropzone.min.js') }}"></script>
<script>
  $(document).ready(function() {
    $('.date-picker').datepicker({ format: 'yyyy-mm-dd', autoclose: true });
    $('.parent_background_id').summernote({
      height: 200
    });
    $('.parent_background_en').summernote({
      height: 200
    });
  });
</script>
@endsection