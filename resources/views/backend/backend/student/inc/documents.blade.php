<div class="form-horizontal">
  <div class="form-document">
    <div class="form-group">
        <label for="period-1" class="col-sm-2 control-label">Which academic year are the documents for?</label>
        <div class="col-sm-10">
            <select class="form-control" name="period">
              <option value="2017-18">2017-18</option>
              <option value="2018-19">2018-19</option>
              <option value="2019-20">2019-20</option>
              <option value="2020-21">2020-21</option>
              <option value="2021-22">2021-22</option>
              <option value="2022-23">2022-23</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="period-1" class="col-sm-2 control-label">Thank You Letter</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="file_1" />
        </div>
    </div>
    <div class="form-group">
        <label for="period-1" class="col-sm-2 control-label">First Report Card</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="file_2" />
        </div>
    </div>
    <div class="form-group">
        <label for="period-1" class="col-sm-2 control-label">Second Report Card</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="file_3" />
        </div>
    </div>
  </div>
  @if(isset($data))
    <div class="form-group">
      <h3>Documents</h3>
      <table class="table table-bordered">
        <tr>
          <th>Period</th>
          <th>Thank You Letter</th>
          <th>First Report Card</th>
          <th>Second Report Card</th>
        </tr>
        @foreach($data->documents as $k => $v)
          <tr>
            <td>{{ $v->period }}</td>
            <td>
              @if(!empty($v->file_1))
                <a href="{{ !empty($v->file_1) ? asset('uploads/documents/' . $v->file_1) : '-' }}">Download</a>
              @else
                -
              @endif
            </td>
            <td>
              @if(!empty($v->file_2))
                <a href="{{ !empty($v->file_2) ? asset('uploads/documents/' . $v->file_2) : '-' }}">Download</a>
              @else
                -
              @endif
            </td>
            <td>
              @if(!empty($v->file_3))
                <a href="{{ !empty($v->file_3) ? asset('uploads/documents/' . $v->file_3) : '-' }}">Download</a>
              @else
                -
              @endif
            </td>
          </tr>
        @endforeach
      </table>
    </div>
  @endif
</div>