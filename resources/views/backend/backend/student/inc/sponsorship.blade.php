<div class="form-group">
  <h3>Is waiting for a donor?</h3>
  <p><i>
    Set to NO to remove him/her from the sponsorship listing. 
    By default, all students are waiting for donors and will automatically be deactivated once the find donors. 
    Override only for administrative purposes.
  </i></p>
  <select name="is_sponsored" class="form-control">
    <option {{ ((isset($data) && $data->is_sponsored == '1') or old("is_sponsored") == '1') ? 'selected' : '' }} value="1">Yes</option>
    <option {{ ((isset($data) && $data->is_sponsored == '0') or old("is_sponsored") == '0') ? 'selected' : '' }} value="0">No</option>
  </select>
</div>