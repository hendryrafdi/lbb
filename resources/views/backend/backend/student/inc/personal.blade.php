<div class="form-group">
  <label for="nisn">NISN</label>
  <input type="text" name="nisn" class="form-control" id="nisn" placeholder="Enter NISN" value="{{ $data->nisn or old('nisn') }}">
</div>
<div class="form-group">
  <label for="first_name">First Name</label>
  <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First Name" value="{{ $data->first_name or old('first_name') }}">
</div>
<div class="form-group">
  <label for="last_name">Last Name</label>
  <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="{{ $data->last_name or old('last_name') }}">
</div>
<div class="form-group">
  <label for="school_code">School</label>
  <select name="school_code" class="form-control">
    @foreach($schools as $k => $v)
      <option {{ ((isset($data) && $data->school_code === $k) or old("school_code") === $k) ? 'selected' : '' }} value="{{ $k }}">{{ $v }}</option>
    @endforeach
  </select>
</div>
<div class="form-group">
  <label for="photo">Photo</label>
  @if(isset($data) && !empty($data->photo))
  <div class="input-group">
    <a href="{{ asset('uploads/photo/'. $data->photo) }}">
      <img class="thumbnail" src="{{ asset('uploads/photo/200_200_'. $data->photo) }}" />
    </a>
  </div>
  @endif
  <input name="photo" class="form-control" id="file" type="file" />
</div>
<div class="form-group">
  <label for="gender">Gender</label>
  <div class="input-group input-append">
    <input type="radio" name="gender" id="male" value="M" {{ ((isset($data) && $data->gender == 'M') or old('gender') == 'M') ? 'checked' : '' }}> <label for="male">Male</label>
    <input type="radio" name="gender" id="female" value="F" {{ ((isset($data) && $data->gender == 'F') or old('gender') == 'F') ? 'checked' : '' }}> <label for="female">Female</label>
  </div>
</div>
<div class="form-group">
  <label for="birth_date">Birth Date</label>
  <div class="input-group input-append bootstrap-timepicker">
      <input type="text" readonly="readonly" name="birth_date" class="form-control date-picker" id="birth_date" placeholder="Enter Birth Date" value="{{ $data->birth_date or old('birth_date') }}">
      <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
  </div>
</div>
<div class="form-group">
  <label for="slh_location">Location</label>
  <input type="text" name="slh_location" class="form-control" id="slh_location" placeholder="Enter Location" value="{{ $data->slh_location or old('slh_location') }}">
</div>
<div class="form-group">
  <label for="academy">Academy</label>
  <input type="text" name="academy" class="form-control" id="academy" placeholder="Enter Academy" value="{{ $data->academy or old('academy') }}">
</div>
<div class="form-group">
  <label for="grade">Grade</label>
  <input type="text" name="grade" class="form-control" id="grade" placeholder="Enter Grade" value="{{ $data->grade or old('grade') }}">
</div>
<div class="form-group">
  <label for="dream">Dream</label>
  <input type="text" name="dream" class="form-control" id="dream" placeholder="Enter Dream" value="{{ $data->dream or old('dream') }}">
</div>
<div class="form-group">
  <label for="parent_background_en">Parent Background (EN)</label>
  <textarea name="parent_background_en" class="parent_background_en">{{ $data->parent_background_en or old('parent_background_en') }}</textarea>
</div>
<div class="form-group">
  <label for="parent_background_id">Parent Background (ID)</label>
  <textarea name="parent_background_id" class="parent_background_id">{{ $data->parent_background_id or old('parent_background_id') }}</textarea>
</div>