@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Setting' : 'Create New Setting' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.setting.index') }}">Setting</a></li>
            <li class="active">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          <form role="form" method="POST"  action="{{ isset($data) ? route('admin.setting.update', ['setting' => $data->id]) : route('admin.setting.store') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">field</label>
                  <input type="text" name="field" class="form-control" id="field" readonly="true" placeholder="Enter School Name" value="{{ $data->field or old('field') }}">
                </div>
                <div class="form-group">
                  <label for="code">type</label>
                  <input type="text" name="type" class="form-control" id="type" readonly="true" placeholder="Enter type" value="{{ $data->type or old('type') }}">
                </div>
                <div class="form-group">
                  <label for="phone">value</label>
                  @if($data->type == 'text')
                    <input type="text" name="value" class="form-control" id="value" placeholder="Enter value" value="{{ $data->value or old('value') }}">
                  @else
                    <textarea name="value" class="form-control" id="value" placeholder="Enter value">{{ $data->value or old('value') }}</textarea>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.school.index') }}" class="btn btn-default">&laquo; Back</a>
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection