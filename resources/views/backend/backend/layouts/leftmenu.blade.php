<div class="page-sidebar sidebar">
  <div class="page-sidebar-inner slimscroll">
      <ul class="menu accordion-menu">
          <li {{ (str_is('admin.user.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.user.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-user"></span><p>Users</p></a></li>
          <li {{ (str_is('admin.school.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.school.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-home"></span><p>Schools</p></a></li>
          <li {{ (str_is('admin.item.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.item.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-book-open"></span><p>Items</p></a></li>
          <li {{ (str_is('admin.pricelist.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.pricelist.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-tag"></span><p>Price Lists</p></a></li>
          <li {{ (str_is('admin.student.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.student.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-graduation"></span><p>Students</p></a></li>
          <li {{ (str_is('admin.customer.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.customer.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-users"></span><p>Customers</p></a></li>
          <li {{ (str_is('admin.transaction.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.transaction.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-wallet"></span><p>Transactions</p></a></li>
          <li class="droplink {{ (str_is('admin.role.*', Route::currentRouteName()) || str_is('admin.permission.*', Route::currentRouteName()) ? 'active open' : '') }}"><a href="#" class="waves-effect waves-button"><span class="menu-icon icon-lock"></span><p>Roles</p><span class="arrow"></span></a>
              <ul class="sub-menu">
                  <li {{ (str_is('admin.role.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.role.index') }}">Roles</a></li>
                  <li {{ (str_is('admin.permission.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.permission.index') }}">Permissions</a></li>
              </ul>
          </li>
          <li {{ (str_is('admin.setting.*', Route::currentRouteName()) ? 'class=active' : '') }}><a href="{{ route('admin.setting.index') }}" class="waves-effect waves-button"><span class="menu-icon icon-lock"></span><p>Setting</p></a>
          </li>
      </ul>
  </div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->