<div class="top-menu">
  <ul class="nav navbar-nav navbar-left">
      <li>		
          <a href="javascript:void(0);" class="sidebar-toggle"><i class="icon-arrow-left"></i></a>
      </li>
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="icon-settings"></i>
          </a>
          <ul class="dropdown-menu dropdown-md dropdown-list theme-settings" role="menu">
              <li class="li-group">
                  <ul class="list-unstyled">
                      <li class="no-link" role="presentation">
                          Fixed Header 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right fixed-header-check">
                          </div>
                      </li>
                  </ul>
              </li>
              <li class="li-group">
                  <ul class="list-unstyled">
                      <li class="no-link" role="presentation">
                          Fixed Sidebar 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right fixed-sidebar-check">
                          </div>
                      </li>
                      <li class="no-link" role="presentation">
                          Horizontal bar 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right horizontal-bar-check">
                          </div>
                      </li>
                      <li class="no-link" role="presentation">
                          Toggle Sidebar 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right toggle-sidebar-check">
                          </div>
                      </li>
                      <li class="no-link" role="presentation">
                          Compact Menu 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right compact-menu-check" checked>
                          </div>
                      </li>
                      <li class="no-link" role="presentation">
                          Hover Menu 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right hover-menu-check">
                          </div>
                      </li>
                  </ul>
              </li>
              <li class="li-group">
                  <ul class="list-unstyled">
                      <li class="no-link" role="presentation">
                          Boxed Layout 
                          <div class="ios-switch pull-right switch-md">
                              <input type="checkbox" class="js-switch pull-right boxed-layout-check">
                          </div>
                      </li>
                  </ul>
              </li>
              <li class="no-link"><button class="btn btn-default reset-options">Reset Options</button></li>
          </ul>
      </li>
  </ul>
  <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="user-name">{{ Auth::user()->name }}<i class="fa fa-angle-down"></i></span>
          </a>
          <ul class="dropdown-menu dropdown-list" role="menu">
              <li role="presentation"><a href="{{ route('admin.profile.index') }}"><i class="icon-user"></i>Profile</a></li>
              <li role="presentation" class="divider"></li>
              <li role="presentation">
                <form method="POST" action="{{ route('logout') }}">
                    {{ csrf_field() }}
                    <button class="btn-logout" id="logout"><i class="icon-key m-r-xs"></i>Sign out</button>
                </form>
              </li>
          </ul>
      </li>
      <!-- <li>
          <a href="javascript:void(0);" id="showRight">
              <i class="icon-bubbles"></i>
          </a>
      </li> -->
  </ul><!-- Nav -->
</div><!-- Top Menu -->