@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Item' : 'Create New Item' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.item.index') }}">Item</a></li>
            <li class="active">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          <form role="form" method="POST"  action="{{ isset($data) ? route('admin.pricelist.update', ['pricelist' => $data->id]) : route('admin.pricelist.store') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Start Year</label>
                  <select class="form-control" name="start_year">
                    <option {{ ((isset($data) && $data->start_year === '2017') or old("start_year") === '2017') ? 'selected' : '' }} value="2017">2017</option>
                    <option {{ ((isset($data) && $data->start_year === '2018') or old("start_year") === '2018') ? 'selected' : '' }} value="2018">2018</option>
                    <option {{ ((isset($data) && $data->start_year === '2019') or old("start_year") === '2019') ? 'selected' : '' }} value="2019">2019</option>
                    <option {{ ((isset($data) && $data->start_year === '2020') or old("start_year") === '2020') ? 'selected' : '' }} value="2020">2020</option>
                    <option {{ ((isset($data) && $data->start_year === '2021') or old("start_year") === '2021') ? 'selected' : '' }} value="2021">2021</option>
                    <option {{ ((isset($data) && $data->start_year === '2022') or old("start_year") === '2022') ? 'selected' : '' }} value="2022">2022</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">End Year</label>
                  <select class="form-control" name="end_year">
                    <option {{ ((isset($data) && $data->end_year === '2017') or old("end_year") === '2017') ? 'selected' : '' }} value="2017">2017</option>
                    <option {{ ((isset($data) && $data->end_year === '2018') or old("end_year") === '2018') ? 'selected' : '' }} value="2018">2018</option>
                    <option {{ ((isset($data) && $data->end_year === '2019') or old("end_year") === '2019') ? 'selected' : '' }} value="2019">2019</option>
                    <option {{ ((isset($data) && $data->end_year === '2020') or old("end_year") === '2020') ? 'selected' : '' }} value="2020">2020</option>
                    <option {{ ((isset($data) && $data->end_year === '2021') or old("end_year") === '2021') ? 'selected' : '' }} value="2021">2021</option>
                    <option {{ ((isset($data) && $data->end_year === '2022') or old("end_year") === '2022') ? 'selected' : '' }} value="2022">2022</option>
                  </select>
                </div>
                <div class="form-group">
                  <a style="margin-bottom: 10px;" class="btn add-row">+ Add School</a>
                  <table class="table-row table table-bordered">
                    <tr>
                      <th>School</th>
                      <th>Price</th>
                      <th width="10">Action</th>
                    </tr>
                    @if(isset($data) && !empty($data->lines))
                      @foreach($data->lines as $k => $lv)
                        <tr>
                          <td>
                            <select name="school_code[]" class="form-control">
                              @foreach($schools as $k => $v)
                                <option value="{{ $k }}" {{ ($lv->school_code == $k) ? 'selected' : '' }}>{{ $v }}</option>
                              @endforeach
                            </select>
                          </td>
                          <td><input type="number" name="price[]" required class="form-control" value="{{ $lv->price }}" /></td>
                          <td><a><i class="delete remove-row icon-trash"></i></a></td>
                        </tr>
                      @endforeach
                    @endif
                  </table>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.pricelist.index') }}" class="btn btn-default">&laquo; Back</a>
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
          <div class="hidden copy-row">
            <select name="school_code[]" class="form-control">
              @foreach($schools as $k => $v)
                <option value="{{ $k }}">{{ $v }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function() {
    $('.table-row').on('click', '.remove-row', function() {
      $(this).closest('tr').remove();
    });

    $('.add-row').on('click', function() {
      var html = $('.copy-row').html();
      $('.table-row tbody:last-child').append('<tr><td>' + html + '</td><td><input type="number" required name="price[]" class="form-control" value="" /></td><td><a><i class="delete remove-row icon-trash"></i></a></td></tr>');
    });
  });
</script>
@endsection