@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Item' : 'Create New Item' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.item.index') }}">Item</a></li>
            <li class="active">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          <form role="form" method="POST" enctype="multipart/form-data" action="{{ isset($data) ? route('admin.item.update', ['item' => $data->id]) : route('admin.item.store') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Item Name" value="{{ isset($data) ? $data->name : old('name') }}">
                </div>
                <div class="form-group">
                  <label for="name">Name (ID)</label>
                  <input type="text" name="name_id" class="form-control" id="name_id" placeholder="Enter Item Name (ID)" value="{{ isset($data) ? $data->name_id : old('name_id') }}">
                </div>
                <div class="form-group">
                  <label for="code">Code</label>
                  <input type="text" name="code" class="form-control" id="code" placeholder="Enter Item Code" value="{{ isset($data) ? $data->code : old('code') }}">
                </div>
                <div class="form-group">
                  <label for="is_active">Is Active</label>
                  <input type="checkbox" {{ ((isset($data) && $data->is_active == 1) or old('is_active') == 1) ? 'checked' : '' }} name="is_active" class="form-control" id="is_active" value="1" />
                </div>
                <div class="form-group">
                  <label for="picture">Picture</label>
                  @if(isset($data) && !empty($data->picture))
                  <div class="input-group">
                    <a href="{{ asset('uploads/items/'. $data->picture) }}">
                      <img class="thumbnail" src="{{ asset('uploads/items/'.$data->picture) }}" style="height: 200px; width: 200px;" />
                    </a>
                  </div>
                  @endif
                  <input name="picture" class="form-control" id="file" type="file" />
                </div>
                <div class="form-group">
                  <label for="item_type_id">Item Type</label>
                  <select name="item_type_id" id="item_type_id" class="form-control">
                    @foreach($item_types as $k => $v)
                      <option {{ ((isset($data) && $data->item_type_id === $k) or old("item_type_id") === $k) ? 'selected' : '' }} value="{{ $k }}">{{ $v }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group" id="donation">
                  <label for="price_list_id">Price List</label>
                  <select name="price_list_id" class="price_list_id form-control">
                    <option value=''>- Select Price List -</option>
                    @foreach($price_lists as $k => $v)
                      <option {{ ((isset($data) && $data->price_list_id === $v->id) or old("price_list_id") === $v->id) ? 'selected' : '' }} value="{{ $v->id }}">{{ $v->start_year . ' - ' . $v->end_year }}</option>
                    @endforeach
                  </select>
                  <div id="table-donation">
                    @if (isset($data->price_list) && !empty($data->price_list->lines))
                      <table class="table table-bordered" style="margin-top: 10px;">
                        <tr>
                          <th width="10">No.</th>
                          <th>School</th>
                          <th>Price</th>
                        </tr>
                        @foreach($data->price_list->lines as $k => $v)
                        <tr>
                          <td>{{ $k + 1 }}</td>
                          <td>{{ $v->school->name }}</td>
                          <td>Rp. {{ $v->price }}</td>
                        </tr>
                        @endforeach
                      </table>
                    @endif
                  </div>
                </div>
                <div class="form-group" id="item">
                  <label for="price">Price</label>
                  <input type="number" name="price" class="form-control price" id="price" placeholder="Enter Price" value="{{ isset($data) ? $data->price : old('price') }}">
                </div>
                <div class="form-group">
                  <label for="description_en">Description (EN)</label>
                  <textarea name="description_en" class="description_en">{{ isset($data) ? $data->description_en : old('description_en') }}</textarea>
                </div>
                <div class="form-group">
                  <label for="description_id">Description (ID)</label>
                  <textarea name="description_id" class="description_id">{{ isset($data) ? $data->description_id : old('description_id') }}</textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.item.index') }}" class="btn btn-default">&laquo; Back</a>
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/summernote-master/summernote.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/summernote-master/summernote.min.js') }}"></script>
<script>
  $(document).ready(function() {
    var donationValue = 1;
    var itemValue = 2;
    var tablePriceList = $("#table-donation");
    var itemTypeSelector = $("#item_type_id");
    var donationSelector = $("#donation");
    var priceListSelector = $(".price_list_id");
    var priceSelector = $(".price");
    var itemSelector = $("#item");
    var itemType = itemTypeSelector.val();
    // If donation
    if (itemType == donationValue) {
      donationSelector.show();
      itemSelector.hide();
      priceSelector.val('');
    } else {
      donationSelector.hide();
      priceListSelector.val('');
      itemSelector.show();
    }

    itemTypeSelector.on('change', function() {
      if ($(this).val() == donationValue) {
        donationSelector.show();
        itemSelector.hide();
      } else {
        tablePriceList.html('');
        donationSelector.hide();
        itemSelector.show();
      }

      priceListSelector.val('');
      priceSelector.val('');
    });

    priceListSelector.on('change', function() {
      var pricelistId = $(this).val();
      $.ajax({
        url: "{{ route('admin.pricelistline.pricelist') }}/" + pricelistId,
        method: 'GET',
        success: function (data) {
          var dataCount = data.length;
          var html = '<table style="margin-top:10px;" class="table table-bordered">';
          html += '<tr>';
          html += '<th>No.</th>';
          html += '<th>School</th>';
          html += '<th>Price</th>';
          html += '</tr>';
          for (var x = 0; x < dataCount; x += 1) {
            html += '<tr>';
            html += '<td width="10">' + (x + 1) + '</td>';
            html += '<td>' + data[x].school.name + '</td>';
            html += '<td>Rp. ' + data[x].price + '</td>';
            html += '</tr>';
          }
          html += '</table>';

          tablePriceList.html(html);
        }
      });
    });
    
    $('.description_id').summernote({
      height: 200
    });
    $('.description_en').summernote({
      height: 200
    });
  });
</script>
@endsection