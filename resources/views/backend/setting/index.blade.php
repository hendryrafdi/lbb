@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">Schools</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li class="active">School</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-white">
              <div class="panel-body">
                  <div class="table-responsive">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                      <table id="data-table" class="display table dataTable" style="width: 100%; cellspacing: 0;">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>field</th>
                            <th>type</th>
                            <th>value</th>
                            <th>action</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>No</th>
                            <th>field</th>
                            <th>type</th>
                            <th>value</th>
                            <th>action</th>
                          </tr>
                        </tfoot>
                      </table>  
                  </div>
              </div>
          </div>
      </div>
  </div><!-- Row -->
</div><!-- Main Wrapper -->
@endsection

@section('styles')
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables.min.css') }}" rel="stylesheet" type="text/css"/>	
<link href="{{ asset('assets/plugins/datatables/css/jquery.datatables_themeroller.css') }}" rel="stylesheet" type="text/css"/>	
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('assets/plugins/datatables/js/jquery.datatables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/dist/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    function init() {
        let table = $('#data-table').DataTable({
          processing: true,
          serverSide: true,
          responsive: true,
          destroy: true,
          ajax: '{!! route('admin.setting.data') !!}',
          columns: [
            { data: 'rownum', name: 'rownum'},
            { data: 'field', name: 'field' },
            { data: 'type', name: 'type' },
            { data: 'value', name: 'value' },
            { data: "action", orderable: false, searchable: false, class: "text-left" }
          ],
          fnDrawCallback: () => {
            $(".delete").on('click', onDelete);
          },
          initComplete: () => {
            $(".delete").on('click', onDelete);
          }
        });
    }

    init();

    //---------- SWEET ALERT ------------//
    function onDelete(data) {
        data.preventDefault();
        return swal({
          title: "Are you sure?",
          text: "Your will not be able to recover this data!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        }, () => {
          let url = '{{ route("admin.school.destroy", [ "school" => ":id"]) }}';
          url = url.replace(':id', $(this).data('id'));
          $.ajax({
            url: url,
            type: 'json',
            data: '_token={{ csrf_token() }}',
            method: 'DELETE',
            error: () => {
              swal("Error!", "Failed to delete data school. Please try again later.", "error");
            },
            success: () => {
              swal("Deleted!", "Data school has been deleted.", "success");
              init();
            }
          });
        });
    }
});
</script>
@endsection
