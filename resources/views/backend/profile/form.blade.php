@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ Auth::user()->name }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li class="active">Update Profile</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif

          <form role="form" method="POST" action="{{ route('admin.profile.update') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="{{ isset($data) ? $data->name : old('name') }}">
                </div>
                <div class="form-group">
                  <label for="email">Email address</label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ isset($data) ? $data->email : old('email') }}">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="password_confirmation">Password Confirmation</label>
                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation">
                </div>
              </div>
            </div>

            <!--/.col (left) -->
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
            <!--/.row -->
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset("/bower_components/select2/dist/css/select2.min.css") }}">
<!-- Datepicker -->
<link rel="stylesheet" href="{{ asset("/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/plugins/iCheck/all.css") }}">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{ asset("/bower_components/admin-lte/plugins/iCheck/icheck.min.js") }}"></script>
<!-- Select2 -->
<script src="{{ asset("/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
<!-- Datepicker -->
<script src="{{ asset("/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}"></script>

<script>
$(document).ready(function() {
  $('#password').val('');

  //------------- CUSTOM PHOTO ---------------//
  $('#photo_browser').on('click', function(e){
      e.preventDefault();
      $('#photo').click();
  });

  $('#photo').on('change', function(){
      $('#photo_path').val($(this).val());
  });

  $('#photo_path').on('click', function(){
      $('#photo_browser').click();
  });

  //------------- CUSTOM ID CARD PHOTO ---------------//
  $('#idcard_photo_browser').on('click', function(e){
      e.preventDefault();
      $('#idcard_photo').click();
  });

  $('#idcard_photo').on('change', function(){
      $('#idcard_photo_path').val($(this).val());
  });

  $('#idcard_photo_path').on('click', function(){
      $('#idcard_photo_browser').click();
  });

  //------------- CUSTOM FAMILY CARD PHOTO ---------------//
  $('#family_card_photo_browser').on('click', function(e){
      e.preventDefault();
      $('#family_card_photo').click();
  });

  $('#family_card_photo').on('change', function(){
      $('#family_card_photo_path').val($(this).val());
  });

  $('#family_card_photo_path').on('click', function(){
      $('#family_card_photo_browser').click();
  });

  //------------- CUSTOM CHILD PHOTO ---------------//
  $('#photo_browser_child').on('click', function(e){
      e.preventDefault();
      $('#photo_child').click();
  });

  $('#photo_child').on('change', function(){
      $('#photo_path_child').val($(this).val());
  });

  $('#photo_path_child').on('click', function(){
      $('#photo_browser_child').click();
  });

  //----------- DATEPICKER -----------//
  $('#birth_date').datepicker({
    autoclose: true
  });

  $('#birth_date_child').datepicker({
    autoclose: true
  });

  //----------- SELECT 2 ------------//
  $('#health_blood_type').select2({
    placeholder: "-- Select blood type --",
    allowClear: true
  });

  $('#user_type').select2({
    placeholder: "-- Select user type --",
    allowClear: true
  });

  $('#cloth_size').select2({
    placeholder: "-- Select cloth size --",
    allowClear: true
  });

  $('#education_level').select2({
    placeholder: "-- Select education level --",
    allowClear: true
  });

  $('#id_territories').select2({
    placeholder: "-- Select territory --",
    allowClear: true
  });

  //------------ MINIMAL ------------//
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });

  //------------ FORM INPUT ADD CHILD -------------//
  $('.table-child').on('click', '.child-remove', function(e) {
    e.preventDefault();

    $(this).closest('tr').remove();
  });

  //---------- SWEET ALERT ------------//
  function onDelete(data) {
    return swal({
      title: "Are you sure?",
      text: "Your will not be able to recover this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    }, () => {
      let url = '{{ route("admin.user.destroy", [ "user" => ":id"]) }}';
      url = url.replace(':id', $(this).data('id'));
      $.ajax({
        url: url,
        type: 'json',
        data: '_token={{ csrf_token() }}',
        method: 'DELETE',
        error: () => {
          swal("Error!", "Failed to delete data user. Please try again later.", "error");
        },
        success: () => {
          swal("Deleted!", "Data user has been deleted.", "success");
          init();
        }
      });
    });
  }
});
</script>
@endsection
