@extends('backend.layouts.app')

@section('breadcrumb')
<div class="page-title">
    <div class="page-breadcrumb">
        <h3 class="breadcrumb-header">{{ isset($data) ? 'Update Permission' : 'Create New Permission' }}</h3>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
            <li><a href="{{ route('admin.permission.index') }}">Permission</a></li>
            <li class="active">{{ isset($data) ? 'Update Row' : 'Create New Row' }}</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          <form role="form" method="POST" enctype="multipart/form-data" action="{{ isset($data) ? route('admin.permission.update', ['permission' => $data->id]) : route('admin.permission.store') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="{{ isset($data) ? $data->name : old('name') }}">
                </div>
                <div class="form-group">
                  <label for="display_name">Display Name</label>
                  <input type="text" name="display_name" class="form-control" id="display_name" placeholder="Enter Display Name" value="{{ isset($data) ? $data->display_name : old('display_name') }}">
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <input type="text" name="description" class="form-control" id="description" placeholder="Enter Description" value="{{ isset($data) ? $data->description : old('description') }}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a href="{{ route('admin.permission.index') }}" class="btn btn-default">&laquo; Back</a>
                <button type="submit" class="pull-right btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection