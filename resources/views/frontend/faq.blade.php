@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>
  <header class="site-header common-static-header">
    <div data-stellar-background-ratio="0.25" style="background-image: url({{URL::to('/')}}/assets/images/standingline.png);" class="parallax-bg"></div>
    <div class="container pull-left">
      <p class='header-faq'>
        {{trans('lang.HELPFUL INFORMATION')}}
      </p>
    </div>
    <div class="container pull-left">
      <p class='header-caption'>
        {{trans('lang.Some frequently asked questions about our program')}}
      </p>
    </div>
  </header>
  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2" align='left'>
            <h2 class='blue'>{{trans('lang.Frequently Asked Questions')}}</h2>
            <p class="faq-caption" align="left">{{trans('lang.Here are some common questions related to donations and students scholarship')}}</p>
                        
              <div class="accordion-3 style-2">
                <div id="accordion-4" class="panel-group">
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel1" class="panel-title">
                        <h5 class="blue">{{trans('lang.What is the Lentera Bagi Bangsa Program')}}</h5></a></div>
                    <div id="panel1" class="panel-collapse collapse in">
                      <div class="panel-body">{{trans('lang.Lentera Bagi Bangsa is an initiative by the Pelita Harapan Education Foundation (YPPH) to support students from economically underprivileged families within the Sekolah Lentera Harapan (SLH) network by providing scholarships')}}</div>
                    </div>
                  </div>                  
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel2" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.What is Lentera Bagi Bangsa’s mission? How does LBB accomplish it')}}</h5></a></div>
                    <div id="panel2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        {{trans('lang.Lentera Bagi Bangsa is helping one child at a time to quality education at little or no cost to the students who need it most. Throughout the years, our foundation has been blessed to have many join the SLH mission by providing scholarships to support students. As we hope to serve even more students in Indonesia, we need your help to do so')}}
                        @if(trans('lang.helping one child at a time'))
                        <b> {{trans('lang.helping one child at a time')}} </b>{{trans('lang.to quality education at little or no cost to the students who need it most. Throughout the years, our foundation has been blessed to have many join the SLH mission by providing scholarships to support students. As we hope to serve even more students in Indonesia, we need your help to do so')}}
                        @endif
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel3" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Why does this program only support students under the Sekolah Lentera Harapan network')}}</h5></a></div>
                    <div id="panel3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">{{trans('lang.This program was specifically established by YPPH to support the SLH mission in serving students from economically underprivileged families')}}</div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel4" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.In which part of Indonesia can I support a student')}}</h5></a></div>
                    <div id="panel4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.Students in the LBB program come from all over Indonesia and you are free to support any waiting student. Click here to see where our schools are located')}}
                          <a href="{{route('front.search')}}" style="color:blue;">{{trans('lang.here')}}</a> {{trans('lang.to see where our schools are located')}}
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel5" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Will my grant go directly and fully to the child’s parents')}}</h5></a></div>
                    <div id="panel5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">{{trans('lang.As this is a scholarship program, 100% of your grant will be allocated for the SLH of the student you support')}}</div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel6" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Can I send cash as a gift directly to the parents of the child I support')}}</h5></a></div>
                    <div id="panel6" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.Please review our Student Protection Policy')}}
                          <i><a href="{{route('front.protectionPolicy')}}" style="color:blue;">{{trans('lang.Student Protection Policy')}}</a></i>
                          {{trans('lang.kami')}}
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel7" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.What happens if the parents of the child I support decide to discontinue enrollment in the Sekolah Lentera Harapan schools network')}}</h5></a></div>
                    <div id="panel7" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">{{trans('lang.If your recipient is no longer a student at SLH (transfer, drop out, death), our staff will notify you immediately and process transferring your scholarship to a different student')}}</div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel8" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Why does supporting a student require at least a one-year commitment? What happens to my money if I start supporting halfway through the academic year')}}</h5></a></div>
                    <div id="panel8" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">{{trans('lang.Students are selected for the LBB program on an academic year basis. Upon becoming a LBB recipient, students only pay partial tuition based on the established agreement between the school and the parents as LBB commits to support the remaining cost. Therefore, grants can be received at any time to cover the students’ tuition cost for that academic year')}}</div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel9" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Will other people be able to support the same student I am funding')}}</h5></a></div>
                    <div id="panel9" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">{{trans('lang.Once you have committed to support a student, he/she will be taken out of the system for the duration of your commitment. Therefore, no student will receive more than one scholarship at the same time')}}</div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel10" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.What materials will I receive when I support a student')}}</h5></a></div>
                    <div id="panel10" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.When you commit to support a student, you will receive the SLH quarterly newsletter, a personal thank you letter from the student, and semester report cards in July and December of the academic year')}}
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel11" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Can I visit the student I support')}}</h5></a></div>
                    <div id="panel11" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.Please review our Student Protection Policy')}}
                          <i><a href="{{route('front.protectionPolicy')}}" style="color:blue;">{{trans('lang.Student Protection Policy')}}</a></i>
                          {{trans('lang.kami')}}    
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel12" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Can I cancel my scholarship at any time')}}</h5></a></div>
                    <div id="panel12" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.You may not request to cancel your scholarship for the remainder of time committed. Supporting a child requires a one-year minimum commitment. Please read our Terms and Conditions')}}
                          <a href="{{route('front.tnc')}}" style="color:blue;"><i>{{trans('lang.Terms and Conditions')}}</i></a> {{trans('lang.yang berlaku')}}
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel13" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Are contributions to Lentera Bagi Bangsa tax deductible if I am based outside Indonesia')}}</h5></a></div>
                    <div id="panel13" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">{{trans('lang.We do not yet hold any certification to accommodate tax deductible contributions. This will depend on your country’s regulations and requirements')}}</div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel14" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.Can I simply make a money donation without providing a scholarship for a student or the gift catalog')}}</h5></a></div>
                    <div id="panel14" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.You can make a general contribution here')}}
                          <a href="{{route('front.gift')}}" style="color:blue"><i>{{trans('lang.contribution here')}}</i></a> {{trans('lang.untuk memberikan kontribusi umum')}}
                      </div>
                    </div>
                  </div>
                  
                  <!--<div class="panel">-->
                  <!--  <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel15" class="panel-title collapsed" aria-expanded="false">-->
                  <!--      <h5 class="blue">{{trans('lang.[One-Time Giving] What will my money do')}}</h5></a></div>-->
                  <!--  <div id="panel15" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">-->
                  <!--    <div class="panel-body">{{trans('lang.Contributions from one-time giving will support general SLH operational costs as needed, including LBB recipients not yet supported by a benefactor')}}</div>-->
                  <!--  </div>-->
                  <!--</div>-->
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel16" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.[Gift Catalog] What will my money do')}}</h5></a></div>
                    <div id="panel16" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.Contribution toward specific gifts will be allocated for purchasing the items upon request from any SLH at the discretion of YPPH')}}
                          <i>{{trans('lang.gifts')}} </i>{{trans('lang.akan dialokasikan untuk membeli barang-barang tersebut atas permintaan SLH dan persetujuan YPPH')}}
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel">
                    <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel17" class="panel-title collapsed" aria-expanded="false">
                        <h5 class="blue">{{trans('lang.[Gift Catalog] Why is there a minimum amount for each gift item')}}</h5></a></div>
                    <div id="panel17" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                          {{trans('lang.The minimum amount is the actual amount needed to purchase the gift')}}
                          <i>{{trans('lang.gifts')}} </i>{{trans('lang.tersebut')}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>{{ trans('lang.Contact Us') }}</h2>  
            
            <p class="contact-caption" align="justify"> 
              @if(session('language') == 'en')
              {{ trans('lang.Have questions? Contact us directly by phone at +62 811 894 7500 and by email at') }} <a class="blue" href="mailto:info@lenterabagibangsa.org">info@lenterabagibangsa.org</a>
              @else
              {{ trans('lang.Have questions? Contact us directly by') }}<br>
              {{ trans('lang.phone at +62 811 894 7500 and by email at') }} <a class="blue" href="mailto:info@lenterabagibangsa.org">info@lenterabagibangsa.org</a>
              @endif
            </p>
          </div>
        </div>
      </div>
    </header>
    
  </section>
  


  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>  
@stop
