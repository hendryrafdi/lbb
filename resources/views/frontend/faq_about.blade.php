<section class="white-background">
  <div class="container">
  <div class='row'>
    <div class="col-lg-3 col-lg-offset-1">
      <img src="{{URL::to('/')}}/assets/images/FAQ.png" width="100%" style='margin-top:-60px'>
    </div>
    <div class='col-lg-7 col-md-7'>
      
      <div class="accordion-3 style-2" align="justify">
        <div id="accordion-4" class="panel-group">

          <div class="panel">
            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel1" class="panel-title">
                <h5 class="blue">{{trans('lang.What is the Lentera Bagi Bangsa Program')}}</h5></a></div>
            <div id="panel1" class="panel-collapse collapse in">
              <div class="panel-body">{{trans('lang.Lentera Bagi Bangsa is an initiative by the Pelita Harapan Education Foundation (YPPH) to support students from economically underprivileged families within the Sekolah Lentera Harapan (SLH) network by providing scholarships')}}</div>
            </div>
          </div>                  
          
          <div class="panel">
            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel2" class="panel-title collapsed" aria-expanded="false">
                <h5 class="blue">{{trans('lang.What is Lentera Bagi Bangsa’s mission? How does LBB accomplish it')}}</h5></a></div>
            <div id="panel2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                {{trans('lang.Lentera Bagi Bangsa is helping one child at a time to quality education at little or no cost to the students who need it most. Throughout the years, our foundation has been blessed to have many join the SLH mission by providing scholarships to support students. As we hope to serve even more students in Indonesia, we need your help to do so')}}
                @if(trans('lang.helping one child at a time'))
                <b> {{trans('lang.helping one child at a time')}} </b>{{trans('lang.to quality education at little or no cost to the students who need it most. Throughout the years, our foundation has been blessed to have many join the SLH mission by providing scholarships to support students. As we hope to serve even more students in Indonesia, we need your help to do so')}}
                @endif
              </div>
            </div>
          </div>
          
        </div>
      </div>
      
      <p><a href="{{route('front.faq')}}"><b><u class='blue'>{{trans('lang.See More Question')}}</u></b></a></p>
      <p align='center'><a href="{{route('front.gift')}}"><button class='btn btn-green'> &oplus; {{trans('lang.Give Now')}} </button></a></p>
    </div>
  </div>    
  </div>
</section>