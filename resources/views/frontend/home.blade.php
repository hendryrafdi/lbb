@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-1-landingpage">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header home-sticky-nav-trigger">
    <div class="home-fw-slider style-7 caption-slider dir-nav parents-height">
      <div class="home-slider-item">
        <div style="background-image: url({{URL::to('/')}}/assets/images/home.png);" class="item-image">
          <div class="overlay">
            <div class="cell-vertical-wrapper">
              <div class="cell-middle">
                <div class="container">
                  <div class="caption-wrapper text-center">
                    <h1 class="caption">{{trans('lang.home caption2 1')}} <br>{{trans('lang.home caption2 2')}}</h1>
                    <p class="caption">{{trans('lang.home caption2 3')}}.</p>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!--end site header-->
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>{{trans('lang.home caption3 1')}}</h2>
            <p class="header-caption">{{trans('lang.home caption3 2')}}</p>
          </div>
        </div>
        <div class="row">
          <a class='btn btn-green' href="{{route('front.about')}}">{{trans('lang.Read More')}}</a> 
        </div>
      </div>
    </header>
  </section>
  
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>{{trans('lang.SLH\'s Approach to Transformational Education')}}</h2>            
          </div>
        </div>
      </div>
    </header>
    <div class="blog-lastest-post">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12">
            <div class="blog-item section-block-p">
              <p class='blue home-about-title'>{{trans('lang.Holistic')}}</p>
              <p class="home-about-detail">{{trans('lang.We aim to developed students academically, socially, physically, and spiritually, preparing them to be served leaders in their community')}}</p>
              
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="blog-item section-block-p">
              <p class='blue home-about-title'>{{trans('lang.With Reach')}}</p>
              <p class="home-about-detail">{{trans('lang.SLH is rapidly growing network of 111 school unit serving over 10.500 students across 14 province of indonesia')}}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="hidden-xs white-background">
    <div class="blog-lastest-post white-background">
      <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>{{trans('lang.Support Our Mission')}}</h2>
            <p class="blue"><b>{{trans('lang.Partner with us to bring')}}<br>{{trans('lang.quality holistic education to even more')}}<br>{{trans('lang.children and youth in Indonesia')}}.</b></p>
          </div>
      </div>
      <div class="container">
        <div class="col-md-12 col-xs-12 hidden-xs">
          <hr class="separator-hr">
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 schoolarship-icon" align="center">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <a href='{{route('front.donation')}}'><img src="{{URL::to('/')}}/assets/images/scholarship_top.png" class="img-responsive"></a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 gift-icon" align="center">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <a href='{{route('front.gift')}}'><img src="{{URL::to('/')}}/assets/images/gift_top.png" class="img-responsive"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="blog-lastest-post blue-background">
      <div class="container">
        <div class="col-md-12 col-xs-12 hidden-xs">
          <hr class="separator-hr">
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 schoolarship-icon" align="center" >
            <div class="row">
              <div class="col-md-12 col-xs-12" >
                <a href='{{route('front.donation')}}'><img src="{{URL::to('/')}}/assets/images/scholarship_bottom.png" class="img-responsive"></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12" >
                <div class="blog-item section-block-p">
                  <p><h1 class="blue">{{trans('lang.Donation')}}</h1></p>
                  <p class="home-about-detail">{{trans('lang.We believe that a little things you share, will occupy the biggest part of others\’ life.  We open donation to manifest a children’s dream to get better education')}}. </p>
                  <br>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                  <p style="margin-top: 16px;">
                    <a href='{{route('front.donation')}}'><button class='btn btn-green'>{{trans('lang.Donate')}}</button></a>
                  </p>
                </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 gift-icon" align="center" >
            <div class="row">
              <div class="col-md-12 col-xs-12" >
                <a href='{{route('front.gift')}}'><img src="{{URL::to('/')}}/assets/images/gift_bottom.png" class="img-responsive"></a>
              </div>
            </div>
            <div class="row">          
              <div class="col-md-12 col-xs-12" >
                <div class="blog-item section-block-p">
                  <p><h1 class="blue">{{trans('lang.Give a gift')}}</h1></p>
                  <p class="home-about-detail">{{trans('lang.Every gift that you give will invest a happiness to children. Your gift can be books, toys and uniform')}}.</p>
                  
                </div>
              </div>          
            </div> 
            <div class="row">
                <div class="col-md-12 col-xs-12">
                  <p class="top30">
                    <a href='{{route('front.gift')}}'><button class='btn btn-green'>{{trans('lang.Give Now')}}</button></a>
                  </p>
                </div>
            </div>
          </div>
        </div>
        
      </div>
      
      
    </div>
  </section>
  
  
  <section class="visible-xs">
    
    <div class="blog-lastest-post white-background">
      <div class="container">
        <div class="col-md-12 col-xs-12 hidden-xs">
          <hr class="separator-hr">
        </div>
        <div class="row">
          <div class="col-xs-12 schoolarship-icon" align="center">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <a href='{{route('front.donation')}}'><img src="{{URL::to('/')}}/assets/images/scholarship_top.png" class="img-responsive"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="blog-lastest-post blue-background">
      <div class="container">
        <div class="col-md-12 col-xs-12 hidden-xs">
          <hr class="separator-hr">
        </div>
        <div class="row">
          <div class="col-xs-12 schoolarship-icon" align="center">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <a href='{{route('front.donation')}}'><img src="{{URL::to('/')}}/assets/images/scholarship_bottom.png" class="img-responsive"></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="blog-item section-block-p">
                  <p><h1 class="blue">{{trans('lang.Donation')}}</h1></p>
                  <p class="home-about-detail">{{trans('lang.We believe that a little things you share, will occupy the biggest part of others\’ life.  We open donation to manifest a children’s dream to get better education')}}. </p>
                  <p class="top30">
                    <a href='{{route('front.donation')}}'><button class='btn btn-green'>{{trans('lang.Donate')}}</button></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
      
      
    </div>
      
    
    <div class="blog-lastest-post white-background">
      <div class="container">
        <div class="col-md-12 col-xs-12 hidden-xs">
          <hr class="separator-hr">
        </div>
        <div class="row">
          <div class="col-xs-12 gift-icon" align="center">
            <div class="row">
              <div class="col-xs-12">
                <a href='{{route('front.gift')}}'><img src="{{URL::to('/')}}/assets/images/gift_top.png" class="img-responsive"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="blog-lastest-post blue-background">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 gift-icon" align="center">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <a href='{{route('front.gift')}}'><img src="{{URL::to('/')}}/assets/images/gift_bottom.png" class="img-responsive"></a>
              </div>
            </div>
            <div class="row">          
              <div class="col-md-12 col-xs-12">
                <div class="blog-item section-block-p">
                  <p><h1 class="blue">{{trans('lang.Gift')}}</h1></p>
                  <p class="home-about-detail">{{trans('lang.Every gift that you give will invest a happiness to children. Your gift can be books, toys and uniform')}}.</p>
                  <p class="top30">
                    <a href='{{route('front.gift')}}'><button class='btn btn-green'>{{trans('lang.Give Now')}}</button></a>
                  </p>
                </div>
              </div>          
            </div>            
          </div>
        </div>
        
      </div>
      
      
    </div>
    
  </section>
  
  <section class="white-background height30">
  
  </section>  

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
