@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
    
  <form role="form" method="POST" action="">
  <!--site header-->
  <header class="site-header"></header>
  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12" align='center'>
            <h2 class="blue">Review Donation & Pay</h2>     
            <ol class="progress progress--medium">
              <li class="is-active" data-step="1" id='step1'>
                Registration
              </li>
              <li data-step="2" id='step2'>
                Payment Plan
              </li>
              <li data-step="3" id='step3'>
                Payment Method
              </li>
              <li data-step="4" class="progress__last" id='step4'>
                Review and Pay
              </li>
            </ol>
      <!--      <div class="progress">-->
      <!--          <div class="one primary-color"></div><div class="two primary-color"></div><div class="three no-color"></div>-->
      <!--    			<div class="progress-bar" style="width: 50%;"></div>-->
		    <!--</div>-->
          </div>
          
        </div>
        
        
        @if(count($session['cart']) == 0)
        <div class="row">
          <div class="col-lg-12 pull-right"><h1>{{trans('lang.cart is empty')}}</h1></div>
        </div>
        
        @else
        
        <div class="row">
          <div class="col-lg-4 pull-right">
                <div class="row" style="padding: 35px; background: #f4fafa;" align="left">
                    <div class="row">
                        <div class="col-lg-12"><h5>Basket Summary</h5></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Below is a summary of your contribution today.</p>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        @php
                          $total = 0;
                          $total_gift = 0;
                          $total_scholarship = 0;
                          $i = 0;
                          $able_recurement = true;
                        @endphp
                        @foreach($session['cart'] as $p => $cart)
                          @foreach($cart as $q => $item)
                            @if($item['code'] == 'SCH')
                            @php
                              $total_year = $item['qty'];
                              $text_year = (int)$total_year >1 ? ' years '  : ' year ';
                              $text_month = '(' . ($item['qty'] * 12) . ' months )';
                              $total_scholarship += ((int)$item['price'] * (int)$item['qty']) * 12;
                            @endphp
                            
                            <div class="row">
                              <div class="col-md-12 col-xs-12" align="left">
                                <b> {{$item['name']}}</b>
                              </div>
                            </div>
                            
                            <div class="row">
                              <div class="col-md-12 col-xs-12" align="left">
                                <table width="100%" class="white-background" border="0">
                                  <tr>
                                    <td width="50%" class="blue"> {{$item['student']['name']}}</td>
                                    <td width="50%" class="blue"><button class="btn btn-btn-outline-warning btn-xs" onClick="lbb.removeCart('{{$p}}', '{{$q}}');" ><i class="fa fa-trash"></i> Remove {{$item['student']['first_name']}} </button></td>
                                  </tr>
                                  <tr></tr>
                                  <tr>
                                    <td width="60%"><small class="text-muted">Tuition fee at SLH {{$item['student']['school']}} is</small><br><b>IDR {{number_format($item['price'],'0',',','.') }} /month </b></td>
                                    <td><b class="blue">IDR  {{number_format(($item['price'] * ($item['qty'] * 12)),'0',',','.')}} </b></td>
                                  </tr>
                                  <tr>
                                    <td width="60%"><small class="text-muted">You're supporting  {{$item['student']['name']}}  for </small></td>
                                  </tr>
                                  <tr>
                                    <td><b>{{$total_year}} {{$text_year}} {{$text_month}}</b></td>
                                  </tr>
                                  <tr>
                                    <td><a href="{{route('front.detail',['type'=>'donation','id'=>$item['student']['id']])}}" class="blue">See profile</a></td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                            
                            @else
                            @php  
                              $total_gift += (int)$item['price'] * (int)$item['qty'];
                              $able_recurement = false;
                            @endphp
                            
                              <div class="row">
                                <div class="col-md-12 col-xs-12" align="left">
                                  <b>{{$item['item_type']}}</b>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12 col-xs-12" align="left">
                                  <table width="100%" class="white-background">
                                    <tr>
                                      <td width="60%" class="blue">{{$item['name']}}</td>
                                      <td width="60%" class="blue" align='right'>
                                        <button class="btn btn-btn-outline-warning btn-xs" onClick="lbb.removeCart('{{$p}}', '{{$q}}');" ><i class="fa fa-trash"></i> Remove </button>
                                      </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                      <td width="60%"><b>IDR  {{number_format($item['price'] * $item['qty'],'0',',','.')}}</b></td>
                                    </tr>
                                  </table>
                                </div>
                              </div>
                            @endif
                          @endforeach
                        @endforeach
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4"><b>Gift Items</b></div>
                                <div class="col-lg-4">&nbsp;</div>
                                <div class="col-lg-4" align="right"><b class="blue">Rp {{number_format($total_gift,'0',',','.')}}</b></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6"><b>Student Scholarship</b></div>
                                <div class="col-lg-6" align="right"><b class="blue">Rp {{number_format($total_scholarship,'0',',','.')}}</b></div>
                            </div>
                            <div class="row">&nbsp;</div>
                            <?php $total = $total_gift + $total_scholarship; ?>
                            <div class="row">
                                <div class="col-lg-4"><h4 class="blue"><b>TOTAL</b></h4></div>
                                <div class="col-lg-2">&nbsp;</div>
                                <div class="col-lg-6" align="right"><h4 class="blue"><b>Rp {{number_format($total,'0',',','.')}} </b></h4></div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
          
          <div class="col-lg-8">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-white">
                        <div class="panel-heading" align="left">
                          <h4 class="blue"><a href="#collapseDonorInformation" data-toggle="collapse">1. Donor Information</a></h4>
                        </div>
                        <div class="panel-body" id="collapseDonorInformation">
                            
                          <p align="left">Please enter your contact information. All field are required.</p>
                            @if(isset($customer))
                              <input name="_method" type="hidden" value="PUT">
                            @endif
                            {{ csrf_field() }}
                            <div class="row" align="left">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">First Name</label>
                                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter First Name" value="{{ isset($customer) ? $customer->name : old('name') }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="last_name">Last Name</label>
                                  <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="{{ isset($customer) ? $customer->last_name : old('last_name') }}">
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="email">Email address</label>
                                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ isset($customer) ? $customer->email : old('email') }}">
                                </div>
                                <div class="form-group">
                                  <label for="address">Address</label>
                                  <input type="text" name="address" class="form-control" id="address" placeholder="Enter address" value="{{ isset($customer) ? $customer->address : old('address') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="city">City </label>
                                  <input type="text" name="city" class="form-control" id="city" placeholder="Enter city" value="{{ isset($customer) ? $customer->city : old('city') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="province">Province / State</label>
                                  <input type="text" name="state" class="form-control" id="state" placeholder="Enter province / state" value="{{ isset($customer) ? $customer->state : old('state') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="country">Country</label>
                                  <input type="text" name="country" class="form-control" id="country" placeholder="Enter country" value="{{ isset($customer) ? $customer->country : old('country') }}">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="postal_code">Postal Code</label>
                                  <input type="text" name="postal_code" class="form-control" id="postal_code" placeholder="Enter postal code" value="{{ isset($customer) ? $customer->postal_code : old('postal_code') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="name">Mobile Phone</label>
                                  <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter Your Phone Number" value="{{ isset($customer) ? $customer->phone : old('phone') }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="last_name">Gender</label><br>
                                    <div class="col-md-3"><input type="radio" name="gender" id='gender' value="m" @if($customer->gender == 'm'){{'checked'}} @endif >  Male<br></div>
                                    <div class="col-md-3"><input type="radio" name="gender" id='gender' value="f" @if($customer->gender == 'f'){{'checked'}} @endif> Female<br></div>
                                </div>
                              </div>

                            </div>
                
                            <!--/.col (left) -->
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" class="btn btn-green btn-md btn-block" data-toggle="collapse" data-target="#collapsePaymentPlan" aria-expanded="false" aria-controls="collapsePaymentPlan" onclick="goto(2)" id="btn-1">Next</button>
                                </div>
                              </div>
                            </div>
                            <!--/.row -->
                          </div>
                        </div>
                  </div>
              </div>
              <hr>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-white">
                          <div class="panel-heading" align="left">
                          <h4 class="blue">2. Scholarship Payment Plan</h4>
                        </div>
                        <div class="panel-body collapse" id="collapsePaymentPlan" align="left">
                          <p>Scholarship Payment Plan.</p>
                          <div class="row">
                              <div class="col-lg-12">
                                  <div class="row" style="line-height: 20px; padding-bottom: 10px;">
                                      <div class="col-lg-4">
                                          <b class="blue">One Time Payment</b>
                                      </div>
                                      <div class="col-lg-6">
                                          You will be paying the full scholarship amount of <b class="blue">Rp. {{number_format($total,'0',',','.')}}</b> today.
                                      </div>
                                      <div class="col-lg-2">
                                          <b class="blue"><input type="radio" name="payment-method" id='payment-method' value="fp" checked></b>
                                      </div>
                                  </div>
                                @if($able_recurement)
                                  <div class="row" style="line-height: 20px;">
                                      <div class="col-lg-4">
                                          <b class="blue">Monthly Payment</b><br>
                                          <small class="text-muted">Only with credit card</small>
                                      </div>
                                      <div class="col-lg-6">
                                          You will be charged on the first monday of every month starting today until the full scholarship amount is paid.
                                      </div>
                                      <div class="col-lg-2">
                                          <b class="blue"><input type="radio" name="payment-method" id='payment-method' value="rp"></b>
                                      </div>
                                  </div>
                                @endif
                              </div>
                              <div class="col-lg-12">
                                <div class="row">&nbsp;</div>    
                              </div>
                              
                              <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6"><a class="btn btn-green btn-md btn-block" data-toggle="collapse" data-target="#collapsePaymentPlan" aria-expanded="false" aria-controls="collapsePaymentPlan" style="background-color: #dddddd;" onclick="backto(1)" id="btn-cancel-2">Back</a></div>
                                    <div class="col-lg-6"><a class="btn btn-green btn-md btn-block" data-toggle="collapse" data-target="#collapsePaymentMethod" aria-expanded="false" aria-controls="collapsePaymentMethod" onclick="goto(3)" id="btn-2">Next</a></div>
                                </div>    
                              </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
              <hr>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-white">
                          <div class="panel-heading" align="left">
                          <h4 class="blue">3. Payment Method</h4>
                        </div>
                        <div class="panel-body collapse" id="collapsePaymentMethod" align="left">
                          <div class="row">
                              <div class="col-lg-12">
                                  <div class="row" style="line-height: 20px; padding-bottom: 10px;">
                                      <div class="col-lg-12">
                                          <p>Since your basket includes reccuring payment for student scholarship(s), you may only
                                          pay using credit card. If you do not a have credit card, please choose the <b>One-time Payment</b>
                                          plan in previous step.
                                          </p>
                                          <p>Choose a payment method:</p>
                                      </div>
                                  </div>
                                  <div class="row" style="line-height: 20px;" align="center">
                                      <div class="col-lg-4">
                                          <input type="radio" name="payment-type" id='payment-type' value="cc" checked><div class="col-lg-12"><img src="{{ asset('assets/images/visaMastercard.png')}}"><br><b class="blue">Credit Card</b></div>
                                      </div>
                                      <div class="col-lg-4">
                                          &nbsp;
                                      </div>
                                      <div class="col-lg-4">
                                          &nbsp;
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12">
                                  <div class="row">&nbsp;</div>
                              </div>
                              <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6"><a class="btn btn-green btn-md btn-block" data-toggle="collapse" data-target="#collapsePaymentMethod" aria-expanded="false" aria-controls="collapsePaymentMethod" style="background-color: #dddddd;" onClick="backto(2)">Back</a></div>
                                    <div class="col-lg-6"><a class="btn btn-green btn-md btn-block" onClick="lbb.showPaymentModal()">Next</a></div>
                                </div>    
                              </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
          
          
        </div>
        
        @endif
      </div>
    </header>
  </section>
  
  <div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <section>
          <div class="modal-body padding-zero" style="margin: -30px 0px -70px;">
            <div class="container-fluid" style="line-height: 20px;">
               <h3 class="blue"><b>Proceed to payment?</b></h3>
               <p>After clicking Pay Now, you will be directed to our payment service. You will not able to change your donation details once you continue to payment.</p>
               <p>By continuing, you agree to our<br>
               <b class="blue"><u><a href="">Terms and Conditions</a></u></b> and <b class="blue"><u><a href="">Policies</a></u></b></p>
               <div class="row" align="center" style="background-color: #e5e5e5; padding:20px;">
                   <div class="col-lg-6">
                     <h5><b><a class="btn btn-block" style="color: #e2843d; background-color:#e5e5e5; font-size: 20px;" onclick='lbb.closePaymentModal(); backto(3)'>No, edit payment details</a></b></h5>
                   </div>
                   <div class="col-lg-6"><a class="btn btn-green btn-block" style="font-size: 20px;" onclick='lbb.doPaymentCheckout()' id='payment-checkout-btn'> Yes, pay now <span class="fa fa-share-square-o"></span></a></div>
               </div>
            </div>
          </div>
          </section>
        </div>
      </div>
    </div>

</form>
  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
  
  function goto($step){
    if($step == '2'){
      $("#btn-1").hide();
      $("#step1").removeClass('is-active').addClass('is-complete');
      $("#step2").addClass('is-active');
    }else if($step == '3'){
        $("#btn-2").hide();
        $("#btn-cancel-2").hide();
        $("#step2").removeClass('is-active').addClass('is-complete');
      $("#step3").addClass('is-active');
    }else if($step == '4'){
        $("#step3").removeClass('is-active').addClass('is-complete');
      $("#step4").addClass('is-active');
    }
    
  }
  
  function backto($step){
      if($step == '1'){
          $("#btn-1").show();
          $("#step1").removeClass('is-complete').addClass('is-active');
          $("#step2").removeClass('is-active');
      }else if($step == '2'){
          $("#btn-2").show();
          $("#btn-cancel-2").show();
          $("#step2").removeClass('is-complete').addClass('is-active');
          $("#step3").removeClass('is-active');
      }else if($step == '3'){
          $("#step3").removeClass('is-complete').addClass('is-active');
          $("#step4").removeClass('is-active');
      }
  }
  
lbb.showPaymentModal = function(){
  $("#payment_modal").modal('toggle');
}

lbb.closePaymentModal = function(){
  $("#payment_modal").modal('toggle');
}

lbb.removeCart = function(item_row, item_line_row){
  var r = confirm("Are you want to remove this item?");
  if (r == false) { 
    return false;
  } 
  
  
  var data = {
    'item_row' : item_row,
    'item_line_row' : item_line_row,
  }
  $.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: lbb.base_url + '/api/remove-cart',
    type: 'post',
    data: data,
    beforeSend: function() {
        $("#cart-content").html('<img src="'+lbb.reload_image+'">');
    },
    complete: function() {

    },
    error : function(){
      alert('Time Out, please try again');  
    },
    success: function(data) {
      location.reload();
    }
  });
    
  return false;
}
 

  lbb.doPaymentCheckout = function(){
    $("#payment-checkout-btn").prop('disabled',true);
    
    var data = {
      'payment_type' : $("input[name='payment-method']:checked").val(),
      'first_name' : $("#name").val(),
      'last_name' : $("#last_name").val(),
      'email' : $("#email").val(),
      'address' : $("#address").val(),
      'city' : $("#city").val(),
      'state' : $("#state").val(),
      'postal_code' : $("#postal_code").val(),
      'country' : $("#country").val(),
      'phone' : $("#phone").val(),
      'gender' : $("#gender").val(),      
    }
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: lbb.payment_api,
      type: 'post',
      data : data,
      beforeSend: function() {
//          $("#cart-content").html('<img src="'+lbb.reload_image+'">');
            
      },
      complete: function() {
        
      },
      error : function(){
        alert('Time Out, please try again');  
      },
      success: function(data) {
        $("#payment-checkout-btn").prop('disabled',false);
        var response = JSON.parse(data);
        
        if(response.error == 1){
          alert(response.message)
        }else{
          window.location = lbb.base_url+'/payment';
        }
        
      }
    });
  }
  
</script>
@stop
