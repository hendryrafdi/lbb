@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>
  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12" align='center'>
            <h2 class="blue">Review Donation & Pay</h2>     
            <div class="progress">
                <div class="one primary-color"></div><div class="two primary-color"></div><div class="three no-color"></div>
          			<div class="progress-bar" style="width: 50%;"></div>
		    </div>
          </div>
          <div class="col-lg-8">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-white">
                        <div class="panel-heading" align="left">
                          <h4 class="blue">1. Donor Information</h4>
                          <p>Please enter your contact information. All field are required.</p>
                        </div>
                        <div class="panel-body">
                          @if ($errors->any())
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                          @endif
                          @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                          @endif
                
                          <form role="form" method="POST" action="{{ route('frontend.dashboard.profile.update') }}">
                            @if(isset($data))
                              <input name="_method" type="hidden" value="PUT">
                            @endif
                            {{ csrf_field() }}
                            <div class="row" align="left">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">First Name</label>
                                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter First Name" value="{{ $data->name or old('name') }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="last_name">Last Name</label>
                                  <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="{{ $data->last_name or old('last_name') }}">
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="email">Email address</label>
                                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ $data->email or old('email') }}">
                                </div>
                                <div class="form-group">
                                  <label for="address">Address</label>
                                  <input type="text" name="address" class="form-control" id="address" placeholder="Enter address" value="{{ $data->address or old('address') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="city">City / Province</label>
                                  <input type="text" name="city" class="form-control" id="city" placeholder="Enter city/province" value="{{ $data->city or old('city') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="postal_code">Postal Code</label>
                                  <input type="text" name="postal_code" class="form-control" id="postal_code" placeholder="Enter postal code" value="{{ $data->postal_code or old('postal_code') }}">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label for="country">Country</label>
                                  <input type="text" name="country" class="form-control" id="country" placeholder="Enter country" value="{{ $data->country or old('country') }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Mobile Phone</label>
                                  <input type="number" name="phone" class="form-control" id="phone" placeholder="Enter Your Phone Number" value="{{ $data->phone or old('phone') }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="last_name">Gender</label><br>
                                    <div class="col-md-3"><input type="radio" name="gender" value="male"> Male<br></div>
                                    <div class="col-md-3"><input type="radio" name="gender" value="female"> Female<br></div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label for="password">Password Confirmation</label>
                                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                </div>
                                <!--<div class="form-group">
                                  <label for="password_confirmation">Password Confirmation</label>
                                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation">
                                </div>-->
                              </div>
                            </div>
                
                            <!--/.col (left) -->
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" class="btn btn-green btn-md btn-block" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Next</button>
                                </div>
                              </div>
                            </div>
                            <!--/.row -->
                          </form>
                          </div>
                        </div>
                  </div>
              </div>
              <hr>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-white">
                          <div class="panel-heading" align="left">
                          <h4 class="blue">2. Scholarship Payment Plan</h4>
                        </div>
                        <div class="panel-body collapse" id="collapseExample">
                          <p>Scholarship Payment Plan.</p>
                        </div>
                      </div>
                  </div>
              </div>
              <hr>
          </div>
          <div class="col-lg-4" class="blue">
              Basket
          </div>
        </div>
        
      </div>
    </header>
  </section>
  

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
