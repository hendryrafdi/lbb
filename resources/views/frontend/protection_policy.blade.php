@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--end site header-->
  <section class="white-background">
    <div align="center" class="header-link">
      <label><a href="{{route('front.tnc')}}">{{trans('lang.Term and Conditions')}}</a></label>
      <label><a href="{{route('front.policies')}}">{{trans('lang.Policies')}}</a></label> 
      <label class="active"><a href="{{route('front.protectionPolicy')}}">{{trans('lang.Student Protection Policy')}}</a></label>
    </div>
    <div class="height30"></div>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2" align='left'>
            <h2 class='blue'>{{trans('lang.Student Protection Policy')}}</h2>
            <div class="policies-caption" align='justify'> 
              <p>
              {{trans('lang.protection policy 1')}}
              </p>
              <p>
                <b>{{trans('lang.Personal Information')}}</b><br>
                {{trans('lang.protection policy 2')}}
                <br>
                
              <ol>
                <li>{{trans('lang.You must have the permission of LBB and YPPH to publicize any information and images of students on public platforms')}}</li>
                <li>{{trans('lang.You may not use any photo or information of students for commercial purposes or for your own benefit in any way form')}}</li>
                <li>{{trans('lang.We do not and will not provide the personal address of any student')}}</li>
              </ol>
    
              </p>
              <p>
                <b>{{trans('lang.Interaction with Students')}}</b><br>
                {{trans('lang.protection policy 3')}} <i>{{trans('lang.chat')}}</i> {{trans('lang.dan lainnya')}}
              </p>
              <p>
                <b>{{trans('lang.Visiting Student')}}</b><br>
                {{trans('lang.Benefactors are not allowed to visit supported students')}}
              </p>
              <p>
                <b>{{trans('lang.Giving Gifts')}}</b><br>
                {{trans('lang.You are advised to not give cash or gift to the child or their family')}}    
              </p>
            </div>
                        
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
