@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>
  <!--<header class="site-header common-static-header">-->
  <!--  <div data-stellar-background-ratio="0.25" style="background-image: url({{URL::to('/')}}/assets/images/standingline.png);" class="parallax-bg"></div>-->
  <!--  <div class="container pull-left">-->
  <!--    <p class='header-faq'>-->
  <!--      HELPFUL INFORMATION-->
  <!--    </p>-->
  <!--  </div>-->
  <!--  <div class="container pull-left">-->
  <!--    <p class='header-caption'>-->
  <!--      Some frequesntly ask question about our programs-->
  <!--    </p>-->
  <!--  </div>-->
  <!--</header>-->
  <header class="site-header common-static-header">
    @if(trans('lang.item 1 privacy 3'))
    <div data-stellar-background-ratio="0.25" style="background-image: url({{URL::to('/')}}/assets/images/location_id.jpg);background-size: auto;" class="parallax-bg"></div>
    @else
    <div data-stellar-background-ratio="0.25" style="background-image: url({{URL::to('/')}}/assets/images/location_en.jpg);background-size: auto;" class="parallax-bg"></div>
    @endif
    <div class="container pull-right">
      <div class="row">
        <div class="col-md-12">
          <p class="header-caption">
            <!--<span class="about-title">{{trans('lang.About Us')}}</span><br>-->
            <!--{{trans('lang.about caption 1')}}<br>-->
            <!--{{trans('lang.about caption 2')}}<br>-->
            <!--{{trans('lang.about caption 3')}}.-->
            <br><br>
            <br><br>
          </p>          
        </div>
      </div>
    </div>
  </header>
  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2" align='justify'>
            <h2 class='blue'>{{trans('lang.Our School Network')}}</h2>
            <!--<p class="faq-caption" align="left"> Here are some common questions related donations and students cholarship.</p>-->
            <p> 
              {{trans('lang.From the shores of Nias to the deep mountains of Papua, we serve children throughout Indonesia despite their economic background')}}. 
              <br>
            </p>
              <div class="col" align="justify">
                  <div class="row">
                    <div class="col-lg-12" >
                        <h4 class="blue">{{trans('lang.Sekolah Lentera Harapan (SLH) since 1998')}}</h4>    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        1998    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 1 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 322    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 24  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 1 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2002    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 2 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 251    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 20  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 2 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2004    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 3 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 88    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 8  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 3 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2005    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 4 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 511    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 39  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 4 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                         
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 5 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 137    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 14  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 5 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2007    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 6 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 566    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 37  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 6 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2008    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 7 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 896    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 56  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 7 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 8 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 222    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 17  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 8 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                         2010   
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 9 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 844    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 49  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 9 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 10 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 1.149    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 73  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 10 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2011    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 11 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 706    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 41  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 11 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                           
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 12 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 1.025    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 61  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 12 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 13 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 801    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 51  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 13 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2012    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 14 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 714    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 47  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 14 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2013    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 15 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 209    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 19  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 15 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 16 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 583    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 38  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 16 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 17 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 227    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 15  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 17 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2016    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 18 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 88    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 6  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 18 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 19 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 470    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 35  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 19 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 20 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 58    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 8  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 20 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 21 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 48    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 5  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 21 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                        2017    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 22 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 89    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 9  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 22 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 23 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 80    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 6  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 23 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  <div class="row">
                    <div class="col-lg-12" >
                            
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 24 Grade')}}    
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-lg-6" >
                        {{trans('lang.Number of students')}} 19    
                    </div>  
                    <div class="col-lg-6" >
                        {{trans('lang.Number of teachers')}} 5  
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-lg-12" >
                        {{trans('lang.SLH 24 Desc')}}   
                    </div>  
                  </div>
                  <div class="row">&nbsp;</div>
                  
                  {{trans('lang.Note')}}
                  <div class="row">
                      <div class="col-lg-1">
                          {{trans('lang.K')}}
                      </div>
                      <div class="col-lg-8">
                          = {{trans('lang.Kindergarten K1-K2 age 4-5')}}
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-1">
                          {{trans('lang.P')}}
                      </div>
                      <div class="col-lg-8">
                          = {{trans('lang.Primary grade 1-6 age 6-11')}}
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-1">
                          {{trans('lang.J')}}
                      </div>
                      <div class="col-lg-8">
                          = {{trans('lang.Junior High School grade 7-9 age 12-14')}}
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-1">
                          {{trans('lang.S')}}
                      </div>
                      <div class="col-lg-8">
                          = {{trans('lang.Senior High School grade 10-12 age 15-17')}}
                      </div>
                  </div>
              </div>
              
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>Contact Us</h2>  
            
            <p class="contact-caption"> 
              Can't find what you're looking for? Let us know how we can help. Email your <br>
              question to <a class="blue" href="mailto:lenterabagibangsa.org">lenterabagibangsa.org</a>
            </p>
          </div>
        </div>
      </div>
    </header>
    
  </section>
  


  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>  
@stop
