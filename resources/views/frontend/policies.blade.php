@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--end site header-->
  <section class="white-background">
    <div align="center" class="header-link">
      <label><a href="{{route('front.tnc')}}">{{trans('lang.Term and Conditions')}}</a></label>
      <label class="active"><a href="{{route('front.policies')}}">{{trans('lang.Policies')}}</a></label> 
      <label><a href="{{route('front.protectionPolicy')}}">{{trans('lang.Student Protection Policy')}}</a></label>
    </div>
    <div class="height30"></div>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2" align='left'>
            <h2 class='blue'>{{trans('lang.Privacy Policy')}}</h2>
            <div class="policies-caption" style="text-align: justify;"> 
              <p>
                <b>{{trans('lang.Lentera Bagi Bangsa')}} </b>{{trans('lang.privacy 1')}}
              </p>
              <p>
                {{trans('lang.privacy 2')}}
              </p>        
              @if(trans('lang.item 1 privacy 3'))
              <p>
                <b>{{trans('lang.collected information')}}</b><br>
                {{trans('lang.privacy 3')}}
                <ol>
                    <li>{{trans('lang.item 1 privacy 3')}}</li>
                    <li>{{trans('lang.item 2 privacy 3')}}</li>
                    <li>{{trans('lang.item 3 privacy 3')}}</li>
                    <li>{{trans('lang.item 4 privacy 3')}}</li>
                    <li>{{trans('lang.item 5 privacy 3')}}</li>
                </ol>
                {{trans('lang.privacy 4')}}
              </p>
              @endif
              <p>
                <b>{{trans('lang.Use of Information')}}</b><br>
                {{trans('lang.privacy 5')}}            
              </p>
              <p>
                <b>{{trans('lang.Payment Gateway')}}</b><br>
                {{trans('lang.privacy 6')}} <a href="http://www.doku.com/" target="_blank" style="color: blue">{{trans('lang.doku site')}}</a> {{trans('lang.continue privacy 6')}}
              </p>
            </div>
                        
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
