@extends('frontend.layouts.admin')

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-heading">
          <h4>My Students</h4>
          <p>
            The list of students that are currently receiving scholarship from you.
          </p>
        </div>
        <div class="panel-body">
          <table class="table">
            <thead>
              <tr>
                <th></th>
                <th class='blue'>NAME</th>
                <th class='blue'>SLH</th>
                <th class='blue'>START</th>
                <th class='blue'>END</th>
              </tr>
            </thead>
            @if (!empty($transaction_line))
            <tbody>
              @foreach($transaction_line as $k => $v)
              @php
                $student_data = json_decode($v->item);
                $start_date_ymd = date_create($v->created_at);
                $start_date_dmy = date_format($start_date_ymd,"d-m-Y");
                
                $end_year_dmy = date("d-m-Y", strtotime($v->created_at . " + ". $v->qty ." year"))
              @endphp
                <tr>
                  <td>
                    @if (isset($student_data->picture))
                      <img src='{{$student_data->picture}}' width='50px' style='border-radius: 50%'>
                    @endif
                  </td>
                  <td>{{ isset($student_data->name)? $student_data->name : '' }}</td>
                  <td>{{ isset($student_data->school)? $student_data->school : '' }}</td>
                  <td>{{ $start_date_dmy }}</td>
                  <td>{{ $end_year_dmy }}</td>
                <td>
                      <a href="javascript:;" class="btn btn-green btnShowDocument-{{ $student_data->id }}" onClick="showDocument({{ $student_data->id }})">Dcouments</a>
                      <a href="javascript:;" class="btn btn-green btnHideDocument-{{ $student_data->id }}" onClick="hideDocument({{ $student_data->id }})" style="display:none;">Dcouments</a>
                  </td>
                </tr>
                <tr class="blue detailHead-{{ $student_data->id }}" style="display: none;">
                    <td></td>
                    <td>Thank you letter</td>
                    <td colspan="2">First Report Card</td>
                    <td>Second Report Card</td>
                    <td></td>
                </tr>
                <tr class="blue detailContent-{{ $student_data->id }}" style="display: none;">
                    @if(isset($v->file_1))
                    <td></td>
                    <td><a href="/uploads/documents/{{ $v->file_1 }}" target="_blank">Download</a></td>
                    @else
                    <td></td>
                    <td>-</td>
                    @endif
                    @if(isset($v->file_2))
                    <td colspan="2"><a href="/uploads/documents/{{ $v->file_2 }}" target="_blank">Download</a></td>
                    @else
                    <td colspan="2">-</td>
                    @endif
                    @if(isset($v->file_3))
                    <td><a href="/uploads/documents/{{ $v->file_3 }}" target="_blank">Download</a></td>
                    <td></td>
                    @else
                    <td>-</td>
                    <td></td>
                    @endif
                </tr>
              @endforeach
            </tbody>
            @endif
          </table>
          {{ $transaction_line->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    function showDocument(id){
        $(".detailHead-"+id).show();
        $(".detailContent-"+id).show();
        $(".btnHideDocument-"+id).show();
        $(".btnShowDocument-"+id).hide();
    }
    
    function hideDocument(id){
        $(".detailHead-"+id).hide();
        $(".detailContent-"+id).hide();
        $(".btnShowDocument-"+id).show();
        $(".btnHideDocument-"+id).hide();
    }
</script>
@endsection