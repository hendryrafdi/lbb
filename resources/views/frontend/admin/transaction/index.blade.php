@extends('frontend.layouts.admin')

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-heading">
          <h4 class="blue"><b>Past Receipt</b></h4>
          <p>
            The list of your ongoing and past donations.
          </p>
        </div>
        <div class="panel-body">
            <div class="row blue" align="center">
                <div class="col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-2 col-xs-2">
                            <b>DATE</b>
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <b>CODE</b>
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <b>AMOUNT</b>
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <b>STATUS</b>
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <b>METHOD</b>
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <b>ACTION</b>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            @if (!empty($transactions))
            @foreach($transactions as $k => $v)
            @php
            $end_date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($v->transaction_date)) ."+".$v->qty."year"));
            @endphp
            <div class="row" align="center">
                <div class="col-lg-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-2 col-xs-2">
                            {{ $v->transaction_date }}
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            {{ $v->code }}
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            {{ 'Rp. ' . number_format($v->total, 0, '.', ',') }}
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            {{ $v->status == 0 ? 'Not Paid' : 'Paid' }}
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            {{ $v->instalment == 0 ? 'Full payment' : 'Recurring' }}
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <button class='btn btn-green' data-toggle="collapse" data-target="#collapseDetail-{{ $v->id }}" aria-expanded="false" aria-controls="collapseDetail-{{ $v->id }}"> <b>Details</b></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row collapse" id="collapseDetail-{{ $v->id }}" style="background:#f4fafa; padding: 5px" align="center">
                <div class="card card-body">
                    <div class="row">
                        <div class="col-lg-2 col-lg-offset-1 col-xs-2"><b class="blue">Frequency</b></div>
                        <div class="col-lg-2 col-xs-2"><b class="blue">Start</b></div>
                        <div class="col-lg-2 col-xs-2"><b class="blue">End</b></div>
                        <div class="col-lg-2 col-xs-2"><b class="blue">Deduct Every</b></div>
                        <div class="col-lg-2 col-xs-2"><b class="blue">ID</b></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-lg-offset-1 col-xs-2">{{ $v->instalment == 0 ? 'Full payment' : 'Monthly' }}</div>
                        <div class="col-lg-2 col-xs-2">{{ $v->transaction_date }}</div>
                            <div class="col-lg-2 col-xs-2">{{ $end_date }}</div>
                        <div class="col-lg-2 col-xs-2">{{ $v->instalment == 0 ? 'Full payment' : 'First monday of every month'}}</div>
                        <div class="col-lg-2 col-xs-2"></div>
                    </div>
                </div>
            </div>
            <hr>
            @endforeach
            @endif
          <!--<table class="table">-->
          <!--  <thead>-->
          <!--    <tr>-->
          <!--      <th>DATE</th>-->
          <!--      <th>Code</th>-->
          <!--      <th>AMOUNT</th>-->
          <!--      <th>STATUS</th>-->
          <!--      <th>METHOD</th>-->
          <!--      <th>ACTION</th>-->
          <!--    </tr>-->
          <!--  </thead>-->
          <!--  @if (!empty($transactions))-->
          <!--  <tbody>-->
          <!--    @foreach($transactions as $k => $v)-->
          <!--      <tr>-->
          <!--        <td>{{ $v->transaction_date }}</td>-->
          <!--        <td>{{ $v->code }}</td>-->
          <!--        <td>{{ 'Rp. ' . number_format($v->total, 0, '.', ',') }}</td>-->
          <!--        <td>{{ $v->status == 0 ? 'Not Paid' : 'Paid' }}</td>-->
          <!--        <td>{{ $v->instalment == 0 ? 'full payment' : 'Recurring' }}</td>-->
          <!--        <td>-->
          <!--            <button class='btn btn-green' data-toggle="collapse" data-target="#collapseDetail-{{ $v->id }}" aria-expanded="false" aria-controls="collapseDetail-{{ $v->id }}"> <b>Details</b></button>-->
          <!--        </td>-->
                  <!--<td><button class='btn btn-green' id='donation-btn_{{ $v->id }}' onClick='showDetailReceipt({{ $v->id }})'> <b>Details</b></button></td>-->
          <!--      </tr>-->
          <!--      <tr id="collapseDetail-{{ $v->id }}" class="collapse bg-primary">-->
          <!--          <td>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">Frequency</div>-->
          <!--              </div>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">isinya</div>-->
          <!--              </div>-->
          <!--          </td>-->
          <!--          <td>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">Start</div>-->
          <!--              </div>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">{{ $v->transaction_date }}</div>-->
          <!--              </div>-->
          <!--          </td>-->
          <!--          <td>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">End</div>-->
          <!--              </div>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">isinya</div>-->
          <!--              </div>-->
          <!--          </td>-->
          <!--          <td>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">Frequency</div>-->
          <!--              </div>-->
          <!--              <div class="row">-->
          <!--                  <div class="col">isinya</div>-->
          <!--              </div>-->
          <!--          </td>-->
          <!--          <td><button class='btn btn-green' id='donation-btn_{{ $v->id }}' onClick='hideDetailReceipt({{ $v->id }})'> Hide</button></td>-->
          <!--      </tr>-->
          <!--    @endforeach-->
          <!--  </tbody>-->
          <!--  @endif-->
          <!--</table>-->
          {{ $transactions->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    function showDetailReceipt(id){
        $("#donation-btn_"+id).hide();
        $("#detail_receipt_"+id).show();
        console.log(id);
    }
    
    function hideDetailReceipt(id){
        $("#donation-btn_"+id).show();
        $("#detail_receipt_"+id).hide();
        console.log(id);
    }
</script>
@endsection