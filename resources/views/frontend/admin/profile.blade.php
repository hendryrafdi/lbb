@extends('frontend.layouts.admin')

@section('content')
<div id="main-wrapper">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-white">
        <div class="panel-heading">
          <h4>Edit Profile</h4>
          <p>
            Change your personal information
          </p>
        </div>
        <div class="panel-body">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif

          <form role="form" method="POST" action="{{ route('frontend.dashboard.profile.update') }}">
            @if(isset($data))
              <input name="_method" type="hidden" value="PUT">
            @endif
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">First Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter First Name" value="{{ isset($data) ? $data->name : old('name') }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="last_name">Last Name</label>
                  <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last Name" value="{{ isset($data) ? $data->last_name : old('last_name') }}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="email">Email address</label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ isset($data) ? $data->email : old('email') }}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" name="address" class="form-control" id="address" placeholder="Enter address" value="{{ isset($data) ? $data->address : old('address') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="city">City </label>
                  <input type="text" name="city" class="form-control" id="city" placeholder="Enter city" value="{{ isset($data) ? $data->city : old('city') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="province">Province / State</label>
                  <input type="text" name="state" class="form-control" id="state" placeholder="Enter province / state" value="{{ isset($data) ? $data->state : old('state') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="country">Country</label>
                  <input type="text" name="country" class="form-control" id="country" placeholder="Enter country" value="{{ isset($data) ? $data->country : old('country') }}">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="postal_code">Postal Code</label>
                  <input type="text" name="postal_code" class="form-control" id="postal_code" placeholder="Enter postal code" value="{{ isset($data) ? $data->postal_code : old('postal_code') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="name">Mobile Phone</label>
                  <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter Your Phone Number" value="{{ isset($data) ? $data->phone : old('phone') }}">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="password_confirmation">Password Confirmation</label>
                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation">
                </div>
              </div>
            </div>

            <!--/.col (left) -->
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!--/.row -->
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection