<footer class="footer-preset-03">
  <div class="container">
    <div class="footer-preset-wrapper">
      <div class="row">
        <div class="col-md-2 col-xs-6">
          <img src="{{URL::to('/')}}/assets/images/LogoLBBcopy2.png" class="img-responsive">
        </div>
        <div class="col-md-2 col-xs-6">
          <label class="blue bold">{{ trans('lang.An Initiative by') }}</label>
          <img src="{{URL::to('/')}}/assets/images/Layer3.png" class="img-responsive">
        </div>
        <div class='col-md-8 col-xs-12'>
          <div class='col-md-12 col-xs'>
          <!-- Begin Mailchimp Signup Form -->
          <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
          <style type="text/css">
            #mc_embed_signup{background:#f4fafa; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
            .subcriber-text{color: #3f7da1;}
            #mc_embed_signup input.email{border-top-left-radius: 15px;border-bottom-left-radius: 15px;}
            @media (max-width: 768px) {
              #mc_embed_signup input.email,#mc_embed_signup .clear {display: inline; width:70%;}
              #mc_embed_signup .button {margin:0; width:30%;}
            }
            /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
               We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
          </style>
          <div id="mc_embed_signup">
          <form action="https://lenterabagibangsa.us17.list-manage.com/subscribe/post?u=f636db9b7347786e15791151b&amp;id=21da4bd5bc" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div id="mc_embed_signup_scroll" align='left'>
              <label for="mce-EMAIL" class='subcriber-text'>{{ trans('lang.Stay up to date') }}</label>
              <label style='font-weight: unset;font-size: 11px;'>{{ trans('lang.Join our email newsletter for updates around SLH school') }}</label>
              <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="{{ trans('lang.email address') }}" required>
              <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;" aria-hidden="true">
                <input type="text" name="b_f636db9b7347786e15791151b_21da4bd5bc" tabindex="-1" value="">
              </div>
              <div class="clear">
                <input type="submit" value="{{ trans('lang.Signup2') }}" name="subscribe" id="mc-embedded-subscribe" class="button" style='background-color: #ffda96;border-radius:15px;margin-left:-20px;color:#000;font-weight:bold'></div>
              </div>
          </form>
          </div>

          <!--End mc_embed_signup-->
          </div>
        </div>  
      </div>
      <div class="row top30">
        <div class="col-md-3 col-xs-6">
          <label class="blue bold">{{trans('lang.Contact Us')}}</label>
          <ul>
            <li><span class="fa fa-envelope"></span> {{isset($config['email'])?$config['email']:''}}</li>
            <li><span class="fa fa-phone"></span> {{isset($config['phone'])?$config['phone']:''}}</li>
            <li>
                <span class="fa fa-map-marker"></span> 
                @if(session('language') == 'en')
                {{isset($config['building'])?$config['building']:''}} 
                @elseif(session('language') == 'id')
                {{isset($config['building_id'])?$config['building_id']:''}} 
                @endif
            </li>
            <li>{{isset($config['address'])?$config['address']:''}} </li>
            <li>{{isset($config['city'])?$config['city']:''}} </li>
          </ul>
        </div>
        <div class="col-md-2 col-xs-6">
          <label class="blue bold">{{trans('lang.About')}}</label>
          <ul>
            <li><a href="#">{{trans('lang.Why Education')}}</li>
            <li style="line-height:20px;"><a href="{{route('front.faq')}}">{{trans('lang.FAQs')}} </li>
          </ul>
        </div>
        <div class="col-md-2 col-xs-6">
          <label class="blue bold">{{trans('lang.How you can help')}}</label>
          <ul>
            <li><a href="{{route('front.donation')}}">{{trans('lang.Support a Student')}}</a></li>
            <li><a href="{{route('front.gift')}}">{{trans('lang.Give a gift')}}</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-xs-6">
          <label class="blue bold">{{ trans('lang.Social') }}</label>
          <ul>
            <li><span class="fa fa-facebook"></span> <a href="//facebook.com/{{isset($config['facebook'])?$config['facebook']:''}}" target="_blank">facebook</a></li>
            <li><span class="fa fa-instagram"></span> <a href="//instagram.com/{{isset($config['instagram'])?$config['instagram']:''}}" target="_blank">Instagram</a></li>
          </ul>  
        </div>
        <div class="col-md-2 col-xs-6">
          <label class="blue bold">{{trans('lang.Language')}}</label>
          <ul>
            <li><a href="{{route('front.setLanguage',['lang' =>'EN'])}}">English</a></li>
            <li><a href="{{route('front.setLanguage',['lang' =>'ID'])}}">{{trans('lang.Indonesia')}}</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-foot">
      <div class="row">
        <div class="col-md-3 hidden-xs">
          &copy;{{date('Y')}} Lentera Bagi Bangsa
        </div>
        
        <div class="col-md-9 hidden-xs" align="right">
          <ul>
            <li><a href="{{route('front.tnc')}}">{{trans('lang.Term and Conditions')}}</a></li>
            <li><a href="{{route('front.policies')}}">{{trans('lang.Policies')}}</a></li>
            <li><a href="{{route('front.protectionPolicy')}}">{{trans('lang.Student Protection Policy')}}</a></li>
          </ul>
        </div>
        
        <div class="col-xs-12 visible-xs" align="center">
          &copy;{{date('Y')}} Lentera Bagi Bangsa
      </div>      
        
        <div class="col-xs-12 visible-xs" align="center">
          <ul>
            <li><a href="{{route('front.tnc')}}">{{trans('lang.Term and Conditions')}}</a></li>
            <li><a href="{{route('front.policies')}}">{{trans('lang.Policies')}}</a></li>
            <li><a href="{{route('front.protectionPolicy')}}">{{trans('lang.Student Protection Policy')}}</a></li>
          </ul>
    </div>
  </div>
    </div>
  </div>
</footer>

<!-- Modal -->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="container-fluid">
          <div class="page-login-wrapper">
            <div class='page-login'>
              <div align="center">
                <img src='{{URL::to('/')}}/assets/images/logo_black.png'>
              </div>
              <div class='row'>
                <div class='col-md-12 col-xs-12' align='center'>
                  <input class="form-control email-login login-input" type='text' value='' id='email' placeholder="email" style="font-size: 13px;">
                </div>
              </div>
              <div class='row'>
                <div class='col-md-12 col-xs-12' align='center'>
                  <input class="form-control password-login login-input" type='password' value='' id='password' placeholder='password' style="font-size: 13px;">
                </div>
              </div>
              <div class='row'>
                <div class='col-md-12 col-xs-12' align='center'>

                </div>
              </div>

              <div align="center">
              <div class="button login-btn" id="login-submit-btn" width="50%"><a href="#" class="normal-btn normal-btn-main btn-size-5 btn-link" style="font-size: 13px;">{{trans('lang.login')}}</a></div>
              <div class="button signup-btn" id="signup-btn"><a href="#" class="border-btn border-btn-lightgray btn-size-5 btn-link" style="font-size: 13px;">{{trans('lang.signup')}}</a></div>
              
              <!-- <button id="gl_fb_button" type="button" class="button sign-btn main-button f-btn"><i class="fa fa-facebook"></i> &nbsp;Continue With Facebook</button> -->
              <div class="button login-btn" id="login-submit-btn" width="50%">
                <a href="/login/facebook" class="button facebook-button">
                  <span class="facebook-button__icon fa fa-facebook">
                  </span>
                  <span class="facebook-button__text">Sign in with Facebook</span>
                </a>
              </div>
              <div class="button login-btn" id="login-submit-btn" width="50%">
                <a href="/login/google" class="button google-button">
                  <span class="google-button__icon">
                    <svg viewBox="0 0 366 372" xmlns="http://www.w3.org/2000/svg"><path d="M125.9 10.2c40.2-13.9 85.3-13.6 125.3 1.1 22.2 8.2 42.5 21 59.9 37.1-5.8 6.3-12.1 12.2-18.1 18.3l-34.2 34.2c-11.3-10.8-25.1-19-40.1-23.6-17.6-5.3-36.6-6.1-54.6-2.2-21 4.5-40.5 15.5-55.6 30.9-12.2 12.3-21.4 27.5-27 43.9-20.3-15.8-40.6-31.5-61-47.3 21.5-43 60.1-76.9 105.4-92.4z" id="Shape" fill="#EA4335"/><path d="M20.6 102.4c20.3 15.8 40.6 31.5 61 47.3-8 23.3-8 49.2 0 72.4-20.3 15.8-40.6 31.6-60.9 47.3C1.9 232.7-3.8 189.6 4.4 149.2c3.3-16.2 8.7-32 16.2-46.8z" id="Shape" fill="#FBBC05"/><path d="M361.7 151.1c5.8 32.7 4.5 66.8-4.7 98.8-8.5 29.3-24.6 56.5-47.1 77.2l-59.1-45.9c19.5-13.1 33.3-34.3 37.2-57.5H186.6c.1-24.2.1-48.4.1-72.6h175z" id="Shape" fill="#4285F4"/><path d="M81.4 222.2c7.8 22.9 22.8 43.2 42.6 57.1 12.4 8.7 26.6 14.9 41.4 17.9 14.6 3 29.7 2.6 44.4.1 14.6-2.6 28.7-7.9 41-16.2l59.1 45.9c-21.3 19.7-48 33.1-76.2 39.6-31.2 7.1-64.2 7.3-95.2-1-24.6-6.5-47.7-18.2-67.6-34.1-20.9-16.6-38.3-38-50.4-62 20.3-15.7 40.6-31.5 60.9-47.3z" fill="#34A853"/></svg>
                  </span>
                  <span class="google-button__text">Sign in with Google</span>
                </a>
              </div>
              <a href="#" class="forgotpass">{{trans('lang.forget password')}}?</a>    
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="signup_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="container-fluid">
          <div align="center">
                <img src='{{URL::to('/')}}/assets/images/logo_black.png'>
          </div>
          <div class='row signup-form'>
            <div class="row">
              <div class='col-md-12 col-xs-12' align='center'>
                <input class="form-control email-signup login-input" type='text' value='' id='email' placeholder="{{trans('lang.email')}}" style="font-size: 13px;" required>
              </div>
            </div>
            <div class="row">
              <div class='col-md-12 col-xs-12' align='center'>
                <input class="form-control name-signup login-input" type='text' value='' id='name' placeholder="{{trans('lang.name')}}" style="font-size: 13px;" required>
              </div>
            </div>
            <div class="row">
              <div class='col-md-12 col-xs-12' align='center'>
                <input class="form-control password-signup login-input" type='password' value='' id='password' placeholder='{{trans('lang.password')}}' style="font-size: 13px;" required>
              </div>
            </div>
            <div class="row">
              <div class='col-md-12 col-xs-12' align='center'>
                <input class="form-control password-signup-2 login-input" type='password' value='' id='password-confirmation' placeholder='{{trans('lang.password confirmation')}}' style="font-size: 13px;" required>
              </div>    
            </div>
            <div class="row">
              <div class='col-md-12 col-xs-12' align='center'>
                <button class='normal-btn normal-btn-main section-block btn-size-1' id="signup-submit-btn" style="font-size: 13px;">{{trans('lang.signup')}}</button>
              </div>
            </div>
            <div class="row">
              <div class='col-md-12 col-xs-12' align='center'>
                {{trans('lang.have an account')}}? <a href = "#" class="login-btn">{{trans('lang.click here')}}</a>
              </div>
            </div>
          </div>

          <div class="row response-signup">
            <div class='col-md-2 col-xs-2' id="signup_response_symbol" align="right"></div>
            <div class='col-md-10 col-xs-10' align='left' id="signup_response"></div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="forget_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content forget-body">
      <div class="modal-body forget-body">
      <div class="container-fluid">
        <div align="center">
            <img src='{{URL::to('/')}}/assets/images/logo_black.png'>
          </div>
        <div class='row'>
          <div class='col-md-12 col-xs-12' align='center'>
            <input class="form-control email-forget login-input" type='text' value='' id='email-forget' placeholder="{{trans('lang.email')}}">
          </div>
        </div> 
        <div class='row'>
          <div class='col-md-12 col-xs-12' align='center'>
            <button class='normal-btn normal-btn-main section-block btn-size-1' id="forget-submit-btn">{{trans('lang.submit')}}</button>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="modal right fade" id="cart_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="container-fluid">
          <div class="page-login-wrapper">
            <div class="row">
              <div class="col-md-6 col-xs-6 pull-left">
                <button class="btn-green" onclick="lbb.hideCartContent()">Keep Browsing</button>
              </div>
              <div class="col-md-6 col-xs-6 pull-right" align="right">
                <a href="#" class="btn btn-green" id="checkout-btn" style="padding: 10px;" onclick="lbb.checkout()">Begin Checkout</a>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12 cart-content" id="cart-content" align="center">
                  
                </div>
                <div class="col-md-12 col-xs-12 payment-content" id="payment-content" align="center">
                  <h2>Payment Option</h2>
                  <div class="container white-background">
                    <div class="row">
                      <div class="col-md-12 col-xs-12" align="left">
                        <img src="{{ asset('assets/images/visaMastercard.png')}}">                        
                      </div>
                    </div>
                    <div class="height30"></div>
                    <div class="row">
                      <div class="col-md-12 col-xs-12" align="left">
                        <input name="payment-method" type="radio" value="fp"> Full Payment
                      </div>
                    </div>
                    <div class="row" id='recuring_div'>
                      <div class="col-md-12 col-xs-12" align="left">
                        <input name="payment-method" type="radio" value="rp"> Reccuring Payment
                      </div>
                    </div>
                    <div class="height30"></div>
                    <div class="row">
                      <div class="col-md-12 col-xs-12" align="left">
                        <button class="btn-green" id="payment-btn" onclick="lbb.doPayment()">Pay</button>                        
                      </div>
                    </div>
                    <div class="height30"></div>
                    
                  </div>
                  
                  
                  
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-xs-12" id="cart-empty" align="center">
                  <b>{{trans('lang.Your cart is empty')}}</b><br>
                  {{trans('lang.Your cart is currently empty. You can continue browsing and start to filling it')}}.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>