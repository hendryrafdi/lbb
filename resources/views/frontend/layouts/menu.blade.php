<nav>
  <div class="main-nav main-nav-1 landing-page">
    <div class="container">
<!--      <div class="nav-logo"><a href="index.html"><img src="assets/images/logo-10-white.svg" alt="Wolverine"></a></div>
      <div class="nav-logo logo-2"><a href="index.html"><img src="assets/images/logo-9.svg" alt="Wolverine" class="logo-2"></a></div>-->
      <ul class="nav-main-menu">
        <li><a href="#">{{$session['language']}}</a>
          <ul>
            <li><a href="{{route('front.setLanguage',['lang' =>'EN'])}}">EN</a></li>
            <li><a href="{{route('front.setLanguage',['lang' =>'ID'])}}">ID</a></li>            
          </ul>
        </li>
        <li><a href="{{route('front.about')}}">{{trans('lang.About')}}</a></li>
        <li><a href="{{route('front.gift')}}">{{trans('lang.Give a gift')}}</a></li>
        <li><a href="{{route('front.donation')}}">{{trans('lang.Support a Student')}}</a></li>
        <li>
        @if(isset($page_type) && $page_type == 'home')
          <a href="{{route('front.home')}}"><img src='{{URL::to('/')}}/assets/images/logo_white.png'></a>
        @else 
          <a href="{{route('front.home')}}">
            <img src='{{URL::to('/')}}/assets/images/logo_black.png'>
        @endif
        </li>
      </ul>
      <span class='logo-white-main mobile-logo'>
        <img src='{{URL::to('/')}}/assets/images/logo_white.png'></a>
      </span>

      
      
      <ul class="nav-main-menu small-screen">
        <li><a href="#">EN</a>
          <ul>
            <li><a href="{{route('front.home',['lang' =>'EN'])}}">EN</a></li>
            <li><a href="{{route('front.home',['lang' =>'ID'])}}">ID</a></li>            
          </ul>
        </li>
        <li><a href="{{route('front.about')}}">{{trans('lang.About')}}</a></li>
        <li><a href="{{route('front.gift')}}">{{trans('lang.Give a gift')}}</a></li>
        <li><a href="{{route('front.donation')}}">{{trans('lang.Support a Student')}}</a></li>
      </ul>
      <div class="nav-hamburger-wrapper pull-right">
        <div class="cell-vertical-wrapper">
          <div class="cell-middle">
            <div class="nav-hamburger"><span></span></div>
          </div>
        </div>
      </div>
      
      <span class='right-main-nav pull-right hidden-xs'>
        @if(isset($page_type) && $page_type == 'home')
          <a href='#' onclick="lbb.showCartContent()"><img src='{{URL::to('/')}}/assets/images/cart_white.png'>
          <span id="badge-cart"></span>
          </a>
          <!--<a href='{{route('front.checkout')}}'><img src='{{URL::to('/')}}/assets/images/cart_white.png'><span class="badge badge-primary"></span></a>-->
          <span class='login-icon'>
          @if(isset($session['user'])&& $session['user'] !== '')
            <a href='{{route('frontend.dashboard.student')}}' id="account-btn"><img src='{{URL::to('/')}}/assets/images/account_white.png'></a>
          @else
            <a href='#' id="login-btn" onclick="lbb.showLoginModal()"><img src='{{URL::to('/')}}/assets/images/account_white.png'></a>
          @endif
          </span>
        @else
          <a href='#' onclick="lbb.showCartContent()"><img src='{{URL::to('/')}}/assets/images/cart_blue.png'>
          <span id="badge-cart"></span>
          </a>
          <!--<a href='{{route('front.checkout')}}'><img src='{{URL::to('/')}}/assets/images/cart_blue.png'><span class="badge badge-primary"></span></a>-->
          <span class='login-icon'>
          @if(isset($session['user'])&& $session['user'] !== '')
            <a href='{{route('frontend.dashboard.student')}}' id="account-btn"><img src='{{URL::to('/')}}/assets/images/account_blue.png'></a>
          @else
            <a href='#' id="login-btn" onclick="lbb.showLoginModal()"><img src='{{URL::to('/')}}/assets/images/account_blue.png'></a>
          @endif
          </span>
        @endif        
      </span>
      
      <span class='right-main-nav pull-right visible-xs'>
        <a href='#' onclick="lbb.showCartContent()"><img src='{{URL::to('/')}}/assets/images/cart_white.png'>
        <span id="badge-cart"></span>
        </a>
        <span class='login-icon'>
        @if(isset($session['user'])&& $session['user'] !== '')
          <a href='{{route('frontend.dashboard.student')}}' id="account-btn"><img src='{{URL::to('/')}}/assets/images/account_white.png'></a>
        @else
          <a href='#' id="login-btn" onclick="lbb.showLoginModal()"><img src='{{URL::to('/')}}/assets/images/account_white.png'></a>
        @endif
        </span>                
      </span>
      
      <div class="nav-top-cart"></div>
    </div>
    
  </div>
</nav>