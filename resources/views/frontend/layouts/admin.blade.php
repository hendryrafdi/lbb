<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Lentera Bagi Bangsa</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="{{ asset('assets/css/login.css')}}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<body>
    <div class="admin-container container-fluid">
      <div class="nav-sidebar">
        <a href={{route('front.home')}}><img width="70" alt="Logo Image" src="{{ asset('assets/images/logo_white.png') }}"></a>
        <ul class="sidebar-menu">
            <li>
              <a href="">
                <i class="fa fa-3x fa-question-circle"></i><br>
                Help
              </a>
            </li >
            <li>
              <form method="POST" action="{{ route('frontend.logout') }}">
                  {{ csrf_field() }}
                  <button class="btn-logout" id="logout"><i class="fa fa-3x fa-sign-out"></i><br><i class="icon-key m-r-xs"></i>Sign Out</button>
              </form>
            </li>
        </ul>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-11">
        <div class="row">
          <div class="col-md-3">
            <h4 class="blue">{{ Auth::guard('customers')->user()->name }}</h4>
            <h4>{{ Auth::guard('customers')->user()->email }}</h4>
            <div class="nav-menu">
              <ul>
                <li><a href="{{ route('frontend.dashboard.student') }}" 
                  <?php if(Request::route()->getName() == 'frontend.dashboard.student'){echo 'class="active"';}?> ><i class="fa fa-users"></i> My Students</a></li>
                <li><a href="{{ route('frontend.dashboard.transaction') }}" 
                  <?php if(Request::route()->getName() == 'frontend.dashboard.transaction'){echo 'class="active"';}?> ><i class="fa fa-clock-o"></i> Past Receipts</a></li>
                <li><a href="{{ route('frontend.dashboard.profile') }}" 
                  <?php if(Request::route()->getName() == 'frontend.dashboard.profile'){echo 'class="active"';}?> ><i class="fa fa-edit"></i> Edit Profile</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-9 admin-content">
            @yield('content')
          </div>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
