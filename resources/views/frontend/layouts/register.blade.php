<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lentera Bagi Bangsa</title>
    <meta name="description" content="Lentera Bagi Bangsa">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--<link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.css')}}">-->
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendors/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/wolverine/styles.css')}}">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/full-page/jquery.fullPage.css')}}">
    <!--Main stylesheet-->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css')}}">
    <!--Modernizr js-->
    <script src="vendors/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script><style> .nav-main-menu > li > ul, .nav-shop-cart{display:none;}</style>
    <style type="text/css">
      body, html{
        height: 100%;
        margin: 0;
        color: #777;
        background-image: url({{URL::to('/')}}/assets/images/home.png);
      }
    </style>
  </head>
  <body class="corporate-page-2" onload="lbb.cartContent()" style="background: rgba(0,0,0, 0.5);" >
  
<script>
  var webURL = "<?php echo route('front.home');?>";
  
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2380473638639215',
      cookie     : true,
      xfbml      : true,
      version    : '{api-version}'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
  function fb_checkLoginState() {
      FB.getLoginStatus(function (response) {
        console.log(response);
      }, true);
  }

</script>

  @yield('content')

    <script src="{{ asset('vendors/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{ asset('vendors/stellar/jquery.stellar.min.js')}}"></script>
    <script src="{{ asset('vendors/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('vendors/smooth-scroll/SmoothScroll.js')}}"></script>
    <script src="{{ asset('vendors/bootstrap/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendors/slick/slick.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendors/magnific-popup/jquery.magnific-popup.js')}}"></script>
    <script src="{{ asset('vendors/isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('vendors/isotope/packery-mode.pkgd.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendors/img-loaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('vendors/countto/jquery.countTo.js')}}"></script>
    <script src="{{ asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{ asset('vendors/progressbar/dist/progressbar.min.js')}}"></script>
    <script src="{{ asset('assets/js/md5.js')}}"></script>
    <script src="{{ asset('assets/js/jquery-ui.js')}}"></script>
    <script src="{{ asset('assets/js/main.js')}}"></script>
    
    <script>
      var email_login = "{{isset($session['email'])?$session['email']:''}}";
      var reset_password_url = "{{route('front.resetPassword')}}";
      lbb.login_api = "{{route('front.loginSubmit')}}";      
      lbb.account_api = "{{route('frontend.dashboard.student')}}";      
      lbb.signup_api = "{{route('front.signup')}}";
      lbb.signupgoogle_api = "{{route('front.signupgoogle')}}";
      lbb.cart_api = "{{route('front.cartContent')}}";
      lbb.base_url = "{{route('front.home')}}";
      lbb.reload_image = "{{ asset('assets/images/reload.gif')}}";
      lbb.payment_api = "{{route('front.paymentSubmit')}}";
      var check_session = "{{route('front.checkSession')}}";      
      var account_url = "{{route('front.account')}}";
    </script>
  @yield('script')

  </body>
</html>