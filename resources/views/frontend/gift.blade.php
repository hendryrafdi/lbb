@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>

  <!--end site header-->
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class='row'>
            <?php
            $i = 0;

            foreach($items as $item){
             if($i==3){
                $i=0;
                ?>
                </div><div class='row'>
              <?php } ?>
                  <div class='col-md-6 col-xs-12'>
                    <a href='{{route('front.detail',['type'=>'gift','id'=>$item->id])}}'>
                    <table width='100%' <?php if($i == '1'){?> 
                           class='middle-table' 
                    <?php } ?>  >
                      <tr>
                        <td>
                            <img src='{{URL::to('/')}}/uploads/items/{{isset($config['image_size']) ? $config['image_size'] : ''}}{{$item->picture}}' style='border-radius: 50%;border:solid 1px #028ccc;'>
                        </td>                                                
                      </tr>
                      <tr>
                        <td>
                          <label class='blue bold'>
                            @if($session['language'] == 'en')
                                {{$item->name}}
                            @else
                                {{$item->name_id}}
                            @endif
                          </label>
                        </td> 
                      </tr>
                      <tr>
                        <td>
                          @if($session['language'] == 'en')
                            <!--<label class='item-description'>{!! substr($item->description_en,0,100) !!}</label>-->
                            <label class='item-description'>{!! $item->description_en !!}</label>
                          @else
                            <!--<label class='item-description'>{!! substr($item->description_id,0,100) !!}</label>-->
                            <label class='item-description'>{!! $item->description_id !!}</label>
                          @endif
                        </td>
                      </tr>
                    </table>
                    </a>  
                  </div>                  
            <?php 
            $i++;
                    }?>
                </div>
                <div class='row'>
                  <div class='col-md-12 paging' align='center'>{{$items->links()}}</div>
                </div>
                    
          </div>
        </div>
        
      </div>
    </header>
  </section>
  <section class='white-background'>
    <div class='row'>
      <div class='col-md-12 col-xs-12 italic' align='center'>
        {{trans('lang.Your contribution wil be allocated for student textbook or SLH libraries')}}, <br>
        {{trans('lang.Any amount given is a stepping stone for their future')}}.<br>
        <img src='{{URL::to('/')}}/assets/images/line.png'>
      </div>
    </div>    
  </section>  
  @include('frontend.gift_section')  

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
