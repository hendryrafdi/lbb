@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>
  <header class="site-header common-static-header">
    <div data-stellar-background-ratio="0.25" style="background-image: url({{URL::to('/')}}/assets/images/aboutus-header.png);background-size: auto;" class="parallax-bg"></div>
    <div class="container pull-right">
      <div class="row">
        <div class="col-md-12">
          <p class="header-caption">
            <span class="about-title">{{trans('lang.About Us')}}</span><br>
            {{trans('lang.about caption 1')}}<br>
            {{trans('lang.about caption 2')}}<br>
            {{trans('lang.about caption 3')}}.
          </p>          
        </div>
      </div>
    </div>
  </header>
  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>{{trans('lang.Our Mission')}}</h2>
            <p class="about-caption"> {{trans('lang.We are helping one child at a time to quality education at little or no cost, to the student who need it most')}}.</p>
            <p class="about-caption"> {{trans('lang.Lentera Bagi Bangsa is an initiative by Yayasan Pendidikan Pelita Harapan (Pelita Harapan Education Foundation) to support and encourage under previleged students in the Sekolah Lentera Harapan (SLH) Network')}}.</p>
            <p class="about-caption"> {{trans('lang.Sekolah Lentera Harapan (SLH) is a chain of 111 school units across 14 provinces in Indonesia. SLH\'s mission is to serve students from economically underprevileges families by transforming lives throughthe principles of faith in Christ, true knowlege and Godly character. Currently, SLH serve over 10.500 students')}}.</p>
            <p class="about-caption"> 
              <b>{{trans('lang.Quality Education')}}</b>
              <br>
              {{trans('lang.Sekolah Lentera Harapan (SLH) uses proven educational approaches to achieve the best educational outcomes. We provide each school with qualified teacher who can inspire and be role models for the students')}}
              <br>
            </p>
            <ul class='about-item'>
              <li>{{trans('lang.100% Teacher with S1 qualifications or above')}}.</li>
              <li>{{trans('lang.Above 90% Teachers hold two bachelor\'s degrees, from Universitas Pelita Harapan Teachers College(Indonesia) and Corban University (USA)')}}. </li>
              <li>{{trans('lang.Goverment approved Quality Curriculum that is professionally reviewed and renewed each year')}}.</li>
            </ul>

          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12" style='background-image: url({{URL::to('/')}}/assets/images/MG5276.png);background-size: cover;height:500px;margin-top:70px;'>
            
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 col-xs-8 why-div">
            <h2 class='blue why-title'>{{trans('lang.Why Education')}}?</h2>
            <p class='why-caption'>
              {{trans('lang.We believe education is a fundamental right for all children')}}.
            </p>
            <p class='why-caption'>
              {{trans('lang.According to Badan Kepedulian dan Keluarga Berencana Nasional 2009 National Board for population and Fsmily Planning, more tahn 13 Million children in Indonesia do not have an opportunity to obtain adequate literacy')}} 
              <i>{{trans('lang.Badan Kependudukan dan Keluarga Berencana Nasional 2009')}}</i>
              {{trans('lang.(National Board for Population and Family Planning) indicates that about 13 million children in Indonesia do not have the opportunity to obtain adequate literacy')}}
            </p>
            <p class='why-caption'>
              {{trans('lang.With proper education, student will be more loikely to break the cycle of coverty, live heather lives, mature socially and psycollogically, contribute to the Indonesiaeconomy and be future leaders in their society')}}.
            </p>
          </div>
        </div>
        
        <div class='row'>
          <div class='col-md-2 col-xs-2'></div>
          <div class='col-md-8 col-xs-8 point-lbb'>
            <div class='row'>
            <div class='col-md-5 col-xs-12 point-lbb-box pull-left'>
              <div class='row'>
                <div class='title'><h2 class='blue'>44,000</h2></div>
              </div>
              <div class='row'>  
                <div class='text'>{{trans('lang.Number of students supported between 2002-2016')}}<br>&nbsp;</div>   
              </div>
            </div>
            <div class='col-md-5 col-xs-12 point-lbb-box pull-right'>
              <div class='row'>
                <div class='title'><h2 class='blue'>Rp 44 {{trans('lang.Billion')}}</h2></div>
              </div>
              <div class='row'>  
                <div class='text'>{{trans('lang.Fourth amount scholarships by YPPH in 2015')}}</div>
              </div>              
            </div>
            </div>
          </div>          
          <div class='col-md-2 col-xs-2'></div>
        </div>
        
      </div>
    </header>
  </section>
  
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1" >
            <h2 class='blue'>{{trans('lang.Our School Network')}}</h2>  
            
            <p class="about-caption"> 
              {{trans('lang.From the shores of Nias to the deep mountains of Papua, we serve children throughout Indonesia despite their economic background')}}. 
              <br>
            </p>
          </div>
          <div class="col-lg-6 col-lg-offset-3" align="justify">
              <div class="row">
                <div class="col-lg-12" align="center">
                    <h4 class="blue">{{trans('lang.Sekolah Lentera Harapan (SLH) since 1998')}}</h4>    
                </div>  
              </div>
              <div class="row">
                  <div class="col-lg-6">
                      <div class="row">
                        <div class="col-lg-12" >
                            <b>1998</b>
                        </div>  
                      </div>
                      <div class="row">
                        <div class="col-lg-12" >
                            {{trans('lang.SLH 1 Grade')}} 
                        </div>  
                      </div>
                      <div class="row" style="line-height:20px;">
                        <div class="col-lg-6" >
                            <p>{{trans('lang.Number of students')}} 322</p>    
                        </div>  
                        <div class="col-lg-6" >
                            <p>{{trans('lang.Number of teachers')}} 24</p>  
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-lg-12" style="line-height:20px;">
                            <p>{{trans('lang.SLH 1 Desc')}}</p>   
                        </div>  
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="row">
                        <div class="col-lg-12" >
                            <b>2002</b>    
                        </div>  
                      </div>
                      <div class="row">
                        <div class="col-lg-12" >
                            {{trans('lang.SLH 2 Grade')}}    
                        </div>  
                      </div>
                      <div class="row" style="line-height:20px;">
                        <div class="col-lg-6" >
                            <p>{{trans('lang.Number of students')}} 251</p>    
                        </div>  
                        <div class="col-lg-6" >
                            <p>{{trans('lang.Number of teachers')}} 20</p>  
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-lg-12" style="line-height:20px;">
                            <p>{{trans('lang.SLH 2 Desc')}}</p>   
                        </div>  
                      </div>
                  </div>
              </div>
              <div class="row" align="center">
                  <div class="col-lg-12">
                      <a href="{{route('front.schoolBackground')}}"><b><u class="blue">{{trans('lang.See More Network')}}</u></b></a>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </header>
    
  </section>
  

  @include('frontend.faq_about') 

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
