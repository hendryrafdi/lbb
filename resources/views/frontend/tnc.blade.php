@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--end site header-->
  <section class="white-background">
      <div align="center" class="header-link">
        <label class="active"><a href="{{route('front.policies')}}">{{trans('lang.Term and Conditions')}}</a></label>
        <label><a href="{{route('front.policies')}}">{{trans('lang.Policies')}}</a></label>
        <label><a href="{{route('front.protectionPolicy')}}">{{trans('lang.Student Protection Policy')}}</a></label>
      </div>
    <div class="height30"></div>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2" align='left'>
            <h2 class='blue'>{{trans('lang.Terms of Service')}}</h2>
            <div class="policies-caption" style="text-align:justify;"> 
              <p>
              <b>{{trans('lang.Lentera Bagi Bangsa')}} </b>{{trans('lang.tnc 1')}}
              </p>
              <p>
                {{trans('lang.tnc 2')}}
              </p>
              <p>
                <b>{{trans('lang.General Terms and Conditions')}}</b><br>
              <ol>
                <li>{{trans('lang.You are legally accountable for your own actions while accessing arid using this platform')}}</li>
                <li>{{trans('lang.You must always provide real and accurate personal information, falssification data is prohibited')}}</li>
                <li>{{trans('lang.We do not hold any certification to accommodate tax deductible contributions. This will depend on your country\'s regulations and requirements')}}</li>
                <li>{{trans('lang.We reserve the right to make any changes to the information provide here at any time, for any reason and without notice')}}</li>
              </ol>
              </p>
              <p>
                <b>{{trans('lang.Pemberian')}}<i> {{trans('lang.One-Time Giving and Gifts')}}</i>
                    
                </b><br>
              <ol>
                <li>{{trans('lang.Contributions drom one-time giving will support general SLH operational costs as needed, including LBB recipients not yet supportes by benefactor')}} <i>{{trans('lang.giftstnc')}}</i> {{trans('lang.akan dialokasikan untuk membeli buku dan seragam')}}</li>
                <li>{{trans('lang.Contributions are not refundable')}}</li>
              </ol>
              </p>
              <p>
                <b>{{trans('lang.Supporting a Student (Scholarship)')}}</b><br>
              <ol>
                <li>{{trans('lang.By confirming your scholarship, you agree to abide by our student Protection Policy')}}</li>
                <li>{{trans('lang.You may not request to change students for any personal reason')}}</li>
                <li>{{trans('lang.Upon selecting a student, you reservation will hold for 45 minutes, if you do not complete the check-out process beyond that time, the student will be automatically removed from your cart and return to a waiting status')}} 
                    @if(trans('lang.check-out'))
                    <i>{{trans('lang.check-out')}}</i> {{trans('lang.process beyond that time, the student will be automatically removed from your cart and return to a waiting status')}}
                    @endif
                </li>
                <li>{{trans('lang.Scholarship are non-refundable')}}</li>
                <li>{{trans('lang.You may not request to cancel your scholarship for the remainder oftime commiter. Supporting a child requiress a one-year minimum commitment')}}</li>
                <li>{{trans('lang.In case of recurring monthly plan, if we fail to receive your transaction for two concecutive times, the student  you support will be displayed on the website and available for other befactors to support')}}</li>
                <li>{{trans('lang.If your recipient is no longer a student at SLH (transfer, drop out, death), our staff will notify you immediately and process transferring your scholarship to a different student')}}</li>
                <li>{{trans('lang.100% of your grant will be allocated for the SLH of the student you support')}}</li>
              </ol>
            </p>
            <p>
              <b>{{trans('lang.SLH Tuition Fees')}}</b>
                <ol>
                  <li>{{trans('lang.SLH requests parents of LBB recipients to pledge a minimal of 10% the tuition fee as an act of commitment towards their child\'s education, although this is often disregarded')}}</li>
                  <li>{{trans('lang.SLH tuition fees are subject to change at any given time thus affecting the required amount for student support')}}</li>
                  <li>{{trans('lang.If the tuitionfee should charge after you have begun supporting a student, you will continueto be charged the originalamount until the end of your predetermined commitment')}}</li>
                </ol>
              </p>
            </div>
                        
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
