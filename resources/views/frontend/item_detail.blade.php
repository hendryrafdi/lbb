@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>

  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class='row'>
              <div class='col-md-12 col-xs-12'>
                <?php if($item_type == 'donation'){?>
                <div class="row">
                  <div class="col-md-4 col-md-offset-1 col-xs-12">
                    <!-- <img src='{{URL::to('/')}}/uploads/photo/{{isset($config['image_size']) ? $config['image_size'] : ''}}{{$item->photo}}' style='border-radius: 50%;'> -->
                    <img src='{{URL::to('/')}}/uploads/photo/{{$item->photo}}' style='border-radius: 50%; width: 210px; height: 260px;'>
                  </div>
                  <div class="col-md-7 col-xs-12">
                    <table width="100%" class="item-detail">
                      <tr>
                        <td class="padding10">
                          <label class='blue bold'>{{$item->first_name.' '.$item->last_name}}</label>
                        </td> 
                      </tr>
                      <tr>
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          <label>{{trans('lang.Dream')}} :</label><label class="bold"> {{$item->dream}}</label>
                        </td> 
                      </tr>
                      <tr>
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          <label>{{trans('lang.Gender')}} :</label><label class="bold"> {{isset($item->gender) && $item->gender == 'M' ? trans('lang.male') : trans('lang.female')}}</label>
                        </td> 
                      </tr>
                      <tr>
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          <label>{{trans('lang.Birthday')}} :</label><label class="bold"> 
                          @if($session['language']=='en')
                          {{isset($item->birth_date)? date_format(date_create($item->birth_date),'F d, Y') : $item->birth_date}}
                          @else
                          @php
                          function tgl_indo($tanggal){
                        	$bulan = array (
                        		1 =>   'Januari',
                        		'Februari',
                        		'Maret',
                        		'April',
                        		'Mei',
                        		'Juni',
                        		'Juli',
                        		'Agustus',
                        		'September',
                        		'Oktober',
                        		'November',
                        		'Desember'
                        	);
                        	$pecahkan = explode('-', $tanggal);
                        	
                        	// variabel pecahkan 0 = tanggal
                        	// variabel pecahkan 1 = bulan
                        	// variabel pecahkan 2 = tahun
                         
                        	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                        }
                         
                        echo tgl_indo(date($item->birth_date));
                          @endphp
                          @endif
                          </label>
                        </td> 
                      </tr>
                      <tr>
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          <?php
                          $age = date('Y') - substr($item->birth_date,0,4);
                          ?>
                          <label>{{trans('lang.Age')}} :</label><label class="bold"> {{$age}}</label>
                        </td> 
                      </tr>
                      <tr>
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          <label>{{trans('lang.School')}} :</label><label class="bold"> {{$item->school->name. ' ' . $item->school->region}}</label>
                        </td> 
                      </tr>
                      <tr>
                          <!--remarks-->
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          <label>{{trans('lang.Grade')}} :</label><label class="bold"> 
                          @if($session['language'] == 'en')
                          {{$item->alias1}}
                          @else
                          {{$item->alias2}}
                          @endif
                          </label>
                        </td> 
                      </tr>
                      <tr>
                        <td class="padding10" style="padding-top:0px;padding-bottom:0px">
                          @if(Session::get('language') == 'en')
                          <label>{!! $item->parent_background_en !!}</label>
                          @else
                          <label>{!! $item->parent_background_id !!}</label>
                          @endif
                        </td> 
                      </tr>
                    </table>
                  </div>
                </div>
                      
                <?php }else{ ?>                
                <table width='100%'>
                  <tr>
                    <td width="40%" align="right" class="padding10">
                        <img src='{{URL::to('/')}}/uploads/items/{{isset($config['image_size']) ? $config['image_size'] : ''}}{{$item->picture}}' style='border-radius: 50%;' class="img-responsive">
                    </td>
                    <td valign="top" width="60%">
                      <table>
                        <tr>
                          <td align="left" class="padding10">
                            <label class='blue bold'>
                            @if($session['language'] == 'en')
                                {{$item->name}}
                            @else
                                {{$item->name_id}}
                            @endif
                            </label>
                          </td> 
                        </tr>
                        <tr>
                          <td align="left" class="padding10" style="padding-top:0px;padding-bottom:0px">
                            @if($session['language'] == 'en')
                              <label class='item-description'>{!! $item->description_en !!}</label>
                            @else
                              <label class='item-description'>{!! $item->description_id !!}</label>
                            @endif                            
                          </td> 
                        </tr>
                        <tr>
                          <td align="left" class="padding10" style="padding-top:0px;padding-bottom:0px">
                            <label>
                                {{trans('lang.Price / Unit')}} 
                                @if($session['language'] == 'en')
                                Rp {{number_format($item->price,0,",",",").",-"}}
                                @else
                                Rp {{number_format($item->price,0,",",".").",-"}}
                                @endif
                            </label>
                          </td>                          
                        </tr>
                      </table>
                    </td>
                  </tr>

                </table>
                
                <?php }?>
              </div>                  
            </div>
                    
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  <section>
    <div class='row' align="center">
      <?php if($item_type == 'donation'){?>
      <div class="col-md-12">
          @php
          if($session['language'] == 'en'){
            $price_detail_v = number_format($price_detail->price,0,",",",");
            }else{
            $price_detail_v = number_format($price_detail->price,0,",",".");
            }
          @endphp
        {{trans('lang.Tuition for')}} {{$item->first_name.' '.$item->last_name}} <b>IDR <?php echo $price_detail !== null ? $price_detail_v : 0  ?> per {{trans('lang.month')}}</b>.<br>
      {{$item->first_name.' '.$item->last_name}} {{trans('lang.needs supported for')}}
      @if ($item->academy == 'SD')
      @php $max_year = (6 - $item->grade + 1) @endphp
      @elseif ($item->academy == 'SMP')
      @php $max_year = (3 - $item->grade + 1) @endphp
      @elseif ($item->academy == 'SMA')
      @php $max_year = (3 - $item->grade + 1) @endphp
      @elseif ($item->academy == 'TK')
      @php $max_year = (2 - $item->grade + 1) @endphp
      @else
      @php $max_year = (3 - $item->grade + 1) @endphp
      @endif
      {{$max_year}}
      {{trans('lang.years to end of')}} {{$item->alias4}}<br>
      <button class='btn btn-green' id='donation-btn' onClick='lbb.showDonationQty()'> {{trans('lang.Support')}} {{$item->first_name}}</button>
      </div>
      
      <div id='donation-qty-div' class="row donation-qty-div">
        <div class="col-md-12" align='center'>
          {{trans('lang.Choose length of support')}}
        </div>
        <div class="col-md-12" align='center'>
          <input type="number" class="" max="{{$max_year}}" min="1" id="qty" size="2" value="{{$max_year}}"> {{trans('lang.year')}}
        </div>
        <div class="col-md-12 padding10" align='center'>
          <button class='btn btn-green' id='add-basket-btn' onClick='lbb.addBasket()'> {{trans('lang.Add basket')}}</button> <label><a href="#" onclick="lbb.showDonationBtn()">{{trans('lang.cancel')}}</a></label>
        </div>
      </div>
      
      <?php }else{?> 
      <div id='book-qty-div' class="row">
        <div class="col-md-12" align='center'>
          {{trans('lang.Quantity')}}
        </div>
        <div class="col-md-12" align='center'>
          <input style="width: 60px;" type="number" class="" min="1" id="qtybook" size="1" value="1" > {{$item->name}}
        </div>
        <div class="col-md-12 padding10">
          <button class='btn btn-green' id='add-basket-btn' onClick='lbb.addBasket()'> {{trans('lang.Give Now')}}</button>
        </div>
      </div>
      <?php } ?>
      </div>
  </section>  

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  lbb.addBasket = function(){
    @if($item_type == 'gift')
      var data = {
        'id'        : "{{$item['id']}}",
        'code'      : "{{$item['code']}}",
        'price'     : "{{$item['price']}}",
        'name'      : "{{$item['name']}}",
        'item_type' : "{{$item_type}}",
        'picture'   : "{{URL::to('/')}}/uploads/items/{{isset($config['image_size']) ? $config['image_size'] : ''}}{{$item->picture}}",
        'qty'       : $("#qtybook").val(),
      }
    @else
      var data = {
        'id'    : "{{$item['product_id']}}",
        'code'  : "{{$item['code']}}",
        'price' : "{{$price_detail !== null ? $price_detail->price : 0}}",
        'student' : {
          'id' : "{{$item['id']}}",
          'name' : "{{$item['first_name']. ' ' . $item['last_name']}}",  
          'first_name' : "{{$item['first_name']}}",
          'school' : "{{$item['slh_location']}}",        
          'academy' : "{{$item['academy']}}",        
          'grade' : "{{$item['grade']}}",    
          'picture' : "{{URL::to('/')}}/uploads/photo/{{$item->photo}}",
        },
        'name' : "{{$item['product_name']}}",
        'item_type' : "{{$item_type}}",        
        'qty' : $("#qty").val(),
      }
    @endif
    
    if(data.item_type == 'donation'){
      $("#donation-qty-div").hide();
      $("#donation-btn").show();
    }
    
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: "{{route('front.addCart')}}",
      type: 'post',
      data: data,
      beforeSend: function() {
          
      },
      complete: function() {
          
      },
      error : function(){
        alert('Time Out, please try again');  
      },
      success: function(data) {
        var response = JSON.parse(data);
        alert('item have been add to your cart');
        lbb.cartContent();
      }
    });
  }
</script>
@stop
