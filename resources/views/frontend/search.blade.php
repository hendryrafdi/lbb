@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>
  <!--end site header-->
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <form method="get" action="{{route('front.donation')}}"> 
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1" align="left">
            <input name="name" class="search-input" placeholder="{{trans('lang.Find students')}}">
            <input type="submit" value="{{trans('lang.Search')}}" name="search" id="mc-embedded-subscribe" class="button search-btn">
          </div>
        </div>
        <div class="row">
          <div class="height30"></div>
        </div>
        <div class="row">
          <div class="col-md-10 blue col-md-offset-1" align="left">
            {{trans('lang.School Locations')}}
          </div>
        </div>
        <div class="row">
          <div class="col-md-10 col-md-offset-1" align="left">
            <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <input type="checkbox" name="slh_locations[]" value="all"> {{trans('lang.All')}}
                </div>
                <?php
                $i = 1;
                foreach($school_param as $location){
                  if($i == 3){?>
                    </div>
                    <div class="row">
                  <?php 
                  $i=0;
                  }?>
                  <div class="col-md-3">
                    <input type="checkbox" name="slh_locations[]" value="{{$location->slh_location}}"> {{$location->slh_location}}
                  </div>
                  <?php
                  $i++;
                }
                ?>
                
              </div>
              <div class="height30"></div>
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="row">
                    <div class="col-md-12 col-xs-12 blue">
                      {{trans('lang.Grade')}}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-4">
                      <input type="checkbox" name="grade[]" value="all"> {{trans('lang.All')}}
                    </div>
                    <?php
                    $n = 1;
                    foreach($grade_param as $grade){
                      if($n == 3){?>
                        </div>
                        <div class="row">
                      <?php 
                      $n=0;
                      }?>
                      <div class="col-md-4 col-xs-4">
                        <input type="checkbox" name="grade[]" value="{{$grade->academy}}"> 
                        @if($session['language']=="en")
                        {{$grade->alias3}}
                        @else
                        {{$grade->academy}}
                        @endif
                      </div>
                      <?php
                      $n++;
                    }
                    ?>                    
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="row">
                    <div class="col-md-12 col-xs-12 blue">
                      {{trans('lang.Gender')}}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-4">
                      <input type="checkbox" name="gender[]" value="F"> {{trans('lang.Female')}}
                    </div>
                    <div class="col-md-4 col-xs-4">
                      <input type="checkbox" name="gender[]" value="M"> {{trans('lang.Male')}}
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="height30"></div>
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="row">
                    <div class="col-md-12 col-xs-12 blue">
                      {{trans('lang.Age')}}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-xs-12">
                      {{trans('lang.What is the age range of students that you would like to support')}}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div id="age-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                        <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 0%;"></div>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span>
                      </div>
                      <input class="form-control optimus-input" id="age1" name="age1" value="" type="hidden">
                      <input class="form-control optimus-input" id="age2" name="age2" value="" type="hidden">
                      <div id="age"></div>
                    </div>
                  </div>
                  
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="row">
                    <div class="col-md-12 col-xs-12 blue">
                      {{trans('lang.Years until graduation')}}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-10 col-xs-12">
                      {{trans('lang.We encourage our donors to support a student until graduation from current stage of schooling (Kindergarten, Primary, Junior, Senior). For how many year(s) would you like to support a student')}}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div id="year-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                        <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 0%;"></div>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span>
                      </div>
                      <input class="form-control optimus-input" id="year1" name="year1" value="" type="hidden">
                      <input class="form-control optimus-input" id="year2" name="year2" value="" type="hidden">
                      <div id="year"></div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            
          </div>
        </div>
        
      </div>
      </form>
    </header>
  </section>
  
  @include('frontend.gift_section')  

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
  $("#age-range").slider({
    range: true,
    min: parseInt("{{$min_age}}"),
    max: parseInt("{{$max_age}}"),
    step: 1,
    //values: [ 1000, 300000 ],
    slide: function (event, ui) {
      $("#age1").val(ui.values[ 0 ]);
      $("#age2").val(ui.values[ 1 ]);
      $("#age").html(ui.values[ 0 ] + " th - " + ui.values[ 1 ] + " th");
    }
  });
  $("#year-range").slider({
    range: true,
    min: 1,
    max: 6,
    step: 1,
    //values: [ 1000, 300000 ],
    slide: function (event, ui) {
      $("#year1").val(ui.values[ 0 ]);
      $("#year2").val(ui.values[ 1 ]);
      $("#year").html(ui.values[ 0 ] + " th - " + ui.values[ 1 ] + " th");
    }
  });
</script>
@stop
