<section class="white-background">
  <div class="container">
  <div class='row'>
    <div class="col-lg-3 col-lg-offset-1">
      <img src="{{URL::to('/')}}/assets/images/FAQ.png" width="100%" style='margin-top:-60px'>
    </div>
    <div class='col-lg-7 col-md-7'>
      
      <div class="accordion-3 style-2" align="justify">
        <div id="accordion-4" class="panel-group">

          <div class="panel">
            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel16" class="panel-title collapsed" aria-expanded="false">
                <h5 class="blue">{{trans('lang.[Gift Catalog] What will my money do')}}</h5></a></div>
            <div id="panel16" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                  {{trans('lang.Contribution toward specific gifts will be allocated for purchasing the items upon request from any SLH at the discretion of YPPH')}}
                  <i>{{trans('lang.gifts')}} </i>{{trans('lang.akan dialokasikan untuk membeli barang-barang tersebut atas permintaan SLH dan persetujuan YPPH')}}
              </div>
            </div>
          </div>
          
          <div class="panel">
            <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion-4" href="#panel17" class="panel-title collapsed" aria-expanded="false">
                <h5 class="blue">{{trans('lang.[Gift Catalog] Why is there a minimum amount for each gift item')}}</h5></a></div>
            <div id="panel17" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                  {{trans('lang.The minimum amount is the actual amount needed to purchase the gift')}}
                  <i>{{trans('lang.gifts')}} </i>{{trans('lang.tersebut')}}
              </div>
            </div>
          </div>
          
        </div>
      </div>
      
      <p><a href="{{route('front.faq')}}"><b><u class='blue'>{{trans('lang.See More Question')}}</u></b></a></p>
      <p align='center'><a href="{{route('front.gift')}}"><button class='btn btn-green'> &oplus; {{trans('lang.Give Now')}} </button></a></p>
    </div>
  </div>    
  </div>
</section>