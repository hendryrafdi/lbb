@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <section class="white-background">
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 col-lg-offset-1">
            <h2 class='blue'>Activation Account</h2>
            <p>Your account activated successfully!</p>
          </div>
        </div>
      </div>
    </header>
  </section>
  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
