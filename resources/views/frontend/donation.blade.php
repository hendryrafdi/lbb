@extends('frontend.layouts.master')

@section('content')

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="main-nav-wrapper nav-wrapper-2">
  @include('frontend.layouts.menu')  
</div>
<main>
  <!--site header-->
  <header class="site-header"></header>
  <!--end site header-->
  <section>
    <header class="section-header header-type-1 style-1">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <a href="{{route('front.search')}}"><button class="btn btn-white"> <i class="fa fa-search"></i> {{trans('lang.Find Student by Name or Filters')}}</button></a>
          </div>
        </div>
        <div class="row">
          <div class="height30"></div>
        </div>
        <div class="row">
          <div class="col-lg-12">       
            <div class='row'>
            <?php
            $i = 0;
    
            foreach($items as $item){
              $birth_date = $item->birth_date;
              $age = date('Y') - substr($birth_date,0,4);
              $gender = $item->gender;
              if($i==3){
                $i=0;
                ?>
                </div><div class='row'>
              <?php } ?>
                  <div class='col-md-4' style="padding:10px; height: 200px;">
                      <!--//remarks-->
                    <a href="{{route('front.detail',['type'=>'donation','id'=>$item->id])}}">
                    <table class='white-background' width='100%'>
                      <tr>
                        <td colspan="2" align='right'><label class='student-school'>{{$item->slh_location}}</label></td>
                      </tr>
                      <tr>
                        <td rowspan="2" width='30%'>
                            <img src='{{URL::to('/')}}/uploads/photo/{{$item->photo}}' width="70%" style='border-radius: 50%; width: 60px; height: 100px;'>
                        </td>
                        <td width='70%' align='left'>
                          <label class='blue'> <b>{{$item->first_name.' '.$item->last_name}}</b></label>
                        </td>                        
                      </tr>
                      <tr>
                        <td align='left'>{{$item->first_name}} is {{$age}} years old. 
                          <?php if($gender == 'M'){?>
                          He 
                          <?php }else{?>
                          She 
                          <?php }?>
                          dreams of becoming a {{$item->dream}}
                          </td>
                      </tr>
                      <tr>
                        <td>
                          @if($session['language'] == 'en')
                          <label class='student-name'>{{$item->alias1}}</label>
                          @else
                          <label class='student-name'>{{$item->alias2}}</label>
                          @endif
                        </td>
                      </tr>
                    </table>
                    </a>
                  </div>   
            <?php }?>
                </div>
                <div class='row'>
                  <div class='col-md-12 paging' align='center'>{{$items->links()}}</div>
                </div>
                    
          </div>
        </div>
        
      </div>
    </header>
  </section>
  
  @include('frontend.gift_section')  

  <!--site footer-->
  @include('frontend.layouts.footer')
</main>

@stop


@section('script')
<script>
  var page_type = "{{$page_type}}";
</script>
@stop
