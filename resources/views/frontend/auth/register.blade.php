@extends('frontend.layouts.register')

@section('content')
<div class="container signup-box">
    <div class="row box-floating">
        <div class="col">
            <!-- <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('frontend.register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> -->
            <!-- <?= $data = extract($register); ?> -->
            <div class="container-fluid signup-panel">
              <div align="center">
                    <img src='{{URL::to('/')}}/assets/images/logo_black.png'>
              </div>
              <div class='row signup-form'>
                <div class="row">
                  <div class='col-md-12 col-xs-12' align='center'>
                    <input class="form-control email-signup login-input" type='text' value="{!! $email !!}" id='email' placeholder="{{trans('lang.email')}}" style="font-size: 13px;">
                  </div>
                </div>
                <div class="row">
                  <div class='col-md-12 col-xs-12' align='center'>
                    <input class="form-control name-signup login-input" type='text' value='{{ $name }}' id='name' placeholder="{{trans('lang.name')}}" style="font-size: 13px;">
                  </div>
                </div>
                <div class="row">
                  <div class='col-md-12 col-xs-12' align='center'>
                    <input class="form-control password-signup login-input" type='password' value='' id='password' placeholder='{{trans('lang.password')}}' style="font-size: 13px;">
                  </div>
                </div>
                <div class="row">
                  <div class='col-md-12 col-xs-12' align='center'>
                    <input class="form-control password-signup-2 login-input" type='password' value='' id='password-confirmation' placeholder='{{trans('lang.password confirmation')}}' style="font-size: 13px;">
                  </div>    
                </div>
                <div class="row">
                  <div class='col-md-12 col-xs-12' align='center'>
                    <button class='normal-btn normal-btn-main section-block btn-size-1' id="signup-submit-btn-g" style="font-size: 13px;">{{trans('lang.signup')}}</button>
                  </div>
                </div>
                <div class="row">
                  <div class='col-md-12 col-xs-12' align='center'>
                    {{trans('lang.have an account')}}? <a href = "#" class="login-btn">{{trans('lang.click here')}}</a>
                  </div>
                </div>
              </div>

              <div class="row response-signup">
                <div class='col-md-2 col-xs-2' id="signup_response_symbol" align="right"></div>
                <div class='col-md-10 col-xs-10' align='left' id="signup_response"></div>
              </div>

            </div>
        </div>
    </div>
</div>
@endsection
