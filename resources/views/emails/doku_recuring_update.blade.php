<!DOCTYPE html>
<html>
  <head>
    <title>Update Payment</title>
  </head>
  <body>
    Hello {{$name}},<br><br>
    Your payment at invoice {{$invoice_number}} can't be charge maybe there is a trouble with your payment credit card, your payment is pending. Please check your credit card and update your credit card info.<br>
    Click <a href="{{ $link }}">Here</a> to update your payment.
  </body>
</html>