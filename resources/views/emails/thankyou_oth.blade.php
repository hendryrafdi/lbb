<!DOCTYPE HTML>
<html>
    <head>
        <title>Thank you for your donation</title>
    </head>
    <body>
        Yth. Bapak/Ibu {{ $name }}
        <br>
        <br>
        Donasi Anda telah kami terima.<br>
        Terima kasih atas perhatian dan cinta kasih yang diberikan untuk anak-anak yang paling membutuhkan di Sekolah Lentera Harapan (SLH). Donasi anda membantu melanjutkan sekolah dan belajar lebih baik.
        <br>
        <br>
        Salam,<br>
        <br>
        Lentera Bagi Bangsa<br>
        <br>
        <hr>
        <br>
        Dear Mr. / Mrs. {{ $name }}
        <br>
        <br>
        We have received your donation.<br>
        Thank you for your attention and love for the children who need it most in the Sekolah Lentera Harapan. Your donation helps continue school and study better.
        <br>
        <br>
        Regards,<br>
        <br>
        Lentera Bagi Bangsa
    </body>
</html>