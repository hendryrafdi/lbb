<!DOCTYPE html>
<html>
  <head>
    <title>Registration Success</title>
  </head>
  <body>
    Yth. Bapak / Ibu {{ $name }},
    <br><br><br>
    Terima kasih, Anda telah mengisi data donatur LBB. <br>
    Jika Anda memiliki pertanyaan, silahkan menghubungi kami melalui telepon di +62 811 894 7500 dan melalui email di <a href="mailto:info@lenterabagibangsa.org">info@lenterabagibangsa.org</a>.<br>
    Silahkan mengikuti Instagram <a href="https://www.instagram.com/lenterabagibangsa/">@lenterabagibangsa</a> dan Facebook <a href="https://www.facebook.com/lenterabagibangsa/">LenteraBagiBangsa</a> agar Anda dapat mengikuti foto dan informasi terbaru bagaimana bantuan Anda menolong pendidikan anak-anak di Sekolah Lentera Harapan seluruh Indonesia.<br>
    <br>
    Untuk mulai menggunakan akun anda, silahkan klik <a href="{{ $link }}">tautan berikut</a> untuk mengaktivasi akun anda.
    <br>
    <br>  
    Hormat kami,<br>
    <br>
    Lentera Bagi Bangsa
    <br>
    <hr>
    <br>
    Dear Mr. / Mrs. {{ $name }},
    <br><br><br>
    Thank you, you have filled in data for LBB donor. <br>
    If you have any questions, please do not hesitate to contact us directly by phone at +62 811 894 7500 and by email at <a href="mailto:info@lenterabagibangsa.org">info@lenterabagibangsa.org</a>.<br>
    Have a minute? Follow us on Instagram <a href="https://www.instagram.com/lenterabagibangsa/">@lenterabagibangsa</a> and like us on Facebook <a href="https://www.facebook.com/lenterabagibangsa/">LenteraBagiBangsa</a> to keep you up to date with all our photos and news on how your support helps education of children at Sekolah Lentera Harapan throughout Indonesia.<br>
    <br>
    To start using your account, please click <a href="{{ $link }}">this link</a> for activate your account.
    <br>
    <br>  
    Regards,<br>
    <br>
    Lentera Bagi Bangsa
  </body>
</html>