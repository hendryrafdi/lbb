<!DOCTYPE html>
<html>
  <head>
    <title>Forgot Password</title>
  </head>
  <body>
     Hai {{ $name }},
    <br><br><br>
    Berikut adalah password sementara anda: <b>{{ $pass }}</b>. <br>Harap untuk segera mengganti password anda.
    <br>
    <hr>  
    <br>
    Salam,<br><br>
    Lentera Bagi Bangsa
    Hi {{ $name }},
    <br><br><br>
    Here is your new temporary password: <b>{{ $pass }}</b>. <br>Please change your temporary password.
    <br>
    <br>  

    Regards,<br><br>
    Lentera Bagi Bangsa
  </body>
</html>