<!DOCTYPE HTML>
<html>
    <head>
        <title>Thank you for your donation</title>
    </head>
    <body>
        Yth. Bapak/Ibu {{ $name }}
        <br>
        <br>
        Donasi Anda telah kami terima.<br>
        Terima kasih atas perhatian dan cinta kasih yang diberikan untuk anak-anak yang paling membutuhkan di Sekolah Lentera Harapan (SLH). <br>
        Donasi anda membantu:
        @php
        if($i > 1){
            echo "<ul>";
            for($a=1; $a <= $i; $a++){
                echo "<li>".${'student_name_'.$a}."</li>";
            }
            echo "</ul>";
        } else {
            echo ${'student_name_'.$i};
        }
        @endphp
        untuk melanjutkan sekolah dan belajar lebih baik.
        <br>
        <br>
        Salam,<br>
        <br>
        Lentera Bagi Bangsa<br>
        <br>
        <hr>
        <br>
        Dear Mr. / Mrs. {{ $name }}
        <br>
        <br>
        We have received your donation.<br>
        Thank you for your attention and love for the children who need it most in the Sekolah Lentera Harapan. <br>
        Your donation helps:
        @php
        if($i > 1){
            echo "<ul>";
            for($a=1; $a <= $i; $a++){
                echo "<li>".${'student_name_'.$a}."</li>";
            }
            echo "</ul>";
        } else {
            echo ${'student_name_'.$i};
        }
        @endphp 
        to continue school and study better.
        <br>
        <br>
        Regards,<br>
        <br>
        Lentera Bagi Bangsa
    </body>
</html>