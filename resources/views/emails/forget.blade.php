<!DOCTYPE html>
<html>
  <head>
    <title>Forget Password</title>
  </head>
  <body>
    Click <a href="{{ route('frontend.password.reset', ['token'=> $token]) }}">Here</a> to reset your password.
  </body>
</html>