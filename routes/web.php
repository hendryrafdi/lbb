<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'front.home', 'uses' => 'HomeController@index' ]);
Route::get('/test_email', ['as' => 'front.test_email', 'uses' => 'HomeController@test_email' ]);
Route::get('/language/{lang?}', ['as' => 'front.setLanguage', 'uses' => 'HomeController@setLanguage' ]);
Route::get('/about', ['as' => 'front.about', 'uses' => 'PageController@about' ]);
Route::get('/faq', ['as' => 'front.faq', 'uses' => 'PageController@faq' ]);
Route::get('/students', ['as' => 'front.students', 'uses' => 'PageController@students' ]);
Route::get('/session', ['as' => 'front.session', 'uses' => 'PageController@session' ]);
Route::get('/policies', ['as' => 'front.policies', 'uses' => 'PageController@policies' ]);
Route::get('/tnc', ['as' => 'front.tnc', 'uses' => 'PageController@tnc' ]);
Route::get('/protection-policy', ['as' => 'front.protectionPolicy', 'uses' => 'PageController@protectionPolicy' ]);
Route::get('/school_background', ['as' => 'front.schoolBackground','uses' => 'PageController@schoolBackground' ]);
Route::get('/regis', ['as' => 'front.regis', 'uses' => 'PageController@regis' ]);
Route::get('/checkout', ['as' => 'front.checkout', 'uses' => 'CheckoutController@index' ]);

Route::post('/api/cart', ['as' => 'front.cartContent', 'uses' => 'CartController@store' ]);
Route::post('/api/add-cart', ['as' => 'front.addCart', 'uses' => 'CartController@addCart' ]);
Route::post('/api/update-cart', ['as' => 'front.updateCart', 'uses' => 'CartController@updateCart' ]);
Route::post('/api/remove-cart', ['as' => 'front.removeCart', 'uses' => 'CartController@removeCart' ]);
Route::post('/login-submit', ['as' => 'front.loginSubmit', 'uses' => 'LoginController@store' ]);
Route::post('/logout', ['as' => 'front.logout', 'uses' => 'LoginController@logout' ]);
Route::get('/logout', ['as' => 'front.logoutGet', 'uses' => 'LoginController@logoutGet' ]);
Route::post('/signup', ['as' => 'front.signup', 'uses' => 'LoginController@signup' ]);
Route::post('/signupgoogle', ['as' => 'front.signupgoogle', 'uses' => 'LoginController@signupgoogle' ]);
Route::post('/reset-password', ['as' => 'front.resetPassword', 'uses' => 'LoginController@resetPassword' ]);
Route::post('/check-session', ['as' => 'front.checkSession', 'uses' => 'LoginController@checkSession' ]);
Route::get('/activate/{email}', ['as' => 'front.activate', 'uses' => 'LoginController@activate' ]);

Route::post('/payment-check', ['as' => 'front.paymentSubmit', 'uses' => 'PaymentController@checkPayment' ]);
Route::get('/payment', ['as' => 'front.payment', 'uses' => 'PaymentController@index' ]);

Route::get('payment.notify.doku', 'PaymentController@DokuNotifyController');
Route::post('payment.notify.doku', 'PaymentController@DokuNotifyController');
Route::post('payment.notify.doku.recuring', 'PaymentController@DokuRecuringNotifyController');
Route::get('payment.redirect.doku', 'PaymentController@DokuRedirectController');
Route::post('payment.redirect.doku', 'PaymentController@DokuRedirectController');
Route::get('verify-payment/{payment_code}', ['as' => 'front.updateDoku', 'uses' => 'PaymentController@UpdateDokuController' ]);
Route::post('payment.notify.doku.recuring.update', 'PaymentController@UpdateProcessNotify');

Route::get('/thankyou', ['as' => 'front.thankyou', 'uses' => 'PageController@thankyou' ]);
Route::get('/failed', ['as' => 'front.failed', 'uses' => 'PageController@failed' ]);
Route::get('/account', ['as' => 'front.account', 'uses' => 'AccountController@index' ]);
Route::get('/donation', ['as' => 'front.donation', 'uses' => 'ItemController@donation' ]);
Route::get('/detail/{type?}/{id?}', ['as' => 'front.detail', 'uses' => 'ItemController@detail' ]);
Route::get('/gift', ['as' => 'front.gift', 'uses' => 'ItemController@gift' ]);
Route::get('/search', ['as' => 'front.search', 'uses' => 'PageController@search' ]);


// LOGIN
Route::get('login', ['as' => 'frontend.login', 'uses' => 'Auth\LoginController@showLoginForm' ]);
Route::post('login', [   'as' => 'frontend.login', 'uses' => 'Auth\LoginController@login']);
Route::get('/login/{provider}', [ 'as' => 'frontend.login.social', 'uses' => 'Auth\LoginController@redirectToProvider']);
Route::get('/login/{provider}/callback', [ 'as' => 'frontend.login.social.callback', 'uses' => 'Auth\LoginController@handleProviderCallback']);

// REGISTER 
Route::get('register', ['as' => 'frontend.register', 'uses' => 'Auth\RegisterController@showRegistrationForm' ]);
Route::post('register', [   'as' => '', 'uses' => 'Auth\RegisterController@register']);

// PASSWORD RESET
Route::post('password/email', [ 'as' => 'frontend.password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset', ['as' => 'frontend.password.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/reset', ['as' => '', 'uses' => 'Auth\ResetPasswordController@reset']);
Route::get('password/reset/{token}', ['as' => 'frontend.password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);

Route::post('password/forgot', [ 'as' => 'frontend.password.forgot', 'uses' => 'LoginController@forgotPassword']);
Route::get('test', ['as' => 'test', 'uses' => 'PaymentController@test']);

// DASHBOARD
Route::middleware(['auth.customer'])->group(function () {
    Route::post('logout', [ 'as' => 'frontend.logout', 'uses' => 'Auth\LoginController@logout' ]);
    Route::get('dashboard/transactions', ['as' => 'frontend.dashboard.transaction', 'uses' => 'TransactionController@index']);
    Route::get('dashboard/customer', ['as' => 'frontend.dashboard.student', 'uses' => 'StudentController@index']);
    Route::get('dashboard/profile', ['as' => 'frontend.dashboard.profile', 'uses' => 'ProfileController@edit']);
    Route::put('dashboard/profile', ['as' => 'frontend.dashboard.profile.update', 'uses' => 'ProfileController@update']);
});

/*-------------------------*/
/*     BACKEND ROUTES      */
/*-------------------------*/

Route::prefix('admin')->namespace('Admin')->group(function () {
    // LOGIN
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm' ]);
    Route::post('login', [   'as' => '', 'uses' => 'Auth\LoginController@login']);

    // PASSWORD RESET
    Route::post('password/email', [ 'as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset', ['as' => 'password.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/reset', ['as' => '', 'uses' => 'Auth\ResetPasswordController@reset']);
    Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);

    Route::middleware(['auth'])->group(function () {
        Route::post('logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout' ]);
        Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@index' ]);
        Route::get('profile', [ 'as' => 'admin.profile.index', 'uses' => 'ProfileController@edit']);
        Route::put('profile', [ 'as' => 'admin.profile.update', 'uses' => 'ProfileController@update']);

        // USER
        Route::get('user/data', [ 'as' => 'admin.user.data', 'uses' => 'UserController@data']);
        Route::resource('user', 'UserController', ['as' => 'admin']);

        // PERMISSION
        Route::get('permission/data', [ 'as' => 'admin.permission.data', 'uses' => 'PermissionController@data']);
        Route::resource('permission', 'PermissionController', ['as' => 'admin']);

        // ROLE
        Route::get('role/data', [ 'as' => 'admin.role.data', 'uses' => 'RoleController@data']);
        Route::resource('role', 'RoleController', ['as' => 'admin']);

        // STUDENT
        Route::get('student/data', [ 'as' => 'admin.student.data', 'uses' => 'StudentController@data']);
        Route::post('student/upload', [ 'as' => 'admin.student.upload', 'uses' => 'StudentController@upload']);
        Route::get('student/download', [ 'as' => 'admin.student.download', 'uses' => 'StudentController@download']);
        Route::resource('student', 'StudentController', ['as' => 'admin']);

        // SCHOOL
        Route::get('school/data', [ 'as' => 'admin.school.data', 'uses' => 'SchoolController@data']);
        Route::resource('school', 'SchoolController', ['as' => 'admin']);

        //setting
        Route::get('setting/data', [ 'as' => 'admin.setting.data', 'uses' => 'SettingController@data']);
        Route::resource('setting', 'SettingController', ['as' => 'admin']);
        
        // ITEM
        Route::get('item/data', [ 'as' => 'admin.item.data', 'uses' => 'ItemController@data']);
        Route::resource('item', 'ItemController', ['as' => 'admin']);

        // PRICE LIST
        Route::get('pricelist/data', [ 'as' => 'admin.pricelist.data', 'uses' => 'PriceListController@data']);
        Route::get('pricelist-line/{pricelist_id?}', [ 'as' => 'admin.pricelistline.pricelist', 'uses' => 'PriceListLineController@getPriceListLineByPricelistId']);
        Route::resource('pricelist', 'PriceListController', ['as' => 'admin']);

        // CUSTOMER
        Route::get('customer/data', [ 'as' => 'admin.customer.data', 'uses' => 'CustomerController@data']);
        Route::resource('customer', 'CustomerController', ['as' => 'admin']);

        // TRANSACTION
        Route::get('transaction/data', [ 'as' => 'admin.transaction.data', 'uses' => 'TransactionController@data']);
        Route::get('transaction', [ 'as' => 'admin.transaction.index', 'uses' => 'TransactionController@index']);
        Route::get('transaction/print/{transaction}', [ 'as' => 'admin.transaction.print', 'uses' => 'TransactionController@printPage']);
        Route::get('transaction/{transaction}', [ 'as' => 'admin.transaction.show', 'uses' => 'TransactionController@show']);
        
        // COORPORATE
        Route::get('coorporate', ['as' => 'admin.coorporate.index', 'uses' => 'CoorporateController@index']);
        Route::get('coorporate/data', [ 'as' => 'admin.coorporate.data', 'uses' => 'CoorporateController@data']);
        Route::get('coorporate/datastudent/{id}', [ 'as' => 'admin.coorporate.datastudent', 'uses' => 'CoorporateController@dataStudent']);
        Route::get('coorporate/datapayment', [ 'as' => 'admin.coorporate.datapayment', 'uses' => 'CoorporateController@dataPayment']);
        Route::get('coorporate/edit/{id}', [ 'as' => 'admin.coorporate.edit', 'uses' => 'CoorporateController@edit']);
        Route::get('coorporate/payment/{id}', [ 'as' => 'admin.coorporate.payment', 'uses' => 'CoorporateController@payment']);
        Route::post('coorporate/updatedetailpayment', ['as' => 'admin.coorporate.updatedetailpayment', 'uses' => 'CoorporateController@updateDetailPayment']);
        Route::post('coorporate/deletepayment', ['as' => 'admin.coorporate.deletedetailpayment', 'uses' => 'CoorporateController@deleteDetailPayment']);
        Route::post('coorporate/store', ['as' => 'admin.coorporate.store', 'uses' => 'CoorporateController@store']);
        Route::post('coorporate/upload', [ 'as' => 'admin.coorporate.upload', 'uses' => 'CoorporateController@upload']);
        Route::post('coorporate/uploadpayment', [ 'as' => 'admin.coorporate.uploadpayment', 'uses' => 'CoorporateController@uploadPayment']);
        Route::post('coorporate/loadpayment', ['as' => 'admin.coorporate.loadpayment', 'uses' => 'CoorporateController@loadPayment']);
        Route::get('coorporate/downloadpayment/{id}', [ 'as' => 'admin.coorporate.downloadpayment', 'uses' => 'CoorporateController@downloadpayment']);
        Route::get('coorporate/downloadstudent/{id}', [ 'as' => 'admin.coorporate.downloadstudent', 'uses' => 'CoorporateController@downloadstudent']);
        // Route::resource('coorporate', 'CoorporateController', ['as' => 'admin']);
    });
});


/*--------------------------*/ 
/*     FRONTEND ROUTES      */
/*--------------------------*/

// LOGIN
Route::get('login', ['as' => 'frontend.login', 'uses' => 'Auth\LoginController@showLoginForm' ]);
Route::post('login', [   'as' => '', 'uses' => 'Auth\LoginController@login']);

// REGISTER
Route::get('register', ['as' => 'frontend.register', 'uses' => 'Auth\RegisterController@showRegistrationForm' ]);
Route::post('register', [   'as' => '', 'uses' => 'Auth\RegisterController@register']);

// PASSWORD RESET
Route::post('password/email', [ 'as' => 'frontend.password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset', ['as' => 'frontend.password.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/reset', ['as' => '', 'uses' => 'Auth\ResetPasswordController@reset']);
Route::get('password/reset/{token}', ['as' => 'frontend.password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);

//Route::get('/home', 'HomeController@index')->name('home');